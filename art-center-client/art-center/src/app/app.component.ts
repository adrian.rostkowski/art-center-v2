import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ApiService} from './api/base/api.service';
import {distinctUntilChanged} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {

  blockUI = false;

  private subscriptions: Subscription[] = [];

  constructor(
    private apiService: ApiService,
    private translate: TranslateService
  ) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.apiService.requestExecuting$
        .pipe(distinctUntilChanged())
        .subscribe((executing) => setTimeout(() => (this.blockUI = executing), 0))
    );
  }

  ngOnDestroy(): void {
  }

}
