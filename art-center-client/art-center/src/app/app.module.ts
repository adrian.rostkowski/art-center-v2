import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './feature/component/home/home.component';
import { NotFoundComponent } from './feature/component/not-found/not-found.component';
import { MenubarModule } from 'primeng/menubar';
import { NavBarComponent } from './feature/component/nav-bar/nav-bar.component';
import { CardModule } from 'primeng/card';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import { TokenInterceptorService } from './api/interceptor/token-interceptor.service';
import { LocalStorageService } from './service/local-storage.service';
import { LoginComponent } from './feature/component/home/login/login.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from 'primeng/dropdown';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FileUploadModule } from 'primeng/fileupload';
import { PanelModule } from 'primeng/panel';
import { InputNumberModule } from 'primeng/inputnumber';
import { EditorModule } from 'primeng/editor';
import { DangerAlertComponent } from './shared/danger-alert/danger-alert.component';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from 'primeng/dialog';
import { AddTextDialogComponent } from './shared/add-text-dialog/add-text-dialog.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { ToastService } from './service/toast.service';
import { DataViewModule } from 'primeng/dataview';
import { LogoutComponent } from './feature/component/home/logout/logout.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SliderModule } from 'primeng/slider';
import { TabViewModule } from 'primeng/tabview';
import { AccordionModule } from 'primeng/accordion';
import { ArtistVisitCardComponent } from './feature/component/artist-visit-card/artist-visit-card.component';
import { RippleModule } from 'primeng/ripple';
import { GalleriaModule } from 'primeng/galleria';
import {LoginGuard} from './shared/login.guard';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import {BlockUIModule} from 'primeng/blockui';
import {TooltipModule} from 'primeng/tooltip';
import { ConfirmAccountWithSecretKeyComponent } from './feature/create-account-module/create-account/confirm-account-with-secret-key/confirm-account-with-secret-key.component';
import { CookieService } from 'ngx-cookie-service';
import {StepsModule} from 'primeng/steps';
import { AuctionDetailsComponent } from './feature/auction-discussion-module/auction-discussion/tabs/auction-details/auction-details.component';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmationService } from 'primeng/api';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DiscussionInfoButtonsComponent } from './feature/auction-discussion-module/auction-discussion/tabs/auction-details/discussion-info-buttons/discussion-info-buttons.component';
import { ZoomImgComponent } from './shared/zoom-img/zoom-img.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from './shared/shared/shared.module';
import {ButtonModule} from 'primeng/button';
import {SearchCriteriaModule} from './feature/search-criteria-module/search-criteria.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    NavBarComponent,
    LoginComponent,
    DangerAlertComponent,
    AddTextDialogComponent,
    LogoutComponent,
    ArtistVisitCardComponent,
    ConfirmAccountWithSecretKeyComponent,
    AuctionDetailsComponent,
    DiscussionInfoButtonsComponent,
    ZoomImgComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    MenubarModule,
    CardModule,
    HttpClientModule,
    ReactiveFormsModule,
    InputTextModule,
    BrowserModule,
    BrowserAnimationsModule,
    DropdownModule,
    ToggleButtonModule,
    InputSwitchModule,
    InputTextareaModule,
    InputTextareaModule,
    FileUploadModule,
    PanelModule,
    InputNumberModule,
    EditorModule,
    DynamicDialogModule,
    DialogModule,
    ToastModule,
    DataViewModule,
    SelectButtonModule,
    FormsModule,
    SliderModule,
    TabViewModule,
    AccordionModule,
    RippleModule,
    GalleriaModule,
    ProgressSpinnerModule,
    BlockUIModule,
    TooltipModule,
    StepsModule,
    ConfirmPopupModule,
    SharedModule,
    SearchCriteriaModule,
    TranslateModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot([
      { path: 'test', loadChildren: './featchure/test-separeted-module/test-sepCreateAccountComponentareted-module.module#TestSeparetedModuleModule' }
    ])
  ],
  providers: [
    LocalStorageService,
    MessageService,
    ToastService,
    LoginGuard,
    CookieService,
    ConfirmationService,
    {
      provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DangerAlertComponent,
  ]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
