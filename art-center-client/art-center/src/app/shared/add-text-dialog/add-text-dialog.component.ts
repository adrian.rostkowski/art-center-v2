import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DynamicDialogConfig,
  DynamicDialogRef
} from 'primeng/dynamicdialog';

@Component({
  selector: 'app-add-text-dialog',
  templateUrl: './add-text-dialog.component.html',
  styleUrls: ['./add-text-dialog.component.css']
})
export class AddTextDialogComponent implements OnInit {

  commentFormGroup: FormGroup;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initCommentFormGroup();
  }

  initCommentFormGroup(): void {
    this.commentFormGroup = this.fb.group({
      text: ['', [Validators.required]],
    });
  }

  addComment(): void {
    this.commentFormGroup.markAsTouched();
    this.commentFormGroup.markAllAsTouched();
    this.commentFormGroup.markAsDirty();
    const formControls = this.commentFormGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.commentFormGroup.get(key).markAsTouched();
      this.commentFormGroup.get(key).markAsDirty();
    }

    if (this.commentFormGroup.valid) {
      this.close(this.commentFormGroup.get('text').value);
    }
  }

  close(text: string): void {
    this.ref.close(text);
  }

}
