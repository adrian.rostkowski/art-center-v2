import {
  Component,
  OnInit
} from '@angular/core';
import {
  DynamicDialogConfig,
  DynamicDialogRef
} from 'primeng/dynamicdialog';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-zoom-img',
  templateUrl: './zoom-img.component.html',
  styleUrls: ['./zoom-img.component.css']
})
export class ZoomImgComponent implements OnInit {

  imgId: number;
  serverUrl = environment.serverUrl;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit(): void {
    this.imgId = this.config.data.imgId;
  }

}
