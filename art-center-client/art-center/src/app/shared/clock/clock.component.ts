import {
  Component,
  Input,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {

  @Input() secondsToEnd: number;
  @Input() stopCondition: boolean;

  interval;
  secondsToEndView: string;
  defaultStringWhenAuctionIsOver = 'EXPIRED';

  constructor() {
  }

  ngOnInit(): void {
    this.calculateTimer();
  }

  calculateTimer(): void {
    this.interval = setInterval(() => {
      const numberOfDays = Math.floor(this.secondsToEnd / 86400);
      const numberOfHours = Math.floor((this.secondsToEnd % 86400) / 3600);
      const numberOfMinutes = Math.floor(((this.secondsToEnd % 86400) % 3600) / 60);
      const numberOfSeconds = Math.floor(((this.secondsToEnd % 86400) % 3600) % 60);

      this.secondsToEndView = numberOfDays + 'd '
        + numberOfHours + 'h ' + numberOfMinutes + 'm ' + numberOfSeconds + 's ';
      if (this.stopCondition || (this.secondsToEnd <= 0)) {
        this.secondsToEndView = this.defaultStringWhenAuctionIsOver;
      }
      this.secondsToEnd--;
    }, 1000);
  }

}
