import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { environment } from '../../../environments/environment';
import { ZoomImgComponent } from '../zoom-img/zoom-img.component';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-img-with-support-imgs',
  templateUrl: './img-with-support-imgs.component.html',
  styleUrls: ['./img-with-support-imgs.component.css']
})
export class ImgWithSupportImgsComponent implements OnInit {

  @Input() imagesId: number[];

  images: any[] = [];
  responsiveOptions: any[];
  serverUrl = environment.serverUrl;

  constructor(public dialogService: DialogService) {
  }

  ngOnInit(): void {

    this.setUpImages(this.imagesId);

    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 5
      },
      {
        breakpoint: '768px',
        numVisible: 3
      },
      {
        breakpoint: '560px',
        numVisible: 1
      }
    ];
  }

  setUpImages(imageIds: number[]): void {
    imageIds.forEach(id => {
      this.images.push(
        {
          // previewImageSrc: '../../../assets/icons/art-center-main-icon.png',
          previewImageSrc: this.serverUrl + '/api/images/' + id,
          thumbnailImageSrc: this.serverUrl + '/api/images/' + id,
          alt: 'Description for Image 1',
          title: 'Title 1',
          imageId: id
        }
      );
    });
  }

  showZoomImg(imgId: number): void {
    const ref = this.dialogService.open(ZoomImgComponent, {
      showHeader: true,
      closeOnEscape: true,
      closable: true,
      dismissableMask: true,
      data: {
        imgId
      }
    });
  }

}
