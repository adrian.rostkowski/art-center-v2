import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { ControlChangeDataComponent } from './control-change-data/control-change-data.component';
import { InputTextModule } from 'primeng/inputtext';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AddTextAreaComponent } from './add-text-area/add-text-area.component';
import { ClockComponent } from '../clock/clock.component';
import { ImgWithSupportImgsComponent } from '../img-with-support-imgs/img-with-support-imgs.component';
import { GalleriaModule } from 'primeng/galleria';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [
    ControlChangeDataComponent,
    AddTextAreaComponent,
    ClockComponent,
    ImgWithSupportImgsComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    InputTextModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    DropdownModule,
    EditorModule,
    SelectButtonModule,
    InputSwitchModule,
    GalleriaModule,
    ButtonModule,
  ],
  exports: [
    ControlChangeDataComponent,
    AddTextAreaComponent,
    ClockComponent,
    ImgWithSupportImgsComponent,
  ]
})
export class SharedModule {
}
