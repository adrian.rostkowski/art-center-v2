import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup
} from '@angular/forms';

@Component({
  selector: 'app-control-change-data',
  templateUrl: './control-change-data.component.html',
  styleUrls: ['./control-change-data.component.css']
})
export class ControlChangeDataComponent implements OnInit {
  @Input() controlName = '';
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;
  @Input() labelValue = '';
  @Input() controlType: string;
  @Input() dropdownValues: DropdownElement[] = [];
  @Input() minPrice: number;
  @Input() optionsToSelect: Option[];

  @Output() fileAdded = new EventEmitter<any>();

  constructor() {
  }

  get formControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }

  ngOnInit(): void {
  }

  onFileUpload(file: any): void {
    this.fileAdded.emit(file);
  }

}

export enum ControlType {
  TEXT,
  TEXT_AREA,
  DROPDOWN,
}

export interface DropdownElement {
  name: string;
  code: string;
}

interface Option {
  name: string;
  value: string;
}
