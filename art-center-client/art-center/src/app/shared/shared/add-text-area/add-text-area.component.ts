import {
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-add-text-area',
  templateUrl: './add-text-area.component.html',
  styleUrls: ['./add-text-area.component.css']
})
export class AddTextAreaComponent implements OnInit {

  @Output() textAdded = new EventEmitter<string>();

  commentFormGroup: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initCommentFormGroup();
  }

  initCommentFormGroup(): void {
    this.commentFormGroup = this.fb.group({
      text: ['', [Validators.required]],
    });
  }

  addComment(): void {
    this.commentFormGroup.markAsTouched();
    this.commentFormGroup.markAllAsTouched();
    this.commentFormGroup.markAsDirty();
    const formControls = this.commentFormGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.commentFormGroup.get(key).markAsTouched();
      this.commentFormGroup.get(key).markAsDirty();
    }

    if (this.commentFormGroup.valid) {
      const textControl = this.commentFormGroup.get('text') as FormControl;
      this.textAdded.emit(textControl.value);
      textControl.setValue('');
    }
  }
}
