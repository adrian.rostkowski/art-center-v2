import {
  Component,
  OnInit
} from '@angular/core';
import {
  DynamicDialogConfig,
  DynamicDialogRef
} from 'primeng/dynamicdialog';

@Component({
  selector: 'app-danger-alert',
  templateUrl: './danger-alert.component.html',
  styleUrls: ['./danger-alert.component.css']
})
export class DangerAlertComponent implements OnInit {

  showChooseButtons = true;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit(): void {
    this.showChooseButtons = this.config.data.showChooseButtons;
  }

  close(result: boolean): void {
    this.ref.close(result);
  }

  closeWithoutResult(): void {
    this.ref.close();
  }
}
