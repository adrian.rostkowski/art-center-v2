import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AddBidToYchCommand } from './dto/AddBidToYchCommand';
import { AddBidToYchResponse } from './dto/AddBidToYchResponse';
import { ApiService } from './base/api.service';
import { GenericResponse } from './generic-response';

@Injectable({
  providedIn: 'root'
})
export class BidApiService {

  serverUrl = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private apiService: ApiService,
  ) {
  }

  public addBid(cmd: AddBidToYchCommand): Observable<GenericResponse<AddBidToYchResponse>> {
    return this.apiService.post<GenericResponse<AddBidToYchResponse>>(
      'api/bid/add',
      cmd,
    );
  }

  public addImgBid(auctionId: number, description: string, file: File[]): Observable<GenericResponse<AddBidToYchResponse>> {

    const formdata: FormData = new FormData();
    const fileName = 'file';
    let i = 1;

    file.forEach((value) => {
      formdata.append('img', value);
      i++;
    });

    formdata.append('description', description);
    formdata.append('auctionId', auctionId.toString());

    // // formdata.append('file', file);
    // Object.keys(cmd)
    //   .filter(key => cmd[key] != null)
    //   .forEach(key => formdata.append(key, cmd[key]));

    return this.apiService.post<GenericResponse<AddBidToYchResponse>>(
      'api/img-bid/add',
      formdata,
    );
  }
}
