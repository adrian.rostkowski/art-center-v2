import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ChangeUserPasswordCommand } from './dto/change-user-password-command';
import { ApiService } from './base/api.service';
import { GenericResponse } from './generic-response';
import { ChangeUsernameCommand } from './dto/change-username-command';
import { IsEmailFreeCommand } from './dto/is-email-free-command';
import { IsLoginFreeCommand } from './dto/is-login-free-command';
import { IsUserNameFreeCommand } from './dto/is-user-name-free-command';
import { CreateNewUserCommand } from './dto/create-new-user-command';
import { ConfirmAccountCommand } from './dto/confirm-account-command';
import { AuthRequest } from './dto/auth-request';
import { AuthenticationRequestResponse } from './dto/authentication-request-response';

@Injectable({
  providedIn: 'root'
})
export class UserApiServiceService {

  serverUrl = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private api: ApiService
  ) {
  }

  public login(userName: string, password: string): Observable<AuthenticationRequestResponse> {
    // const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(username + ':' + password)});
    // return this.http.get<string>(this.serverUrl + '/api/user/authenticate', {headers, responseType: 'text' as 'json'});
    const authRequest = {login: userName, password} as AuthRequest;
    return this.http.post<AuthenticationRequestResponse>(this.serverUrl + '/api/user/authenticate', authRequest);
  }

  public clearCookies(): Observable<string> {
    return this.api.get<string>('api/user/logout');
  }

  public changePassword(cmd: ChangeUserPasswordCommand): Observable<GenericResponse<void>> {
    return this.api.post<GenericResponse<void>>('api/user/change-password', cmd);
  }

  public changeUsername(cmd: ChangeUsernameCommand): Observable<GenericResponse<void>> {
    return this.api.post<GenericResponse<void>>('api/user/change-username', cmd);
  }

  public isEmailFree(cmd: IsEmailFreeCommand): Observable<boolean> {
    return this.api.post<boolean>('api/user/email/is-free', cmd);
  }

  public isLoginFree(cmd: IsLoginFreeCommand): Observable<boolean> {
    return this.api.post<boolean>('api/user/login/is-free', cmd);
  }

  public isUserNameFree(cmd: IsUserNameFreeCommand): Observable<boolean> {
    return this.api.post<boolean>('api/user/user-name/is-free', cmd);
  }

  public create(cmd: CreateNewUserCommand): Observable<GenericResponse<boolean>> {
    return this.api.post<GenericResponse<boolean>>('api/user/create', cmd);
  }

  public confirmAccountBySecretKey(cmd: ConfirmAccountCommand): Observable<GenericResponse<void>> {
    console.log(this.serverUrl);
    console.log(this.serverUrl);
    console.log(this.serverUrl);
    return this.api.post<GenericResponse<void>>('api/user/confirm-account', cmd);
  }

  verifyCaptcha(response: any): Observable<GenericResponse<boolean>> {
    return this.api.post<GenericResponse<boolean>>('api/user/create-account/verify-captcha', response);
  }
}
