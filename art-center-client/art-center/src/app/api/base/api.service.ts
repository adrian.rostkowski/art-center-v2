import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';

import {
  BehaviorSubject,
  Observable,
  throwError
} from 'rxjs';
import {
  catchError,
  finalize,
  map
} from 'rxjs/operators';

import { MessageService } from 'primeng/api';
import { ToastService } from '../../service/toast.service';
import { environment } from '../../../environments/environment';
import { GenericResponse } from '../generic-response';
import { LogoutService } from '../../service/logout.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private static REQUEST_STATUS_FINISHED_UPDATE_TIMEOUT = 1000;
  requestExecuting$: Observable<boolean>;
  baseUrl: string;
  private requestExecuting = new BehaviorSubject<boolean>(false);
  private numberOfRequestExecuting = 0;

  constructor(
    private http: HttpClient,
    private toastService: ToastService,
    private messageService: MessageService,
    private logoutService: LogoutService,
  ) {
    this.baseUrl = environment.serverUrl;
    this.requestExecuting$ = this.requestExecuting.asObservable();
  }

  get<T>(
    path: string,
    successMessageCode: string = null,
    errorMessageCode: string = null,
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: 'body';
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
  ): Observable<T> {
    this.requestStarted();
    return this.http
      .get<T>(`${this.baseUrl}/${path}`, options)
      .pipe(map(response => this.successHandler(response)))
      .pipe(catchError((error: HttpErrorResponse) => this.errorHandler(error, errorMessageCode)))
      .pipe(finalize(() => this.requestFinished()));
  }


  post<T>(
    path: string,
    data: any,
    successMessageCode?: string,
    errorMessageCode?: string,
    showSpinner: boolean = true,
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: 'body';
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
  ): Observable<T> {
    this.requestStarted();
    return this.http
      .post(`${this.baseUrl}/${path}`, data, options)
      .pipe(map(response => this.successHandler(response)))
      .pipe(catchError((error: HttpErrorResponse) => this.errorHandler(error, errorMessageCode)))
      .pipe(finalize(() => this.requestFinished()));
  }

  showSuccess(response: GenericResponse<any>): void {
    this.messageService.add({severity: 'success', summary: 'Success', detail: response.message});
  }

  showError(errorMessage: string): void {
    this.messageService.add({severity: 'error', summary: 'Error', detail: errorMessage});
  }

  private successHandler(response: any): any {
    if (response.message) {
      this.showSuccess(response);
    }
    return response;
  }

  private errorHandler(errorResponse: HttpErrorResponse, errorMessageCode: string, hideSpinner: boolean = false): Observable<never> {
    if (errorResponse.status === 500) {
      this.showError('Server Error');
    } else if (errorResponse.status === 403) {
      if (!!errorResponse.headers.get('AuthorizationError')) {
        this.showAuthorizationErrors(errorResponse);
      } else {
        this.showError('Action forbidden, please login, relog or create account');
      }
    } else {
      this.showError(errorResponse.error);
    }

    return throwError(errorResponse);
  }

  private requestStarted(): void {
    this.numberOfRequestExecuting++;
    this.updateRequestStatus();
  }

  private requestFinished(): void {
    this.numberOfRequestExecuting--;
    this.updateRequestStatus();
  }

  private updateRequestStatus(): void {
    if (this.numberOfRequestExecuting > 0) {
      this.requestExecuting.next(true);
    } else {
      setTimeout(() => {
        this.requestExecuting.next(false);
      }, ApiService.REQUEST_STATUS_FINISHED_UPDATE_TIMEOUT);
    }
  }

  private showAuthorizationErrors(errorResponse: HttpErrorResponse): void {
    switch (errorResponse.headers.get('AuthorizationError')) {
      case 'INVALID_JWT': {
        this.showError('Something goes wrong, please try to logout and login again :)');
        break;
      }
      case 'JWT_TIME_OUT': {
        this.logoutService.logoutUser();
        break;
      }
      default: {
        this.showError('Something goes wrong, please try to logout and login again :)');
        break;
      }
    }
  }
}
