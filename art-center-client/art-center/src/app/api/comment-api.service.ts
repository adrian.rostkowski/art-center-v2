import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AddCommentCommand } from './dto/AddCommentCommand';
import { ApiService } from './base/api.service';
import { GenericResponse } from './generic-response';

@Injectable({
  providedIn: 'root'
})
export class CommentApiService {

  serverUrl = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private apiService: ApiService,
  ) {
  }

  public addComment(cmd: AddCommentCommand): Observable<GenericResponse<void>> {
    return this.apiService.post<GenericResponse<void>>(
      'api/comment/add',
      cmd,
    );
  }
}
