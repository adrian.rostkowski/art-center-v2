import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'src/app/service/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private localStorageService: LocalStorageService,
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const jwt = this.localStorageService.getAuthorizationData();

    if (localStorage.getItem('AuthenticationACBarer')) {
      const cloneReq = request.clone({
        setHeaders: {
          Authorization: 'Barer ' + jwt,
        }
      });
      return next.handle(cloneReq);
    }
    return next.handle(request);
  }
}
