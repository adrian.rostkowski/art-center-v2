export enum AuctionKind {
  YCH = 'YCH',
  ART_TRADE = 'ART_TRADE',
  ADOPT = 'ADOPT'
}
