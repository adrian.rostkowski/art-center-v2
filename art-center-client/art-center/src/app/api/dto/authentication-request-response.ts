export interface AuthenticationRequestResponse {
  userName: string;
  jwt: string;
}
