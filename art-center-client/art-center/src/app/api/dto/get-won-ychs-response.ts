import { YchAuctionCardDto } from './YchAuctionCardDto';

export interface GetWonYchsResponse {
  cards: YchAuctionCardDto[];
  countOfPages: number;
}
