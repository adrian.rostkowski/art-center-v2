export interface GetYchMainPageCommand {
  auctionStep: string;
  daysToEnd: number;
  priceRangeFrom: number;
  priceRangeTo: number;
  ychType: string;
  ychCharacterType: string;
  findByCurrentUser: boolean;
  ychStatus: string;
  userName: string;
  auctionKind: string;

  pageSize: number;
  pageNumber: number;

  orderBy: string[];
  orderDirection: string;
}
