import { YchAuctionCardDto } from './YchAuctionCardDto';

export interface GetBiddingByUserResponse {
  countOfPages: number;
  cards: YchAuctionCardDto[];
}
