export interface GetWonYchsCommand {
  pageSize: number;
  pageNumber: number;
}
