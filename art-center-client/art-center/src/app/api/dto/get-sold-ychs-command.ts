export interface GetSoldYchsCommand {
  pageSize: number;
  pageNumber: number;
}
