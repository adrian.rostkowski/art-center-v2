export class CreateYchAuctionCommand {
  constructor(
    public description: string,
    public minBid: string,
    public maxBid: string,
    public isMaxBid: string,
    public countOfDays: string,
    public ychAuctionType: string,
    public ychAuctionCharacterType: string,
    public auctionKind: string,
    public bidCurrency: string,
  ) {
  }
}
