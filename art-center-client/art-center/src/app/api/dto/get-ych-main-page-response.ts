import { YchAuctionCardDto } from './YchAuctionCardDto';

export interface GetYchMainPageResponse {
  cards: YchAuctionCardDto[];
  countOfPages: number;
}
