export interface CreateNewUserCommand {
  login: string;
  userName: string;
  password: string;
  repeatPassword: string;
  email: string;
}
