import { CommentType } from './enum/CommentType';

export interface AddCommentCommand {
  commentType: CommentType;
  entityId: number;
  textValue: string;
}
