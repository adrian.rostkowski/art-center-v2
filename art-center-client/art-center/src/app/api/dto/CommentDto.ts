export interface CommentDto {
  commentId: number;
  text: string;
  ownerName: string;
}
