export interface ImgBidDto {
  imgId: number;
  ownerName: string;
  description: string;
}
