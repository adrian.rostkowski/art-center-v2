import { YchAuctionCardDto } from './YchAuctionCardDto';

export interface GetSoldYchsResponse {
  cards: YchAuctionCardDto[];
  countOfPages: number;
}
