import { BidDto } from './BidDto';
import { CommentDto } from './CommentDto';
import { AuctionKind } from './enum/auction-kind.enum';
import { ImgBidDto } from './img-bid-dto';

export interface YchAuctionDto {
  ychAuctionId: number;
  imageId: number;
  description: string;
  minBid: number;
  maxBid: number;
  isMaxBid: boolean;
  beginning: Date;
  endDate: number;
  artistName: string;
  bids: BidDto[];
  comments: CommentDto[];
  secondsToEnd: number;
  step: string;
  auctionIsOver: boolean;
  auctionKind: AuctionKind;
  supportImagesId: number[];
  imgBids: ImgBidDto[];
  currency: string;
  isOwnerChooseTime: boolean;
  isUserInArtistFollowList: boolean;

  availableActions: string[];
}
