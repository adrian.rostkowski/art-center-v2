export interface ConfirmAccountCommand {
  userId: number;
  secretKey: string;
}
