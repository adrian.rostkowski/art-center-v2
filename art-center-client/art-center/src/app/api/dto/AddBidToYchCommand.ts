export interface AddBidToYchCommand {
  auctionId: number,
  bidValue: number,
}
