export interface BidDto {
  ownerName: string;
  bidValue: number;
}
