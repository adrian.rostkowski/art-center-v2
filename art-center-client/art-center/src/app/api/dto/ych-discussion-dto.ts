import { CommentDto } from './CommentDto';

export interface YchDiscussionDto {
  comments: CommentDto[];
  winnerId: number;
  winnerName: string;
  ownerId: number;
  ownerName: string;
  description: string;
  ychId: number;
  price: number;
  imageId: number;
  userIsOwner: boolean;
  step: string;
  auctionKind: string;
}
