export interface ChangeUserPasswordCommand {
  newPassword: string;
  repeatNewPassword: string;
  currentPassword: string;
}
