export interface ChangeUsernameCommand {
  newUsername: string;
  currentPassword: string;
}
