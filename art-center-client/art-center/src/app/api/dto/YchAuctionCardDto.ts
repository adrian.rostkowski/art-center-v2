export interface YchAuctionCardDto {
  ychId: number;
  imageId: number;
  artistName: string;
  price: number;
  minBid: number;
  daysToEnd: number;
  auctionStatus: string;
}
