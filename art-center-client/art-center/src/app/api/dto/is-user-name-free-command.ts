export interface IsUserNameFreeCommand {
  userName: string;
}
