export interface ChooseWinnerCommand {
  winnerUserName: string;
  auctionId: number;
}
