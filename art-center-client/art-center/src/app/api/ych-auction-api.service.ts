import { environment } from './../../environments/environment';
import { CreateYchAuctionCommand } from './dto/CreateYchAuctionCommand';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { YchAuctionDto } from './dto/YchAuctionDto';
import { AddBidToYchCommand } from './dto/AddBidToYchCommand';
import { AddBidToYchResponse } from './dto/AddBidToYchResponse';
import { ApiService } from './base/api.service';
import { GenericResponse } from './generic-response';
import { AutoBuyYchAuctionCommand } from './dto/auto-buy-ych-auction-command';
import { YchDiscussionDto } from './dto/ych-discussion-dto';
import { GetYchMainPageCommand } from './dto/get-ych-main-page-command';
import { GetYchMainPageResponse } from './dto/get-ych-main-page-response';
import { GetWonYchsResponse } from './dto/get-won-ychs-response';
import { GetWonYchsCommand } from './dto/get-won-ychs-command';
import { GetSoldYchsResponse } from './dto/get-sold-ychs-response';
import { GetSoldYchsCommand } from './dto/get-sold-ychs-command';
import { ChooseWinnerCommand } from './dto/choose-winner-command';
import { GetBiddingByUserResponse } from './dto/get-bidding-by-user-response';
import { FollowCommand } from './dto/follow-command';

@Injectable({
  providedIn: 'root'
})
export class YchAuctionApiService {

  serverUrl = environment.serverUrl;

  constructor(
    private apiService: ApiService,
  ) {
  }

  public create(cmd: CreateYchAuctionCommand, file: File[]): Observable<GenericResponse<YchAuctionDto>> {

    const formdata: FormData = new FormData();
    const fileName = 'file';
    let i = 1;

    file.forEach((value) => {
      formdata.append(fileName + i, value);
      i++;
    });

    // formdata.append('file', file);
    Object.keys(cmd)
      .filter(key => cmd[key] != null)
      .forEach(key => formdata.append(key, cmd[key]));

    return this.apiService.post<GenericResponse<YchAuctionDto>>(
      'api/ych-auction/create',
      formdata,
    );
  }

  public get(auctionId: number): Observable<YchAuctionDto> {
    return this.apiService.get<YchAuctionDto>('api/ych-auction/get/' + auctionId);
  }

  public getMainYchsPageAuctions(cmd: GetYchMainPageCommand): Observable<GetYchMainPageResponse> {
    return this.apiService.post<GetYchMainPageResponse>('api/ych-auction/get/ych-main-page', cmd);
  }

  public getWonYchAuctions(cmd: GetWonYchsCommand): Observable<GetWonYchsResponse> {
    return this.apiService.post<GetWonYchsResponse>('api/ych-auction/get/won', cmd);
  }

  public getAuctionsCurrentlyBiddingByUser(pageNumber: number, countOfRecords: number): Observable<GetBiddingByUserResponse> {
    return this.apiService.get<GetWonYchsResponse>(
      `api/ych-auction/auctions-currently-bidding/by-user/page/${pageNumber}/records/${countOfRecords}`
    );
  }

  public getSoldYchAuctions(cmd: GetSoldYchsCommand): Observable<GetSoldYchsResponse> {
    return this.apiService.post<GetSoldYchsResponse>('api/ych-auction/get/sold', cmd);
  }

  public getYchAuctionsByCriteriaQuery(cmd: GetYchMainPageCommand): Observable<GetSoldYchsResponse> {
    return this.apiService.post<GetSoldYchsResponse>('api/ych-auction/get/ych-main-page', cmd);
  }


  public addBid(cmd: AddBidToYchCommand): Observable<AddBidToYchResponse> {
    return this.apiService.post<AddBidToYchResponse>(
      'api/ych-auction/add',
      cmd,
    );
  }

  public autoBuy(cmd: AutoBuyYchAuctionCommand): Observable<GenericResponse<void>> {
    return this.apiService.post<GenericResponse<void>>('api/ych-auction/auto-buy', cmd);
  }

  public getDiscussion(ychId: number): Observable<YchDiscussionDto> {
    return this.apiService.get<YchDiscussionDto>('api/ych-auction/get/discussion/' + ychId);
  }

  public closeAuction(ychId: number): Observable<GenericResponse<void>> {
    return this.apiService.post<GenericResponse<void>>('api/ych-auction/close/' + ychId, null);
  }

  public chooseAWinner(cmd: ChooseWinnerCommand): Observable<GenericResponse<void>> {
    return this.apiService.post<GenericResponse<void>>('api/ych-auction/art-trade/add-winner', cmd);
  }

  public followAnArtist(artistToFollow: string): Observable<GenericResponse<void>> {
    const cmd = {artistToFollow} as FollowCommand;
    return this.apiService.post<GenericResponse<void>>('api/follow-group/add', cmd);
  }
}
