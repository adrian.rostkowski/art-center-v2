import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() {
  }

  public setUserId(userId: string): void {
    localStorage.setItem('UserId', userId);
  }

  public setAuthorizationData(auth: string): void {
    localStorage.setItem('AuthenticationACBarer', auth);
  }

  public setUsername(login: string): void {
    localStorage.setItem('username', login);
  }

  public getUsername(): string {
    return  localStorage.getItem('username');
  }

  public getAuthorizationData(): string {
    return localStorage.getItem('AuthenticationACBarer');
  }

  public setUserAvatarId(id: string): void {
    localStorage.setItem('UserAvatarId', id);
  }

  public getUserAvatarId(): string {
    return localStorage.getItem('UserAvatarId');
  }

  public deleteUserAvatarId(): void {
    localStorage.removeItem('UserAvatarId');
  }

  public getUserIdData(): string {
    return localStorage.getItem('UserId');
  }

  public isUserLogin(): boolean {
    return localStorage.getItem('AuthorizationData') != null;
  }

  public isUserAvatarId(): boolean {
    console.log('use avatar id -> ' + localStorage.getItem('UserAvatarId'));
    return localStorage.getItem('UserAvatarId') != null && localStorage.getItem('UserAvatarId') !== '0';
  }

  public deleteAuthorizationData(): void {
    localStorage.removeItem('AuthenticationACBarer');
  }

  public deleteUserId(): void {
    localStorage.removeItem('UserId');
  }
}
