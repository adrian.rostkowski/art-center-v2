import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // private userAuthenticated = false;
  // todo only for develop
  private userAuthenticated = true;

  constructor() { }

  setAuthenticated(isUserAuthenticated: boolean): void {
    this.userAuthenticated = isUserAuthenticated;
  }

  isUserAuthenticated(): boolean {
    return this.userAuthenticated;
  }
}
