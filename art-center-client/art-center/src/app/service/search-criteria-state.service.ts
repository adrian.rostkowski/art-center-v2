import {Injectable} from '@angular/core';
import {SelectOption} from '../shared/select-option';

@Injectable({
  providedIn: 'root'
})
export class SearchCriteriaStateService {

  private model: SearchCriteriaModel;
  private optionsToSelect: SearchCriteriaOptionsToSelectModel;

  constructor() {
    this.initOptionsToSelect();
    this.initModel();
  }

  getModel(): SearchCriteriaModel {
    return this.model;
  }

  getModelToSelect(): SearchCriteriaOptionsToSelectModel {
    return this.optionsToSelect;
  }

  public setYchOptions(): void {
    this.model.selectedYchOption = { name: 'All', value: 'ALL' };
    console.log('hi from state service');
  }


  private initModel(): void {
    this.model = {
      selectedAuctionKindOption: {name: 'All', value: 'ALL'},
      selectedStepOption: {name: 'BIDDING', value: 'BIDDING'},
      selectedYchTypeOption: {name: 'All', value: 'ALL'},
      selectedYchCharacterTypeOption: {name: 'All', value: 'ALL'},
      selectedYchOption: {name: 'All', value: 'ALL'},
      daysToEndSlider: 7,
      rangeValues: [0, 100],
      pageSize: 20
    };
  }

  private initOptionsToSelect(): void {
    this.optionsToSelect = {
      stepOptions: [
        {name: 'BIDDING', value: 'BIDDING'},
        {name: 'CLOSED', value: 'CLOSED'},
        {name: 'ALL', value: 'ALL'}
      ],
      ychTypeOptions: [
        {name: 'Safe', value: 'SAFE'},
        {name: 'Explicit', value: 'EXPLICIT'},
        {name: 'NSFW', value: 'NOT_SAFE_FOR_WORK'},
        {name: 'All', value: 'ALL'}
      ],
      ychCharacterTypeOptions: [
        {name: 'Human', value: 'HUMAN'},
        {name: 'Pony', value: 'PONY'},
        {name: 'Furry', value: 'FURRY'},
        {name: 'All', value: 'ALL'}
      ],
      ychOptions: [
        {name: 'Won', value: 'WON'},
        {name: 'Sold', value: 'SOLD'},
        {name: 'All', value: 'ALL'}
      ],
      auctionKindOptions: [
        {name: 'YCH', value: 'YCH'},
        {name: 'Adopt', value: 'ADOPT'},
        {name: 'Art Trade', value: 'ART_TRADE'},
        {name: 'All', value: 'ALL'}
      ],
    };
  }
}

export interface SearchCriteriaModel {
  selectedAuctionKindOption: SelectOption;
  selectedStepOption: SelectOption;
  selectedYchTypeOption: SelectOption;
  selectedYchCharacterTypeOption: SelectOption;
  selectedYchOption: SelectOption;
  rangeValues: number[];
  daysToEndSlider: number;
  pageSize: number;
}

export interface SearchCriteriaOptionsToSelectModel {
  auctionKindOptions: SelectOption[];
  stepOptions: SelectOption[];
  ychTypeOptions: SelectOption[];
  ychCharacterTypeOptions: SelectOption[];
  ychOptions: SelectOption[];
}
