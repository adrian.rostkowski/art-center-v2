import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {LocalStorageService} from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userName: string;
  userNameSubject = new BehaviorSubject<string>('');

  constructor(
    private localStorageService: LocalStorageService,
  ) {
    this.setUserName();
  }

  updateUserName(userName: string): void {
    this.userNameSubject.next(userName);
  }

  setUserName(): void {
    this.userName = this.localStorageService.getUsername();
    this.updateUserName(this.userName);
  }
}
