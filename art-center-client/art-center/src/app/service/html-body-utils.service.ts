import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HtmlBodyUtilsService {

  constructor() { }

  public parseBody(body: string): string {
    if (!body || body.length <= 0) {
      return;
    }
    const formatHttp = body.replace( /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim, '<a href="$1" target="_blank">$1</a>');
    const formatWww = formatHttp.replace( /(^|[^\/])(www\.[\S]+(\b|$))/gim, '$1<a href="http://$2" target="_blank">$2</a>');
    return formatWww;
  }
}
