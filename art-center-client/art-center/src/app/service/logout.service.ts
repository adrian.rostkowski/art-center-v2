import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { LocalStorageService } from './local-storage.service';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(
    private userService: UserService,
    private localStorageService: LocalStorageService,
    private route: Router,
    private authService: AuthService,
  ) { }

  public logoutUser(): void {
    this.userService.userNameSubject.next('');
    this.localStorageService.setAuthorizationData('');
    this.localStorageService.setUsername('');
    this.authService.setAuthenticated(false);
    this.route.navigate(['/login']);
  }
}
