import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuctionDiscussionComponent} from './auction-discussion/auction-discussion.component';

const routes: Routes = [
    { path: '', component: AuctionDiscussionComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuctionDiscussionRoutingModule { }
