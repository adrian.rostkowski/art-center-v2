import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {YchAuctionDto} from '../../../api/dto/YchAuctionDto';
import {ActivatedRoute} from '@angular/router';
import {YchAuctionApiService} from '../../../api/ych-auction-api.service';
import {YchDiscussionDto} from '../../../api/dto/ych-discussion-dto';
import {AddTextDialogComponent} from '../../../shared/add-text-dialog/add-text-dialog.component';
import {CommentType} from '../../../api/dto/enum/CommentType';
import {DialogService} from 'primeng/dynamicdialog';
import {CommentApiService} from '../../../api/comment-api.service';
import {DangerAlertComponent} from '../../../shared/danger-alert/danger-alert.component';
import {HtmlBodyUtilsService} from '../../../service/html-body-utils.service';
import {debuglog} from 'util';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-auction-discussion',
  templateUrl: './auction-discussion.component.html',
  styleUrls: ['./auction-discussion.component.css'],
  providers: [DialogService]
})
export class AuctionDiscussionComponent implements OnInit {

  ychDiscussionDto: YchDiscussionDto;
  currentStep: string;
  serverUrl = environment.serverUrl;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ychAuctionApiService: YchAuctionApiService,
    public dialogService: DialogService,
    private commentApiService: CommentApiService,
    public htmlBodyUtilsService: HtmlBodyUtilsService,
    private detectionRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      const ychId = params.get('ychId') as any as number;
      this.getYchAndSetStep(ychId);
    });
  }

  getYchAndSetStep(id: number): void {
    this.ychAuctionApiService.getDiscussion(id)
      .subscribe(
        (response) => {
          this.ychDiscussionDto = response;
          this.currentStep = this.ychDiscussionDto.step;
        }
      );
  }

  showAddComment(): void {
    const ref = this.dialogService.open(AddTextDialogComponent, {
      header: 'Add comment',
      width: '70%'
    });

    ref.onClose.subscribe((text: string) => {
      if (text) {
        this.saveComment(text);
      }
    });
  }

  saveComment(text: string): void {
    this.commentApiService.addComment(
      {
        commentType: CommentType.BIDDING,
        entityId: this.ychDiscussionDto.ychId,
        textValue: text,
      }).subscribe(
      () => {
        this.getYchAndSetStep(this.ychDiscussionDto.ychId);
      }
    );
  }

  isDiscussionStep(): boolean {
    return this.currentStep === 'CONSULTATIONS';
  }

  showCloseAuctionAlert(): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'Are you sure to close this whole commission?',
      width: '70%',
      data: {
        showChooseButtons: true
      }
    });

    ref.onClose.subscribe((success: boolean) => {
      if (success) {
        this.closeYch();
      }
    });
  }

  private closeYch(): void {
    this.ychAuctionApiService.closeAuction(this.ychDiscussionDto.ychId).subscribe(
      () => {
        this.getYchAndSetStep(this.ychDiscussionDto.ychId);
      }
    );
  }

}
