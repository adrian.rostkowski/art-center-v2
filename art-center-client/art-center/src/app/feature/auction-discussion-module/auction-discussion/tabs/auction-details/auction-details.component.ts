import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {YchDiscussionDto} from '../../../../../api/dto/ych-discussion-dto';

@Component({
  selector: 'app-auction-details',
  templateUrl: './auction-details.component.html',
  styleUrls: ['./auction-details.component.css']
})
export class AuctionDetailsComponent implements OnInit {

  @Input() ychDiscussionDto: YchDiscussionDto;
  @Input() currentStep: string;
  @Output() onCloseDiscussion = new EventEmitter<void>();

  serverUrl = environment.serverUrl;

  constructor() { }

  ngOnInit(): void {
  }

  isDiscussionStep(): boolean {
    return this.currentStep === 'CONSULTATIONS';
  }

  showCloseAuctionAlert(): void {
    this.onCloseDiscussion.emit();
  }

}
