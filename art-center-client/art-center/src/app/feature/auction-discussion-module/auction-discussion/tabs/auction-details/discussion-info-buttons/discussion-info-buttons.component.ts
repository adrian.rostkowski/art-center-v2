import {Component, Input, OnInit} from '@angular/core';
import {YchDiscussionDto} from '../../../../../../api/dto/ych-discussion-dto';

@Component({
  selector: 'app-discussion-info-buttons',
  templateUrl: './discussion-info-buttons.component.html',
  styleUrls: ['./discussion-info-buttons.component.css']
})
export class DiscussionInfoButtonsComponent implements OnInit {

  @Input() ychDiscussionDto: YchDiscussionDto;

  constructor() { }

  ngOnInit(): void {
  }

}
