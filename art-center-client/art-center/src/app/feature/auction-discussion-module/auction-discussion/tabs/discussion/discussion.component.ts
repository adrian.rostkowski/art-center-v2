import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {YchDiscussionDto} from '../../../../../api/dto/ych-discussion-dto';
import {HtmlBodyUtilsService} from '../../../../../service/html-body-utils.service';

@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.css']
})
export class DiscussionComponent implements OnInit {

  @Input() ychDiscussionDto: YchDiscussionDto;
  @Output() onCommentAdded = new EventEmitter<any>();

  constructor(
    public htmlBodyUtilsService: HtmlBodyUtilsService,
  ) { }

  ngOnInit(): void {
  }

  saveComment(comment: string): void {
    this.onCommentAdded.emit(comment);
  }

  isDiscussionOver(): boolean {
    return this.ychDiscussionDto.step === 'CLOSED';
  }

}
