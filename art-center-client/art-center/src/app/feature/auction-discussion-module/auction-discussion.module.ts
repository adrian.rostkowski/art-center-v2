import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuctionDiscussionRoutingModule } from './auction-discussion-routing.module';
import {AuctionDiscussionComponent} from './auction-discussion/auction-discussion.component';
import {TabViewModule} from 'primeng/tabview';
import {DiscussionComponent} from './auction-discussion/tabs/discussion/discussion.component';
import {ButtonModule} from 'primeng/button';
import {AuctionDetailsModule} from '../auction-details-module/auction-details.module';
import {SharedModule} from '../../shared/shared/shared.module';


@NgModule({
  declarations: [
    AuctionDiscussionComponent,
    DiscussionComponent
  ],
  imports: [
    CommonModule,
    AuctionDiscussionRoutingModule,
    TabViewModule,
    SharedModule,
    ButtonModule,
    AuctionDetailsModule,
  ]
})
export class AuctionDiscussionModule { }
