import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuctionDetailsRoutingModule } from './auction-details-routing.module';
import {YchAuctionDetailsComponent} from './ych-auction-details/ych-auction-details.component';
import {ImgBidsComponent} from './ych-auction-details/img-bids/img-bids.component';
import {ImgBidCardComponent} from './ych-auction-details/img-bid-card/img-bid-card.component';
import {CommentsComponent} from './ych-auction-details/comments/comments.component';
import {BidSectionComponent} from './ych-auction-details/bid-section/bid-section.component';
import {AuctionInfoButtonsComponent} from './ych-auction-details/auction-info-buttons/auction-info-buttons.component';
import {AddImgBidComponent} from './ych-auction-details/add-img-bid/add-img-bid.component';
import {AddBidComponent} from './ych-auction-details/add-comment/add-bid.component';
import {TabViewModule} from 'primeng/tabview';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion';
import {SharedModule} from '../../shared/shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FollowButtonComponent} from "../fallow-module/follow-button/follow-button.component";
import {FollowModule} from "../fallow-module/follow.module";


@NgModule({
  declarations: [
    YchAuctionDetailsComponent,
    ImgBidsComponent,
    ImgBidCardComponent,
    CommentsComponent,
    BidSectionComponent,
    AuctionInfoButtonsComponent,
    AddImgBidComponent,
    AddBidComponent,
  ],
  imports: [
    CommonModule,
    AuctionDetailsRoutingModule,
    TabViewModule,
    CardModule,
    ButtonModule,
    AccordionModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FollowModule
  ],
  exports: [
    YchAuctionDetailsComponent
  ]
})
export class AuctionDetailsModule { }
