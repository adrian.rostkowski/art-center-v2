import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {YchAuctionDetailsComponent} from './ych-auction-details/ych-auction-details.component';

const routes: Routes = [
    { path: '', component: YchAuctionDetailsComponent },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuctionDetailsRoutingModule { }
