import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {YchAuctionDto} from '../../../../api/dto/YchAuctionDto';

@Component({
  selector: 'app-bid-section',
  templateUrl: './bid-section.component.html',
  styleUrls: ['./bid-section.component.css']
})
export class BidSectionComponent implements OnInit {

  @Input() ychAuctionDto: YchAuctionDto;
  @Output() onShowAddBid = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  showAddBid(): void {
    this.onShowAddBid.emit();
  }

}
