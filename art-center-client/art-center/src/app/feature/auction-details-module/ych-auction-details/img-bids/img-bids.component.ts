import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {YchAuctionDto} from '../../../../api/dto/YchAuctionDto';

@Component({
  selector: 'app-img-bids',
  templateUrl: './img-bids.component.html',
  styleUrls: ['./img-bids.component.css']
})
export class ImgBidsComponent implements OnInit {

  @Input() ychAuctionDto: YchAuctionDto;
  @Output() onChooseAWinnerForArtTrade = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  chooseAWinnerForArtTrade(ownerName: string): void {
    this.onChooseAWinnerForArtTrade.emit(ownerName);
  }

}
