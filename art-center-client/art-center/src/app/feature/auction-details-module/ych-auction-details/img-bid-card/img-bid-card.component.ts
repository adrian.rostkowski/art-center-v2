import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImgBidDto} from '../../../../api/dto/img-bid-dto';
import {YchAuctionCardDto} from '../../../../api/dto/YchAuctionCardDto';
import {YchAuctionDto} from '../../../../api/dto/YchAuctionDto';
import {HtmlBodyUtilsService} from '../../../../service/html-body-utils.service';
import {environment} from '../../../../../environments/environment';
import {DialogService} from 'primeng/dynamicdialog';
import {ZoomImgComponent} from '../../../../shared/zoom-img/zoom-img.component';

@Component({
  selector: 'app-img-bid-card',
  templateUrl: './img-bid-card.component.html',
  styleUrls: ['./img-bid-card.component.css']
})
export class ImgBidCardComponent implements OnInit {

  @Input() imgBid: ImgBidDto;
  @Input() auction: YchAuctionDto;

  @Output() winnerHasBeenSelected = new EventEmitter<string>();

  serverUrl = environment.serverUrl;

  constructor(
    public htmlBodyUtilsService: HtmlBodyUtilsService,
    public dialogService: DialogService,
  ) { }

  ngOnInit(): void {
  }

  chooseAWinner(): void {
    this.winnerHasBeenSelected.emit(this.imgBid.ownerName);
  }

  isAuctionOnBiddingStep(): boolean {
    return this.auction?.step === 'BIDDING';
  }

  isActionAvailable(action: string): boolean {
    return this.auction.availableActions.includes(action);
  }

  showZoomImg(imgId: number): void {
    const ref = this.dialogService.open(ZoomImgComponent, {
      showHeader: true,
      closeOnEscape: true,
      closable: true,
      dismissableMask: true,
      data: {
        imgId
      }
    });
  }

}
