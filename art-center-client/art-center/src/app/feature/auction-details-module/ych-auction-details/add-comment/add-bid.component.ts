import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-bid.component.html',
  styleUrls: ['./add-bid.component.css']
})
export class AddBidComponent implements OnInit {

  formGroup: FormGroup;
  minBidValue: number;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.minBidValue = this.config.data.minBidValue;
    this.initFormGroup();
  }

  initFormGroup(): void {
    this.formGroup = this.fb.group({
      bidValue: [this.minBidValue + 1, [Validators.required]],
    });
  }

  async onBidSubmit(): Promise<void> {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (this.formGroup.valid) {
      this.close(this.formGroup.get('bidValue').value);
    }
  }

  close(bid: number): void {
    this.ref.close(bid);
  }
}
