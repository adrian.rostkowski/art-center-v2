import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {YchAuctionDto} from '../../../../api/dto/YchAuctionDto';

@Component({
  selector: 'app-auction-info-buttons',
  templateUrl: './auction-info-buttons.component.html',
  styleUrls: ['./auction-info-buttons.component.css']
})
export class AuctionInfoButtonsComponent implements OnInit {

  @Input() ychAuctionDto: YchAuctionDto;
  @Output() autoBuy = new EventEmitter<void>();
  @Output() navigateToArtistVisitCard = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  autoBuyEmit(): void {
    this.autoBuy.emit();
  }

  onNavigateToArtistVisitCard(): void {
    this.navigateToArtistVisitCard.emit();
  }

}
