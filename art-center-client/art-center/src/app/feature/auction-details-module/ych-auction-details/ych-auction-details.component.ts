import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BidApiService} from 'src/app/api/bid-api.service';
import {AddBidToYchCommand} from 'src/app/api/dto/AddBidToYchCommand';
import {YchAuctionDto} from 'src/app/api/dto/YchAuctionDto';
import {YchAuctionApiService} from 'src/app/api/ych-auction-api.service';
import {CommentApiService} from '../../../api/comment-api.service';
import {CommentType} from '../../../api/dto/enum/CommentType';
import {DialogService} from 'primeng/dynamicdialog';
import {AddTextDialogComponent} from '../../../shared/add-text-dialog/add-text-dialog.component';
import {DangerAlertComponent} from '../../../shared/danger-alert/danger-alert.component';
import {AuctionKind} from '../../../api/dto/enum/auction-kind.enum';
import {AddBidComponent} from './add-comment/add-bid.component';
import {AddImgBidComponent} from './add-img-bid/add-img-bid.component';
import {ChooseWinnerCommand} from '../../../api/dto/choose-winner-command';
import {HtmlBodyUtilsService} from '../../../service/html-body-utils.service';
import {environment} from '../../../../environments/environment';
import {ZoomImgComponent} from '../../../shared/zoom-img/zoom-img.component';

@Component({
  selector: 'app-ych-auction-details',
  templateUrl: './ych-auction-details.component.html',
  styleUrls: ['./ych-auction-details.component.css'],
  providers: [DialogService]
})
export class YchAuctionDetailsComponent implements OnInit {

  @Input() ychId: number;

  ychAuctionDto: YchAuctionDto;
  newBidValue: number;
  minBidValue: number;
  auctionKind: AuctionKind = AuctionKind.ADOPT;

  serverUrl = environment.serverUrl;

  constructor(
    private ychAuctionApiService: YchAuctionApiService,
    private bidApiService: BidApiService,
    private commentApiService: CommentApiService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private router: Router,
    public htmlBodyUtilsService: HtmlBodyUtilsService
  ) {
  }

  ngOnInit(): void {
    if (!!this.ychId) {
      this.getYch(this.ychId);
    } else {
      this.activatedRoute.paramMap.subscribe(params => {
        const ychId = params.get('ychId') as any as number;
        // this.auctionKind = params.get('auctionKind') as any as AuctionKind;
        this.getYch(ychId);
      });
    }
  }

  setMinBidValue(): void {
    if (this.ychAuctionDto.bids.length > 0) {
      const values = this.ychAuctionDto.bids.map(n => n.bidValue);
      this.minBidValue = Math.max(...values);
    } else if (this.ychAuctionDto.minBid) {
      this.minBidValue = this.ychAuctionDto.minBid - 1;
    } else {
      this.minBidValue = 0;
    }
  }

  getAuction(auctionId: number): void {
    this.ychAuctionApiService.get(auctionId)
      .subscribe(
        (response) => {
          this.ychAuctionDto = response;
          this.auctionKind = this.ychAuctionDto.auctionKind;
          this.setMinBidValue();
        }
      );
  }

  getYch(id: number): void {
    this.ychAuctionApiService.get(id)
      .subscribe(
        (response) => {
          this.ychAuctionDto = response;
          this.auctionKind = this.ychAuctionDto.auctionKind;
          this.setMinBidValue();
        }
      );
  }

  getAdopt(id: number): void {
    this.ychAuctionApiService.get(id)
      .subscribe(
        (response) => {
          this.ychAuctionDto = response;
          this.auctionKind = this.ychAuctionDto.auctionKind;
          this.setMinBidValue();
        }
      );
  }

  showAddBid(): void {
    switch (this.auctionKind) {
      case AuctionKind.YCH: {
        this.showAddCurrencyBid();
        break;
      }
      case AuctionKind.ADOPT: {
        this.showAddCurrencyBid();
        // this.showAddCurrencyBid();
        break;
      }
      case AuctionKind.ART_TRADE: {
        this.showAddImgBid();
        break;
      }
    }
  }

  showAddImgBid(): void {
    const ref = this.dialogService.open(AddImgBidComponent, {
      data: {
        auctionId: this.ychAuctionDto.ychAuctionId
      },
      header: 'Add Bid',
      width: '600px'
    });

    ref.onClose.subscribe((bid: number) => {
      this.getYch(this.ychAuctionDto.ychAuctionId);
      // if (bid) {
      //   this.saveBid(bid);
      // }
    });
  }

  showAddCurrencyBid(): void {
    const ref = this.dialogService.open(AddBidComponent, {
      data: {
        minBidValue: this.minBidValue
      },
      header: 'Add Bid',
      width: '600px'
    });

    ref.onClose.subscribe((bid: number) => {
      if (bid) {
        this.saveBid(bid);
      }
    });
  }

  saveBid(bid: number): void {
    const cmd: AddBidToYchCommand = {
      auctionId: this.ychAuctionDto.ychAuctionId,
      bidValue: bid,
    };
    this.bidApiService.addBid(cmd)
      .subscribe(
        () => {
          this.getYch(this.ychAuctionDto.ychAuctionId);
        }
      );
  }

  showAddComment(): void {
    const ref = this.dialogService.open(AddTextDialogComponent, {
      header: 'Add comment',
      width: '70%'
    });

    ref.onClose.subscribe((text: string) => {
      if (text) {
        this.saveComment(text);
      }
    });
  }

  saveComment(text: string): void {
    this.commentApiService.addComment(
      {
        commentType: CommentType.BIDDING,
        entityId: this.ychAuctionDto.ychAuctionId,
        textValue: text,
      }).subscribe(
      () => {
        this.getYch(this.ychAuctionDto.ychAuctionId);
      }
    );
  }

  onAutoBuy(): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: `Are you sure to buy this auction by ${this.ychAuctionDto.maxBid} USD`,
      width: '50%',
      data: {
        showChooseButtons: true
      }
    });

    ref.onClose.subscribe(
      (result) => {
        if (result === true) {
          this.ychAuctionApiService.autoBuy({ ychId: this.ychAuctionDto.ychAuctionId }).subscribe(
            (response) => {
              if (response.success) {
                this.showInfoAfterAutoBuy();
                this.getYch(this.ychAuctionDto.ychAuctionId);
              }
            }
          );
        }
      }
    );
  }

  showInfoAfterAutoBuy(): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'You are a WINNER! To find your auction go to your panel and select "Won Auctions"!',
      width: '50%',
      closable: false,
      data: {
        showChooseButtons: false
      }
    });
  }

  navigateToArtistVisitCard(): void {
      this.router.navigate(['/artist-visit-card', this.ychAuctionDto.artistName]);
  }

  getImagesId(auctionDto: YchAuctionDto): number[] {
    const imagesId: number[] = [];
    imagesId.push(auctionDto.imageId);
    auctionDto.supportImagesId.forEach(id => imagesId.push(id));
    return imagesId;
  }

  chooseAWinnerForArtTrade(winnerName: string): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: `${winnerName} will be your winner! After that this auction will be closed and you should look for CONSULTATIONS in your panel to take contact with your winner`,
      width: '50%',
      closable: false,
      data: {
        showChooseButtons: true
      }
    });

    ref.onClose.subscribe(isSuccess => {
      if (isSuccess) {
        const cmd: ChooseWinnerCommand = {
          winnerUserName: winnerName,
          auctionId: this.ychAuctionDto.ychAuctionId
        };

        this.ychAuctionApiService.chooseAWinner(cmd).subscribe(() => {
            this.getYch(this.ychAuctionDto.ychAuctionId);
        });
      }
    });
  }

  isAuctionOnBiddingStep(): boolean {
    return this.ychAuctionDto.step === 'BIDDING';
  }

  isActionAvailable(action: string): boolean {
    return this.ychAuctionDto.availableActions.includes(action);
  }

  showZoomImg(imgId: number): void {
    const ref = this.dialogService.open(ZoomImgComponent, {
      showHeader: true,
      closeOnEscape: true,
      closable: true,
      dismissableMask: true,
      data: {
        imgId
      }
    });
  }

  // parseBody(body: string): string {
  //   return body.replace(/(http.*?\s)/, '<a href=\"$1\">$1</a>');
  // }
}
