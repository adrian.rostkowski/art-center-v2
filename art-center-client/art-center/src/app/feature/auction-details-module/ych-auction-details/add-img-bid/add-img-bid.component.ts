import { Component, OnInit } from '@angular/core';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CreateYchAuctionCommand} from '../../../../api/dto/CreateYchAuctionCommand';
import {YchAuctionDto} from '../../../../api/dto/YchAuctionDto';
import {DangerAlertComponent} from '../../../../shared/danger-alert/danger-alert.component';
import {BidApiService} from '../../../../api/bid-api.service';

@Component({
  selector: 'app-add-img-bid',
  templateUrl: './add-img-bid.component.html',
  styleUrls: ['./add-img-bid.component.css']
})
export class AddImgBidComponent implements OnInit {

  formGroup: FormGroup;
  auctionId: number;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private fb: FormBuilder,
    public dialogService: DialogService,
    private bidApiService: BidApiService,
  ) { }

  ngOnInit(): void {
    this.auctionId = this.config.data.auctionId;
    this.initForm();
  }

  initForm(): void {
    this.formGroup = this.fb.group({
      description: ['', []],
      file: ['', [Validators.required]],
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (!this.formGroup.get('file').valid) {
      this.showAlert();
    }

    if (this.formGroup.valid) {
      // const cmd: CreateYchAuctionCommand = {
      //   description: this.formGroup.get('description').value,
      // };

      const images = [this.formGroup.get('file').value];

      this.bidApiService.addImgBid(this.auctionId, this.formGroup.get('description').value, images)
        .subscribe(
          (response) => {
            this.onClose();
            // const ychId = response.response as YchAuctionDto;
            // this.router.navigate(['/ych-details', ychId.ychAuctionId]);
          }
        );
    }
  }

  onUpload(files: FileList): void {
    const file = files.item(0);
    const control = this.formGroup.get('file');
    control.setValue(file);
  }

  showAlert(): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'Art image is required',
      width: '50%',
      data: {
        showChooseButtons: false
      }
    });
  }

  onClose(): void {
    this.ref.close();
  }

}
