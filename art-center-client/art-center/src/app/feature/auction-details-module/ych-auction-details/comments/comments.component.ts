import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {YchAuctionDto} from '../../../../api/dto/YchAuctionDto';
import {HtmlBodyUtilsService} from '../../../../service/html-body-utils.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  @Input() ychAuctionDto: YchAuctionDto;
  @Output() onSaveComment = new EventEmitter<string>();

  constructor(public htmlBodyUtilsService: HtmlBodyUtilsService) { }

  ngOnInit(): void {
  }

  saveComment(comment: string): void {
    this.onSaveComment.emit(comment);
  }

}
