import {
  Component,
  OnInit
} from '@angular/core';
import { SearchMode } from '../../search-criteria-module/search-criteria-auctions/search-criteria-auctions.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-artist-visit-card',
  templateUrl: './artist-visit-card.component.html',
  styleUrls: ['./artist-visit-card.component.css']
})
export class ArtistVisitCardComponent implements OnInit {

  searchMode = SearchMode.FIND_BY_SPECIFIC_ARTIST;
  artistName: string;

  constructor(
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.artistName = params.get('artistName') as any as string;
    });
  }

}
