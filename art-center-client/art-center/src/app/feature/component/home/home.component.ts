import {
  Component,
  OnInit
} from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { ViewMode } from '../../search-criteria-module/search-criteria-auctions/search-criteria-auctions.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  images: any[];
  responsiveOptions: any[];
  searchCriteriaViewMode = ViewMode.BASIC;

  constructor(
    private primengConfig: PrimeNGConfig
  ) {

  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;

    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 5
      },
      {
        breakpoint: '768px',
        numVisible: 3
      },
      {
        breakpoint: '560px',
        numVisible: 1
      }
    ];

    // this.images = [
    //   // '../../../assets/icons/art-center-main-icon.png'
    //   {
    //     // previewImageSrc: '../../../assets/icons/art-center-main-icon.png',
    //     previewImageSrc: 'http://localhost:8080/api/images/45',
    //     thumbnailImageSrc: 'http://localhost:8080/api/images/45',
    //     alt: 'Description for Image 1',
    //     title: 'Title 1'
    //   },
    //   {
    //     // previewImageSrc: '../../../assets/icons/art-center-main-icon.png',
    //     previewImageSrc: 'http://localhost:8080/api/images/53',
    //     thumbnailImageSrc: 'http://localhost:8080/api/images/53',
    //     alt: 'Description for Image 1',
    //     title: 'Title 1'
    //   },
    //   {
    //     // previewImageSrc: '../../../assets/icons/art-center-main-icon.png',
    //     previewImageSrc: 'http://localhost:8080/api/images/46',
    //     thumbnailImageSrc: 'http://localhost:8080/api/images/46',
    //     alt: 'Description for Image 1',
    //     title: 'Title 1'
    //   },
    //   {
    //     // previewImageSrc: '../../../assets/icons/art-center-main-icon.png',
    //     previewImageSrc: 'http://localhost:8080/api/images/47',
    //     thumbnailImageSrc: 'http://localhost:8080/api/images/47',
    //     alt: 'Description for Image 1',
    //     title: 'Title 1'
    //   },
    // ];
  }

}
