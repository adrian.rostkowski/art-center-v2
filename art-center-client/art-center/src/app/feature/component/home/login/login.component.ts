import { LocalStorageService } from '../../../../service/local-storage.service';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserApiServiceService } from 'src/app/api/user-api-service.service';
import { UserService } from '../../../../service/user.service';
import { AuthService } from '../../../../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userApiService: UserApiServiceService,
    private route: Router,
    private localStorageService: LocalStorageService,
    private userService: UserService,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.initFormGroup();
  }

  initFormGroup(): void {
    this.formGroup = this.fb.group({
      login: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (this.formGroup.valid) {
      this.localStorageService.deleteAuthorizationData();

      const login = this.formGroup.get('login').value as any as string;
      const password = this.formGroup.get('password').value as any as string;

      this.userApiService.login(login, password)
        .subscribe(
          (response) => {
            if (response) {
              this.authService.setAuthenticated(true);
              this.localStorageService.setUsername(response.userName);
              this.userService.updateUserName(response.userName);
              // this.localStorageService.setAuthorizationData(login + ':' + password);
              this.localStorageService.setAuthorizationData(response.jwt);
              this.route.navigate(['/home']);
            }
          },
          (error) => {
            alert('wrong login or password');
          }
        );
    }
  }

  cast<T>(data: any, model: new (...args: any[]) => T): T {
    const classInstance = new model();
    const classProps = Object.getOwnPropertyNames(classInstance);

    classProps.forEach(prop => classInstance[prop] = data[prop]);
    return classInstance;
  }

}
