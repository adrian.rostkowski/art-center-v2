import {
  Component,
  OnInit
} from '@angular/core';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { LogoutService } from '../../../../service/logout.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
  // providers: [DynamicDialogRef]
})
export class LogoutComponent implements OnInit {

  constructor(
    public ref: DynamicDialogRef,
    public logoutService: LogoutService,
  ) {
  }

  ngOnInit(): void {
  }

  logout(): void {
    this.logoutService.logoutUser();
    this.close();
  }

  close(): void {
    this.ref.close();
  }

}
