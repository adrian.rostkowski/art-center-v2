import {
  Component,
  OnInit
} from '@angular/core';
import {
  ConfirmationService,
  MenuItem
} from 'primeng/api';
import { UserService } from '../../../service/user.service';
import { DialogService } from 'primeng/dynamicdialog';
import { LogoutComponent } from '../home/logout/logout.component';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  providers: [DialogService]
})
export class NavBarComponent implements OnInit {

  items: MenuItem[] = [];
  userName: string;
  userIsLoggedIn: boolean;

  constructor(
    private userService: UserService,
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
  ) {
  }

  ngOnInit(): void {
    this.setUpMenu();
    this.setUserName();
  }

  setUpMenu(): void {
    this.items = [
      {label: 'Home', icon: 'pi pi-fw pi-home', routerLink: ['/home']},
      {
        label: 'Create Auction', icon: 'pi pi-fw pi-plus', visible: this.userIsLoggedIn,
        items: [
          {label: 'Create Ych', routerLink: ['/create-ych', 'YCH'], icon: 'pi pi-fw pi-palette'},
          {label: 'Create Adopt', routerLink: ['/create-ych', 'ADOPT'], icon: 'pi pi-fw pi-palette'},
          {label: 'Create Art Trade', routerLink: ['/create-ych', 'ART_TRADE'], icon: 'pi pi-fw pi-palette'}
        ]
      },
      {label: 'Login', icon: 'pi pi-fw pi-sign-in', routerLink: ['/login'], visible: !this.userIsLoggedIn},
      {
        label: 'Logout', icon: 'pi pi-fw pi-sign-out', visible: this.userIsLoggedIn, command: event => {
          this.openLogoutDialog();
        }
      },
      {
        label: 'Create Account',
        icon: 'pi pi-fw pi-user-plus',
        routerLink: ['/create-account'],
        visible: !this.userIsLoggedIn
      },

      {label: 'Your Panel', icon: 'pi pi-fw pi-user', routerLink: ['/user-panel'], visible: this.userIsLoggedIn},
    ];
  }

// todo this is wrong, we should not relate username and isLogIn
  setUserName(): void {
    this.userService.userNameSubject.subscribe(
      (userName) => {
        this.userName = userName;
        if (userName != null && userName !== '') {
          this.userIsLoggedIn = true;
        } else {
          this.userIsLoggedIn = false;
        }

        this.setUpMenu();
      }
    );
  }

  openLogoutDialog(): void {
    this.dialogService.open(LogoutComponent, {
      header: 'Are you sure to logout?',
      width: '600px',
      closable: false
    });

    // ref.onClose.subscribe();
  }

}
