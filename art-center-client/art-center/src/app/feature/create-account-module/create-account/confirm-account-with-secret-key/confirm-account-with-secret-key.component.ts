import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { UserApiServiceService } from '../../../../api/user-api-service.service';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import { ConfirmAccountCommand } from '../../../../api/dto/confirm-account-command';

@Component({
  selector: 'app-confirm-account-with-secret-key',
  templateUrl: './confirm-account-with-secret-key.component.html',
  styleUrls: ['./confirm-account-with-secret-key.component.css']
})
export class ConfirmAccountWithSecretKeyComponent implements OnInit {

  formGroup: FormGroup;
  userId: number;

  constructor(
    private fb: FormBuilder,
    private userService: UserApiServiceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.userId = params.get('userId') as any as number;
    });
    this.initFormGroup();
  }

  initFormGroup(): void {
    this.formGroup = this.fb.group({
      secretKey: ['',
        [
          Validators.required,
        ]],
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (this.formGroup.valid) {
      const cmd: ConfirmAccountCommand = {
        userId: this.userId,
        secretKey: this.formGroup.get('secretKey').value
      };
      this.userService.confirmAccountBySecretKey(cmd).subscribe(
        () => {
          this.router.navigate(['/login']);
        }
      );
    }
  }

}
