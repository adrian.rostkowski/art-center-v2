import {
  Component,
  OnInit
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { BaseComponent } from '../../../shared/BaseComponent';
import { UserApiServiceService } from '../../../api/user-api-service.service';
import {
  debounceTime,
  first,
  map,
  switchMap
} from 'rxjs/operators';
import { CreateNewUserCommand } from '../../../api/dto/create-new-user-command';
import { Router } from '@angular/router';
import { DangerAlertComponent } from '../../../shared/danger-alert/danger-alert.component';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css'],
  providers: [DialogService]
})
export class CreateAccountComponent extends BaseComponent implements OnInit {

  formGroup: FormGroup;
  reCaptchaSuccess = false;

  constructor(
    private fb: FormBuilder,
    private userService: UserApiServiceService,
    private router: Router,
    public dialogService: DialogService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.initFormGroup();
    this.onRepeatNewPasswordChange();
    // this.onEmailChange();
  }

  initFormGroup(): void {
    this.formGroup = this.fb.group({
      login: ['',
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.minLength(6)
        ], [this.isLoginFreeValidator()]],
      password: ['',
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.minLength(6)
        ]],
      repeatPassword: ['',
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.minLength(6)
        ]],
      userName: ['',
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.minLength(6)
        ], [this.isUserNameFreeValidator()]],
      email: ['',
        [
          Validators.required,
          Validators.email,
          Validators.maxLength(40)
        ], [this.isEmailFreeValidator()]],
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (this.formGroup.valid && this.reCaptchaSuccess) {
      const cmd: CreateNewUserCommand = {
        login: this.formGroup.get('login').value,
        userName: this.formGroup.get('userName').value,
        password: this.formGroup.get('password').value,
        repeatPassword: this.formGroup.get('repeatPassword').value,
        email: this.formGroup.get('email').value,
      };
      this.userService.create(cmd).subscribe(
        (response) => {
          if (response.success) {
            this.showConfirmAccountAlert();
          }
        }
      );
    }
  }

  showConfirmAccountAlert(): void {
    this.dialogService.open(DangerAlertComponent, {
      header: 'Check your email to verify your account',
      width: '50%',
      data: {
        showChooseButtons: false
      }
    });
  }

  forbiddenNameValidator(invalid: boolean): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control ? {loginIsTaken: true} : null;
    };
  }

  isEmailFreeValidator(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(500),
        switchMap(value => this.userService.isEmailFree({email: control.value})),
        map(
          (res: boolean) => {
            if (!res) {
              control.setErrors({emailIsTaken: true});
              // on production un comment this
              // return { emailIsTaken: true };
            }
            return null;
          }
        )).pipe(first());
  }

  isLoginFreeValidator(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(500),
        switchMap(value => this.userService.isLoginFree({login: control.value})),
        map(
          (res: boolean) => {
            if (!res) {
              control.setErrors({loginIsTaken: true});
              return {loginIsTaken: true};
            }
            return null;
          }
        )).pipe(first());
  }

  isUserNameFreeValidator(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(500),
        switchMap(value => this.userService.isUserNameFree({userName: control.value})),
        map(
          (res: boolean) => {
            if (!res) {
              control.setErrors({userNameIsTaken: true});
              return {userNameIsTaken: true};
            }
            return null;
          }
        )).pipe(first());
  }

  passwordsMustBeEquals(textToRepeat: FormControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value !== textToRepeat.value) {
        return {passwordIsNotEquals: true};
      } else {
        return null;
      }
    };
  }

  onEmailChange(): void {
    this.subscriptions.push(
      this.formGroup.get('email').valueChanges.pipe(debounceTime(500)).subscribe(
        (emailInput) => {
          this.userService.isEmailFree({email: emailInput}).subscribe(
            (result) => {
              if (!result) {
                this.formGroup.get('email').setValidators([Validators.maxLength(1)]);
              }
            }
          );
        }
      )
    );
  }

  onRepeatNewPasswordChange(): void {
    this.subscriptions.push(
      this.formGroup.get('repeatPassword').valueChanges.subscribe(
        (value: string) => {
          const newPassword = this.formGroup.get('password') as FormControl;
          const repeatNewPassword = this.formGroup.get('repeatPassword') as FormControl;

          if (repeatNewPassword.value != null && repeatNewPassword.value !== '' && value !== newPassword.value) {
            repeatNewPassword.setValidators([Validators.required, this.passwordsMustBeEquals(newPassword)]);
            repeatNewPassword.updateValueAndValidity({emitEvent: false});
          }
        }
      )
    );
  }

  showResponse(response): void {
    this.userService.verifyCaptcha(response).subscribe(
      (result) => {
        this.reCaptchaSuccess = result.response;
      }
    );
  }
}
