import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateAccountModuleRoutingModule } from './create-account-module-routing.module';
import { CreateAccountComponent } from './create-account/create-account.component';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { SharedModule } from '../../shared/shared/shared.module';
import { CaptchaModule } from 'primeng/captcha';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import {
  RouterModule,
  Routes
} from '@angular/router';


const routes: Routes = [{path: '', component: CreateAccountComponent}];

@NgModule({
  declarations: [
    CreateAccountComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    CreateAccountModuleRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CaptchaModule,
    ButtonModule,
    InputTextModule,
  ]
})
export class CreateAccountModuleModule {
}
