import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { YourPanelRoutingModule } from './your-panel-routing.module';
import { UserPanelComponent } from './your-panel/user-panel.component';
import { TabViewModule } from 'primeng/tabview';
import { WonAuctionsComponent } from './your-panel/won-auctions/won-auctions.component';
import { SoldAuctionsComponent } from './your-panel/sold-auctions/sold-auctions.component';
import { AccountComponent } from './your-panel/account/account.component';
import { SearchCriteriaModule } from '../search-criteria-module/search-criteria.module';
import { AuctionListModule } from '../auction-list-module/auction-list.module';
import { ChangeUserNameComponent } from './your-panel/account/change-user-name/change-user-name.component';
import { ChangePasswordComponent } from './your-panel/account/change-password/change-password.component';
import { AccordionModule } from 'primeng/accordion';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { SharedModule } from '../../shared/shared/shared.module';
import { YourBidsComponent } from './your-panel/your-bids/your-bids.component';


@NgModule({
  declarations: [
    UserPanelComponent,
    WonAuctionsComponent,
    SoldAuctionsComponent,
    AccountComponent,
    ChangeUserNameComponent,
    ChangePasswordComponent,
    YourBidsComponent,
  ],
  imports: [
    CommonModule,
    YourPanelRoutingModule,
    TabViewModule,
    SearchCriteriaModule,
    AuctionListModule,
    AccordionModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
    UserPanelComponent,
  ]
})
export class YourPanelModule {
}
