import {
  Component,
  OnInit
} from '@angular/core';
import { SearchMode } from '../../search-criteria-module/search-criteria-auctions/search-criteria-auctions.component';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {

  searchMode = SearchMode.FIND_BY_CURRENT_USER;

  constructor() {
  }

  ngOnInit(): void {
  }

}
