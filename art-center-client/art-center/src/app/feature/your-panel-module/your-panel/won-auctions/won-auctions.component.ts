import {
  Component,
  OnInit
} from '@angular/core';
import { YchAuctionCardDto } from '../../../../api/dto/YchAuctionCardDto';
import { YchAuctionApiService } from '../../../../api/ych-auction-api.service';
import { GetWonYchsCommand } from '../../../../api/dto/get-won-ychs-command';

@Component({
  selector: 'app-won-auctions',
  templateUrl: './won-auctions.component.html',
  styleUrls: ['./won-auctions.component.css']
})
export class WonAuctionsComponent implements OnInit {

  currentPageNumber: number;
  pageSize = 14;
  auctionCards: YchAuctionCardDto[];

  constructor(
    private ychAuctionApiService: YchAuctionApiService,
  ) {
  }

  // tslint:disable-next-line:variable-name
  _countsOfPages = 1;

  public get countsOfPages(): number {
    return this._countsOfPages;
  }

  ngOnInit(): void {
  }


  searchAuctionCards(event = {rows: this.pageSize, first: this.currentPageNumber}): void {

    this.currentPageNumber = event.first;

    const cmd: GetWonYchsCommand = {
      pageSize: event.rows,
      pageNumber: (this.currentPageNumber / this.pageSize)
    };

    this.ychAuctionApiService.getWonYchAuctions(cmd)
      .subscribe(
        (response) => {
          this.auctionCards = response.cards;
          this._countsOfPages = response.countOfPages;
        }
      );
  }

}
