import {
  Component,
  OnInit
} from '@angular/core';
import { YchAuctionApiService } from '../../../../api/ych-auction-api.service';
import { YchAuctionCardDto } from '../../../../api/dto/YchAuctionCardDto';

@Component({
  selector: 'app-your-bids',
  templateUrl: './your-bids.component.html',
  styleUrls: ['./your-bids.component.css']
})
export class YourBidsComponent implements OnInit {

  currentPageNumber: number;
  pageSize = 14;
  auctionCards: YchAuctionCardDto[];

  constructor(
    private ychAuctionApiService: YchAuctionApiService,
  ) {
  }

  // tslint:disable-next-line:variable-name
  _countsOfPages = 1;

  public get countsOfPages(): number {
    return this._countsOfPages;
  }

  ngOnInit(): void {
  }

  getCurrentUserBids(
    event = {rows: this.pageSize, first: this.currentPageNumber}
  ): void {
    this.currentPageNumber = event.first;

    const currentPageNumber = event.rows;
    const currentPageSize = (this.currentPageNumber / this.pageSize);


    this.ychAuctionApiService.getAuctionsCurrentlyBiddingByUser(currentPageNumber, currentPageSize)
      .subscribe(
        (response) => {
          this.auctionCards = response.cards;
          this._countsOfPages = response.countOfPages;
        }
      );
  }

}
