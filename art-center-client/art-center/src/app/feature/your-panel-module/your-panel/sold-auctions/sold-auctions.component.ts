import {
  Component,
  OnInit
} from '@angular/core';
import { YchAuctionCardDto } from '../../../../api/dto/YchAuctionCardDto';
import { YchAuctionApiService } from '../../../../api/ych-auction-api.service';
import { GetSoldYchsCommand } from '../../../../api/dto/get-sold-ychs-command';

@Component({
  selector: 'app-sold-auctions',
  templateUrl: './sold-auctions.component.html',
  styleUrls: ['./sold-auctions.component.css']
})
export class SoldAuctionsComponent implements OnInit {

  currentPageNumber: number;
  pageSize = 14;
  auctionCards: YchAuctionCardDto[];

  constructor(
    private ychAuctionApiService: YchAuctionApiService,
  ) {
  }

  // tslint:disable-next-line:variable-name
  _countsOfPages = 1;

  public get countsOfPages(): number {
    return this._countsOfPages;
  }

  ngOnInit(): void {
  }

  searchAuctionCards(event = {rows: this.pageSize, first: this.currentPageNumber}): void {

    this.currentPageNumber = event.first;

    const cmd: GetSoldYchsCommand = {
      pageSize: event.rows,
      pageNumber: (this.currentPageNumber / this.pageSize)
    };

    this.ychAuctionApiService.getSoldYchAuctions(cmd)
      .subscribe(
        (response) => {
          this.auctionCards = response.cards;
          this._countsOfPages = response.countOfPages;
        }
      );
  }


}
