import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { BaseComponent } from '../../../../../shared/BaseComponent';
import { UserApiServiceService } from '../../../../../api/user-api-service.service';
import { ChangeUsernameCommand } from '../../../../../api/dto/change-username-command';
import { LocalStorageService } from '../../../../../service/local-storage.service';
import { UserService } from '../../../../../service/user.service';

@Component({
  selector: 'app-change-user-name',
  templateUrl: './change-user-name.component.html',
  styleUrls: ['./change-user-name.component.css']
})
export class ChangeUserNameComponent extends BaseComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userApi: UserApiServiceService,
    private localStorage: LocalStorageService,
    private userService: UserService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initFormGroup();
  }

  initFormGroup(): void {
    this.formGroup = this.fb.group({
      currentPassword: ['', [Validators.required, Validators.maxLength(20)]],
      newUsername: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (this.formGroup.valid) {
      const cmd: ChangeUsernameCommand = {
        newUsername: this.formGroup.get('newUsername').value,
        currentPassword: this.formGroup.get('currentPassword').value,
      };

      this.userApi.changeUsername(cmd).subscribe(
        () => {
          this.userService.updateUserName(cmd.newUsername);
        }
      );
    }
  }

}
