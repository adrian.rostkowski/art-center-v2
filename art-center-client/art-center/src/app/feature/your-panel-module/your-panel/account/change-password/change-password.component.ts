import {
  Component,
  OnInit
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { BaseComponent } from '../../../../../shared/BaseComponent';
import { UserApiServiceService } from '../../../../../api/user-api-service.service';
import { ChangeUserPasswordCommand } from '../../../../../api/dto/change-user-password-command';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent extends BaseComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userApi: UserApiServiceService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initFormGroup();
    this.onRepeatNewPasswordChange();
  }

  initFormGroup(): void {
    this.formGroup = this.fb.group({
      oldPassword: ['', [Validators.maxLength(20)]],
      newPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      repeatNewPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]]
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    if (this.formGroup.valid) {
      const cmd: ChangeUserPasswordCommand = {
        newPassword: this.formGroup.get('newPassword').value,
        repeatNewPassword: this.formGroup.get('repeatNewPassword').value,
        currentPassword: this.formGroup.get('oldPassword').value,
      };

      this.userApi.changePassword(cmd).subscribe(
        () => {
          alert('success!');
        }
      );
    }
  }

  passwordsMustBeEquals(textToRepeat: FormControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value !== textToRepeat.value) {
        return {passwordIsNotEquals: true};
      } else {
        return null;
      }
    };
  }

  onRepeatNewPasswordChange(): void {
    this.subscriptions.push(
      this.formGroup.get('repeatNewPassword').valueChanges.subscribe(
        (value: string) => {
          const newPassword = this.formGroup.get('newPassword') as FormControl;
          const repeatNewPassword = this.formGroup.get('repeatNewPassword') as FormControl;

          if (repeatNewPassword.value != null && repeatNewPassword.value !== '' && value !== newPassword.value) {
            repeatNewPassword.setValidators([Validators.required, this.passwordsMustBeEquals(newPassword)]);
            repeatNewPassword.updateValueAndValidity({emitEvent: false});
          }
        }
      )
    );
  }

}
