import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchCriteriaRoutingModule } from './search-criteria-routing.module';
import {AuctionListModule} from '../auction-list-module/auction-list.module';
import {SearchCriteriaAuctionsComponent} from './search-criteria-auctions/search-criteria-auctions.component';
import {AccordionModule} from 'primeng/accordion';
import {SelectButtonModule} from 'primeng/selectbutton';
import {FormsModule} from '@angular/forms';
import {InputNumberModule} from 'primeng/inputnumber';
import {SliderModule} from 'primeng/slider';
import {ButtonModule} from 'primeng/button';


@NgModule({
  declarations: [SearchCriteriaAuctionsComponent],
  imports: [
    CommonModule,
    SearchCriteriaRoutingModule,
    AuctionListModule,
    AccordionModule,
    SelectButtonModule,
    FormsModule,
    InputNumberModule,
    SliderModule,
    ButtonModule
  ], exports: [
    SearchCriteriaAuctionsComponent
  ]
})
export class SearchCriteriaModule { }
