import {Component, Input, OnInit} from '@angular/core';
import {YchAuctionCardDto} from '../../../api/dto/YchAuctionCardDto';
import {SelectOption} from '../../../shared/select-option';
import {GetYchMainPageCommand} from '../../../api/dto/get-ych-main-page-command';
import {YchAuctionApiService} from '../../../api/ych-auction-api.service';
import {SearchCriteriaModel, SearchCriteriaStateService} from '../../../service/search-criteria-state.service';

@Component({
  selector: 'app-search-criteria-auctions',
  templateUrl: './search-criteria-auctions.component.html',
  styleUrls: ['./search-criteria-auctions.component.css']
})
export class SearchCriteriaAuctionsComponent implements OnInit {

  @Input() mode: SearchMode;
  @Input() viewMode: ViewMode;
  @Input() artistName: string;

  selectedOptionsModel: SearchCriteriaModel;

  // pageSize = 25;
  currentPageNumber: number;

  auctionCards: YchAuctionCardDto[];

  oneCriteriaInputSize = 4;

  // tslint:disable-next-line:variable-name
  _countsOfPages = 1;
  public get countsOfPages(): number {
    return this._countsOfPages;
  }

  constructor(
    private ychAuctionApiService: YchAuctionApiService,
    public stateService: SearchCriteriaStateService,
  ) { }

  ngOnInit(): void {
    this.selectedOptionsModel = this.stateService.getModel();
    this.setUpAuctionStepOptions();
  }

  isViewModeEqualsTo(mode: string): boolean {
    return this.viewMode === mode;
  }

  private setUpAuctionStepOptions(): void {
    if (this.mode === SearchMode.FIND_BY_SPECIFIC_ARTIST) {
      this.stateService.getModelToSelect().stepOptions = [
        {name: 'BIDDING', value: 'BIDDING'},
        {name: 'CLOSED', value: 'CLOSED'},
        {name: 'ALL', value: 'ALL'}
      ];
    } else {
      this.stateService.getModelToSelect().stepOptions = [
        {name: 'BIDDING', value: 'BIDDING'},
        {name: 'CONSULTATIONS', value: 'CONSULTATIONS'},
        {name: 'CLOSED', value: 'CLOSED'},
        {name: 'ALL', value: 'ALL'}
      ];
    }
  }

  searchAuctionCards(event = { rows: this.selectedOptionsModel.pageSize, first: this.currentPageNumber }): void {

    this.currentPageNumber = event.first;

    const cmd: GetYchMainPageCommand = {
      auctionStep: this.getAuctionStep(),
      daysToEnd: this.selectedOptionsModel.daysToEndSlider,
      priceRangeFrom: this.sendPriceRangeToServer() ? this.selectedOptionsModel.rangeValues[0] : null,
      priceRangeTo: this.sendPriceRangeToServer() ?  this.selectedOptionsModel.rangeValues[1] : null,
      ychType: this.selectedOptionsModel.selectedYchTypeOption.value,
      ychCharacterType: this.selectedOptionsModel.selectedYchCharacterTypeOption.value,
      findByCurrentUser: this.mode === SearchMode.FIND_BY_CURRENT_USER,
      ychStatus: this.getAuctionStatus(),
      userName: this.artistName,
      auctionKind: this.selectedOptionsModel.selectedAuctionKindOption.value !== 'ALL' ?
        this.selectedOptionsModel.selectedAuctionKindOption.value : null,
      orderBy: ['endDate'],
      orderDirection: 'ASC',
      pageSize: event.rows,
      pageNumber: (this.currentPageNumber / this.selectedOptionsModel.pageSize)
    };

    this.ychAuctionApiService.getYchAuctionsByCriteriaQuery(cmd)
      .subscribe(
        (response) => {
          this.auctionCards = response.cards;
          this._countsOfPages = response.countOfPages;
        }
      );
  }

  onAuctionStepChange(event): void {
    if (event.value.value !== 'BIDDING') {
      this.selectedOptionsModel.daysToEndSlider = null;
    } else {
      this.selectedOptionsModel.selectedYchOption = {name: 'All', value: 'ALL'};
      this.selectedOptionsModel.daysToEndSlider = 7;
    }
  }

  onYchOptionChange(event): void {
    // if ((event.value.value !== 'ALL' && event.value.value !== 'ART_TRADE') && this.selectedStepOption.value === 'BIDDING') {
    //   this.selectedStepOption = {name: 'ALL', value: 'ALL'};
    // }
  }

  sendPriceRangeToServer(): boolean {
    return this.selectedOptionsModel.selectedAuctionKindOption.value !== 'ALL'
           && this.selectedOptionsModel.selectedAuctionKindOption.value !== 'ART_TRADE';
  }

  onAuctionKindChange(event): boolean {
    return event.value.value !== 'ART_TRADE';
  }

  isCurrentStepEqualsTo(step: string): boolean {
    return this.selectedOptionsModel.selectedStepOption.value === step;
  }

  currentModeIsEqualTo(mode: string): boolean {
    return this.mode === mode;
  }

  showPriceRange(): boolean {
    return this.selectedOptionsModel.selectedAuctionKindOption.value !== 'ART_TRADE'
      && this.selectedOptionsModel.selectedAuctionKindOption.value !== 'ALL';
  }

  getAuctionStep(): string {
    if (this.viewMode === 'BASIC') {
      return 'BIDDING';
    }
    return this.selectedOptionsModel.selectedStepOption.value !== 'ALL' ? this.selectedOptionsModel.selectedStepOption.value : null;
  }

  getAuctionStatus(): string {
    if (this.viewMode === 'BASIC') {
      return null;
    }
    return this.selectedOptionsModel.selectedYchOption.value;
  }

  onAuctionStatusChange(event): void {
    if (event.value.value !== 'ALL') {
      this.selectedOptionsModel.selectedStepOption = {name: 'ALL', value: 'ALL'};
    }
  }

}

export enum SearchMode {
  FIND_BY_ALL= 'FIND_BY_ALL',
  FIND_BY_CURRENT_USER = 'FIND_BY_CURRENT_USER',
  FIND_BY_SPECIFIC_ARTIST = 'FIND_BY_SPECIFIC_ARTIST'
}

export enum ViewMode {
  BASIC= 'BASIC',
  YOUR_PANEL = 'YOUR_PANEL',
}
