import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchCriteriaAuctionsComponent} from "./search-criteria-auctions/search-criteria-auctions.component";


const routes: Routes = [{ path: '', component: SearchCriteriaAuctionsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchCriteriaRoutingModule { }
