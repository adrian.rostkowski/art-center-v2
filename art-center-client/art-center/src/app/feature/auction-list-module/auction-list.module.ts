import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuctionListRoutingModule } from './auction-list-routing.module';
import { AuctionListComponent } from './auction-list/auction-list.component';
import { AuctionItemComponent } from './auction-list/auction-item/auction-item.component';
import { DataViewModule } from 'primeng/dataview';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [
    AuctionListComponent,
    AuctionItemComponent,
  ],
  imports: [
    CommonModule,
    AuctionListRoutingModule,
    DataViewModule,
    ButtonModule,
  ], exports: [
    AuctionListComponent
  ]
})
export class AuctionListModule {
}
