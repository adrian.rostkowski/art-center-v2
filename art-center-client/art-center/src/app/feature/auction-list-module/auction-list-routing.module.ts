import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';
import { AuctionListComponent } from './auction-list/auction-list.component';

const routes: Routes = [{path: '', component: AuctionListComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuctionListRoutingModule {
}
