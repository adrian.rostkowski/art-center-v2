import {
  Component,
  HostListener,
  Input,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { YchAuctionCardDto } from 'src/app/api/dto/YchAuctionCardDto';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-auction-item',
  templateUrl: './auction-item.component.html',
  styleUrls: ['./auction-item.component.css']
})
export class AuctionItemComponent implements OnInit {

  @Input() ych: YchAuctionCardDto;
  daysToEnd: string;
  isMobile = false;
  serverUrl = environment.serverUrl;
  private mobileWidth = 700;

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.setUpIsMobile();
    this.setDaysToEnd();
  }

  setDaysToEnd(): void {
    this.ych.daysToEnd > 0 ? this.daysToEnd = this.ych.daysToEnd.toString() : this.daysToEnd = 'today';
  }

  navigateToDetailsFromCard(): void {
    if (this.isAuctionOnConsultationStatus()) {
      return;
    }
    this.router.navigate(['/ych-details', this.ych.ychId]);
  }

  navigateToDetailsFromButton(): void {
    this.router.navigate(['/ych-details', this.ych.ychId]);
  }

  navigateToAuctionDiscussion(): void {
    this.router.navigate(['/auction-discussion', this.ych.ychId]);
  }

  isAuctionOnConsultationStatus(): boolean {
    return this.ych.auctionStatus === 'CONSULTATIONS';
  }

  isAuctionOnClosedStatus(): boolean {
    return this.ych.auctionStatus === 'CLOSED';
  }

  isAuctionNotOnBiddingStatus(): boolean {
    return this.ych.auctionStatus !== 'BIDDING';
  }

  isCurrentStepEquals(step: string): boolean {
    return this.ych.auctionStatus === step;
  }

  @HostListener('window:resize', ['$event'])
  changeIsMobileOnResize(event): void {
    if (event.target.innerWidth > this.mobileWidth) {
      this.isMobile = false;
    } else {
      this.isMobile = true;
    }
  }

  getMobileClassIfNeeded(className: string): string {
    if (this.isMobile) {
      return className + '-mobile';
    } else {
      return className;
    }
  }

  getMobileClassIfNeededAndColoredTag(className: string, daysToEnd: string): string {
    if (this.isMobile) {
      return className + '-mobile' + this.getColorByDaysToEnd(daysToEnd);
    } else {
      return className + this.getColorByDaysToEnd(daysToEnd);
    }
  }

  getColorByDaysToEnd(daysToEndString: string): string {
    const daysToEnd: number = +daysToEndString;
    if (daysToEnd >= 5) {
      return ' blue';
    } else if (daysToEnd >= 3) {
      return ' yellow';
    } else {
      return ' red';
    }
  }

  setUpIsMobile(): void {
    window.innerWidth > this.mobileWidth ? this.isMobile = false : this.isMobile = true;
  }

}
