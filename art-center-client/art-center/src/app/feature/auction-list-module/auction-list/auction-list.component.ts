import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { YchAuctionCardDto } from 'src/app/api/dto/YchAuctionCardDto';

@Component({
  selector: 'app-auction-list',
  templateUrl: './auction-list.component.html',
  styleUrls: ['./auction-list.component.css']
})
export class AuctionListComponent implements OnInit {

  isMobile = false;
  @Input() auctionCards: YchAuctionCardDto[];
  @Input() pageSize: number;
  @Input() countOfPages: number;
  @Output() loadData = new EventEmitter<any>();
  private mobileWidth = 700;

  constructor() {
  }

  ngOnInit(): void {
    this.setUpIsMobile();
  }

  loadDataEmit(event): void {
    this.loadData.emit(event);
  }

  @HostListener('window:resize', ['$event'])
  changeIsMobileOnResize(event): void {
    if (event.target.innerWidth > this.mobileWidth) {
      this.isMobile = false;
    } else {
      this.isMobile = true;
    }
  }

  setUpIsMobile(): void {
    window.innerWidth > this.mobileWidth ? this.isMobile = false : this.isMobile = true;
  }

}
