import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import { CreateYchComponent } from './create-ych/create-ych.component';

const routes: Routes = [{path: '', component: CreateYchComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateAuctionRoutingModule {
}
