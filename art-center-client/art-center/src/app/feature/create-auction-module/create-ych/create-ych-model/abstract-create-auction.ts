import {
  FormBuilder,
  FormControl,
  FormGroup
} from '@angular/forms';
import { BaseComponent } from '../../../../shared/BaseComponent';
import { AuctionKind } from '../../../../api/dto/enum/auction-kind.enum';
import { CurrentStepView } from '../current-step-view.enum';

export abstract class AbstractCreateAuction extends BaseComponent {

  formGroup: FormGroup;
  steps: Step[];
  auctionKind: AuctionKind;

  protected constructor(
    protected fb: FormBuilder
  ) {
    super();
  }

  init(): void {
    this.setAuctionKind();
    this.initForm();
    this.initElementsValue();
    this.initSubscriptions();
    this.initSteps();
  }

  getFormGroup(): FormGroup {
    return this.formGroup;
  }

  abstract initSubscriptions(): void;

  abstract initForm(): void;

  abstract initElementsValue(): void;

  abstract setAuctionKind(): void;

  abstract initSteps(): void;

  abstract validateSteps(): void;

  abstract onSubmit(): void;
}

export interface Step {
  stepType: CurrentStepView;
  name: string;
  controls: FormControl[];
}
