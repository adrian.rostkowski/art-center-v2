import { AbstractCreateAuction } from './abstract-create-auction';
import { AuctionKind } from '../../../../api/dto/enum/auction-kind.enum';
import { DropdownElement } from '../../../../shared/shared/control-change-data/control-change-data.component';
import { YchAuctionApiService } from '../../../../api/ych-auction-api.service';
import { Router } from '@angular/router';
import { DialogService } from 'primeng/dynamicdialog';
import {
  FormControl,
  Validators
} from '@angular/forms';
import { CreateYchAuctionCommand } from '../../../../api/dto/CreateYchAuctionCommand';
import { YchAuctionDto } from '../../../../api/dto/YchAuctionDto';
import { DangerAlertComponent } from '../../../../shared/danger-alert/danger-alert.component';
import { CurrentStepView } from '../current-step-view.enum';

export class CreateArtTradeAuction extends AbstractCreateAuction {

  auctionKind: AuctionKind = AuctionKind.ART_TRADE;

  constructor(
    props,
    private ychAuctionApiService: YchAuctionApiService,
    private router: Router,
    public dialogService: DialogService,
  ) {
    super(props);
  }

  // tslint:disable-next-line:variable-name
  _showMaxBid = false;

  get showMaxBid(): boolean {
    return this._showMaxBid;
  }

  // tslint:disable-next-line:variable-name
  _countsOfDay: DropdownElement[] = [];

  get countsOfDay(): DropdownElement[] {
    return this._countsOfDay;
  }

  initElementsValue(): void {
    this.initDropdownElements();
  }

  initDropdownElements(): void {
    this._countsOfDay = [
      {name: '1', code: '1'},
      {name: '2', code: '2'},
      {name: '3', code: '3'},
      {name: '4', code: '4'},
      {name: '5', code: '5'},
      {name: '6', code: '6'},
      {name: '7', code: '7'},
    ];

    this.formGroup.get('countOfDays').setValue(this._countsOfDay[0]);
  }

  initForm(): void {
    this.formGroup = this.fb.group({
      description: ['', []],
      countOfDays: ['', [Validators.required]],
      file: ['', [Validators.required]],
      supportFile1: [''],
      supportFile2: [''],
      supportFile3: [''],
      ychType: ['', [Validators.required]],
      characterType: ['', [Validators.required]],
    });
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    this.validateSteps();

    if (!this.formGroup.get('file').valid) {
      this.showAlert();
    }

    if (this.formGroup.valid) {
      const cmd: CreateYchAuctionCommand = {
        description: this.formGroup.get('description').value,
        minBid: null,
        maxBid: null,
        isMaxBid: null,
        countOfDays: this.formGroup.get('countOfDays').value.code,
        ychAuctionType: this.formGroup.get('ychType').value.value,
        ychAuctionCharacterType: this.formGroup.get('characterType').value.value,
        auctionKind: this.auctionKind,
        bidCurrency: null
      };

      const images = [
        this.formGroup.get('file').value,
        this.formGroup.get('supportFile1')?.value,
        this.formGroup.get('supportFile2')?.value,
        this.formGroup.get('supportFile3')?.value,
      ];

      this.ychAuctionApiService.create(cmd, images)
        .subscribe(
          (response) => {
            const ychId = response.response as YchAuctionDto;
            this.router.navigate(['/ych-details', ychId.ychAuctionId]);
          }
        );
    }
  }

  showAlert(): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'Art image is required',
      width: '50%',
      data: {
        showChooseButtons: false
      }
    });
  }

  initSubscriptions(): void {
  }

  initSteps(): void {
    this.steps = [
      {
        stepType: CurrentStepView.AUCTION_CATEGORY_CHARACTER_TYPE,
        name: CurrentStepView.AUCTION_CATEGORY_CHARACTER_TYPE.toString(),
        controls: [
          this.formGroup.get('characterType') as FormControl,
          this.formGroup.get('ychType') as FormControl,
        ]
      },
      {
        stepType: CurrentStepView.IMAGE,
        name: CurrentStepView.IMAGE.toString(),
        controls: [
          this.formGroup.get('file') as FormControl,
          this.formGroup.get('supportFile1') as FormControl,
          this.formGroup.get('supportFile2') as FormControl,
          this.formGroup.get('supportFile3') as FormControl,
        ]
      },
      {
        stepType: CurrentStepView.DAYS_DESCRIPTION,
        name: CurrentStepView.DAYS_DESCRIPTION.toString(),
        controls: [
          this.formGroup.get('countOfDays') as FormControl,
          this.formGroup.get('description') as FormControl,
        ]
      },
    ];
  }

  validateSteps(): void {
    const invalidSteps: string[] = [];
    this.steps.forEach(n => {
      n.controls.forEach(m => {
        if (!m.valid) {
          invalidSteps.push(CurrentStepView[n.name]);
        }
      });
    });

    if (invalidSteps.length > 0) {
      this.showStepAlert(invalidSteps[0]);
    }
  }

  showStepAlert(stepName: string): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'An error occurred on ' + stepName + ' step',
      width: '50%',
      data: {
        showChooseButtons: false
      }
    });
  }

  setAuctionKind(): void {
    this.auctionKind = AuctionKind.ART_TRADE;
  }
}
