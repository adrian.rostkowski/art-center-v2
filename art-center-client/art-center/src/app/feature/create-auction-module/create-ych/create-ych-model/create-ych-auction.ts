import { AbstractCreateAuction } from './abstract-create-auction';
import {
  FormControl,
  Validators
} from '@angular/forms';
import { CreateYchAuctionCommand } from '../../../../api/dto/CreateYchAuctionCommand';
import { YchAuctionDto } from '../../../../api/dto/YchAuctionDto';
import { AuctionKind } from '../../../../api/dto/enum/auction-kind.enum';
import { YchAuctionApiService } from '../../../../api/ych-auction-api.service';
import { Router } from '@angular/router';
import { DangerAlertComponent } from '../../../../shared/danger-alert/danger-alert.component';
import { DialogService } from 'primeng/dynamicdialog';
import { DropdownElement } from '../../../../shared/shared/control-change-data/control-change-data.component';
import { CurrentStepView } from '../current-step-view.enum';

export class CreateYchAuction extends AbstractCreateAuction {

  auctionKind: AuctionKind = AuctionKind.YCH;

  constructor(
    props,
    private ychAuctionApiService: YchAuctionApiService,
    private router: Router,
    public dialogService: DialogService,
  ) {
    super(props);
  }

  // tslint:disable-next-line:variable-name
  _showMaxBid = false;

  get showMaxBid(): boolean {
    return this._showMaxBid;
  }

  // tslint:disable-next-line:variable-name
  _countsOfDay: DropdownElement[] = [];

  get countsOfDay(): DropdownElement[] {
    return this._countsOfDay;
  }


  initForm(): void {
    this.formGroup = this.fb.group({
      description: ['', []],
      minBid: ['', [Validators.required]],
      maxBid: ['', [Validators.required]],
      isMaxBid: ['', [Validators.required]],
      countOfDays: ['', [Validators.required]],
      file: ['', [Validators.required]],
      ychType: ['', [Validators.required]],
      characterType: ['', [Validators.required]],
      bidCurrency: ['', [Validators.required]],
    });
  }

  initElementsValue(): void {
    this.initDropdownElements();
    this.initIsMaxBid();
    this.initMinBid();
  }

  onSubmit(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAllAsTouched();
    this.formGroup.markAsDirty();
    const formControls = this.formGroup.controls;

    // tslint:disable-next-line: forin
    for (const key in formControls) {
      this.formGroup.get(key).markAsTouched();
      this.formGroup.get(key).markAsDirty();
    }

    this.validateSteps();

    if (!this.formGroup.get('file').valid) {
      this.showImgAlert();
    }

    if (this.formGroup.valid) {
      const cmd: CreateYchAuctionCommand = {
        description: this.formGroup.get('description').value,
        minBid: this.formGroup.get('minBid').value,
        maxBid: this.formGroup.get('maxBid').value,
        isMaxBid: this.formGroup.get('isMaxBid').value,
        countOfDays: this.formGroup.get('countOfDays').value.code,
        ychAuctionType: this.formGroup.get('ychType').value.value,
        ychAuctionCharacterType: this.formGroup.get('characterType').value.value,
        auctionKind: this.auctionKind,
        bidCurrency: this.formGroup.get('bidCurrency').value.value
      };

      const images = [this.formGroup.get('file').value];

      this.ychAuctionApiService.create(cmd, images)
        .subscribe(
          (response) => {
            const ychId = response.response as YchAuctionDto;
            this.router.navigate(['/ych-details', ychId.ychAuctionId]);
          }
        );
    }
  }

  initSubscriptions(): void {
    this.chnageIsMaxBid();
    this.updateMaxBidBasedOnMinBid();
  }

  initSteps(): void {
    this.steps = [
      {
        stepType: CurrentStepView.PRICE_CURRENCY,
        name: CurrentStepView.PRICE_CURRENCY.toString(),
        controls: [
          this.formGroup.get('maxBid') as FormControl,
          this.formGroup.get('minBid') as FormControl,
          this.formGroup.get('isMaxBid') as FormControl,
          this.formGroup.get('bidCurrency') as FormControl,
        ]
      },
      {
        stepType: CurrentStepView.AUCTION_CATEGORY_CHARACTER_TYPE,
        name: CurrentStepView.AUCTION_CATEGORY_CHARACTER_TYPE.toString(),
        controls: [
          this.formGroup.get('characterType') as FormControl,
          this.formGroup.get('ychType') as FormControl,
        ]
      },
      {
        stepType: CurrentStepView.IMAGE,
        name: CurrentStepView.IMAGE.toString(),
        controls: [
          this.formGroup.get('file') as FormControl,
        ]
      },
      {
        stepType: CurrentStepView.DAYS_DESCRIPTION,
        name: CurrentStepView.DAYS_DESCRIPTION.toString(),
        controls: [
          this.formGroup.get('countOfDays') as FormControl,
          this.formGroup.get('description') as FormControl,
        ]
      },
    ];
  }

  validateSteps(): void {
    const invalidSteps: string[] = [];
    this.steps.forEach(n => {
      n.controls.forEach(m => {
        if (!m.valid) {
          invalidSteps.push(CurrentStepView[n.name]);
        }
      });
    });

    if (invalidSteps.length > 0) {
      this.showStepAlert(invalidSteps[0]);
    }
  }

  chnageIsMaxBid(): void {
    this.subscriptions.push(
      this.formGroup.get('isMaxBid').valueChanges.subscribe(
        (value: boolean) => {
          const control = this.formGroup.get('maxBid');

          this._showMaxBid = value;

          if (value) {
            control.setValidators([Validators.required]);
          } else {
            control.setValidators(null);
            control.setValue(null);
          }

          control.updateValueAndValidity();
        }
      )
    );
  }

  updateMaxBidBasedOnMinBid(): void {
    this.subscriptions.push(
      this.formGroup.get('minBid').valueChanges.subscribe(
        (value: number) => {
          const control = this.formGroup.get('maxBid');

          if (value >= control.value) {
            control.setValue(value + 1);
          }

          control.updateValueAndValidity();
        }
      )
    );
  }

  initIsMaxBid(): void {
    this.formGroup.get('isMaxBid').setValue(false);
  }

  initMinBid(): void {
    this.formGroup.get('minBid').setValue(0);
  }

  showImgAlert(): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'Art image is required',
      width: '50%',
      data: {
        showChooseButtons: false
      }
    });
  }

  // TODO ADD CUSTOM TRANSLACTIONS
  showStepAlert(stepName: string): void {
    const ref = this.dialogService.open(DangerAlertComponent, {
      header: 'An error occurred on ' + stepName + ' step',
      width: '50%',
      data: {
        showChooseButtons: false
      }
    });
  }

  initDropdownElements(): void {
    this._countsOfDay = [
      {name: '1', code: '1'},
      {name: '2', code: '2'},
      {name: '3', code: '3'},
      {name: '4', code: '4'},
      {name: '5', code: '5'},
      {name: '6', code: '6'},
      {name: '7', code: '7'},
    ];

    this.formGroup.get('countOfDays').setValue(this._countsOfDay[0]);
  }

  setAuctionKind(): void {
    this.auctionKind = AuctionKind.YCH;
  }
}
