import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { BaseComponent } from 'src/app/shared/BaseComponent';
import { YchAuctionApiService } from 'src/app/api/ych-auction-api.service';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import { DialogService } from 'primeng/dynamicdialog';
import { AuctionKind } from '../../../api/dto/enum/auction-kind.enum';
import { CreateYchAuction } from './create-ych-model/create-ych-auction';
import { CreateAdoptAuction } from './create-ych-model/create-adopt-auction';
import { CreateArtTradeAuction } from './create-ych-model/create-art-trade-auction';
import { MenuItem } from 'primeng/api';
import { CurrentStepView } from './current-step-view.enum';

@Component({
  selector: 'app-create-ych',
  templateUrl: './create-ych.component.html',
  styleUrls: ['./create-ych.component.css'],
  providers: [DialogService]
})
export class CreateYchComponent extends BaseComponent implements OnInit {

  steps: MenuItem[];
  currentStepView = 0;

  auctionKind: AuctionKind = AuctionKind.ADOPT;
  formGroup: FormGroup;

  showMaxBid = false;

  uploadedFiles: any[] = [];

  ychTypeOptions: Option[];
  // selectedYchTypeOption = {name: 'Human', value: 'HUMAN'};

  ychCharacterTypeOptions: Option[];
  // selectedYchCharacterTypeOption = {name: 'Human', value: 'HUMAN'};

  bidCurrencyOptions: Option[];

  // todo change this union to abstract class
  createAuctionImp: CreateYchAuction | CreateAdoptAuction | CreateArtTradeAuction;

  showSubmitButton = false;

  constructor(
    private fb: FormBuilder,
    private ychAuctionApiService: YchAuctionApiService,
    private router: Router,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
  ) {
    super();
  }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe(params => {
      this.auctionKind = params.get('auctionKind') as any as AuctionKind;
      switch (this.auctionKind) {
        case AuctionKind.YCH: {
          this.createAuctionImp = new CreateYchAuction(this.fb, this.ychAuctionApiService, this.router, this.dialogService);
          this.createAuctionImp.init();
          this.formGroup = this.createAuctionImp.getFormGroup();
          break;
        }
        case AuctionKind.ADOPT: {
          this.createAuctionImp = new CreateAdoptAuction(this.fb, this.ychAuctionApiService, this.router, this.dialogService);
          this.createAuctionImp.init();
          this.formGroup = this.createAuctionImp.getFormGroup();
          break;
        }
        case AuctionKind.ART_TRADE: {
          this.createAuctionImp = new CreateArtTradeAuction(this.fb, this.ychAuctionApiService, this.router, this.dialogService);
          this.createAuctionImp.init();
          this.formGroup = this.createAuctionImp.getFormGroup();
          break;
        }
      }

      this.initSteps();
    });

    this.ychTypeOptions = [
      {name: 'Safe', value: 'SAFE'},
      {name: 'Explicit', value: 'EXPLICIT'},
      {name: 'NSFW', value: 'NOT_SAFE_FOR_WORK'}
    ];

    this.ychCharacterTypeOptions = [
      {name: 'Human', value: 'HUMAN'},
      {name: 'Pony', value: 'PONY'},
      {name: 'Furry', value: 'FURRY'}
    ];

    this.bidCurrencyOptions = [
      {name: 'USD', value: 'USD'},
      {name: 'DeviantArt points', value: 'DEVIANT_ART_POINTS'},
    ];
  }

  initSteps(): void {
    if (this.createAuctionImp.auctionKind === AuctionKind.ART_TRADE) {
      this.createStepsImgAuction();
    } else {
      this.createStepsForCurrencyAuction();
    }
  }

  onSubmit(): void {
    this.createAuctionImp.onSubmit();
    // for (let i = 0; i < 100; i++) {
    //   this.createAuctionImp.onSubmit();
    // }
  }

  onUpload(files: FileList): void {
    const file = files.item(0);
    const control = this.formGroup.get('file');
    control.setValue(file);
  }

  onUploadSupportImg(files: FileList, supportNumber: string): void {
    const file = files.item(0);
    const control = this.formGroup.get('supportFile' + supportNumber);
    control.setValue(file);
  }

  supportImagesAreAvailable(): boolean {
    return this.auctionKind === AuctionKind.ADOPT || this.auctionKind === AuctionKind.ART_TRADE;
  }

  currencyBidsAreAvailable(): boolean {
    return this.auctionKind === AuctionKind.ADOPT || this.auctionKind === AuctionKind.YCH;
  }

  isCurrentStepEqualsTo(step: string): boolean {
    return this.createAuctionImp.steps[this.currentStepView].stepType === CurrentStepView[step];
    // return false;
  }

  nextView(): void {
    const nextStep = this.currentStepView + 1;
    if (nextStep < this.createAuctionImp.steps.length) {
      this.currentStepView = nextStep;
    }
  }

  previousView(): void {
    const nextStep = this.currentStepView + -1;
    if (nextStep >= 0) {
      this.currentStepView = nextStep;
    }
  }

  setSubmitVisibleIfNeeded(): boolean {
    if (this.showSubmitButton === false && this.currentStepView === this.steps.length - 1) {
      this.showSubmitButton = true;
      return true;
    }
    return this.showSubmitButton;
  }

  private setStepView(step: CurrentStepView): void {
    this.currentStepView = step;
  }

  private createStepsForCurrencyAuction(): void {
    this.steps = [
      {label: 'Price & Currency', command: () => this.setStepView(0)},
      {label: 'Category', command: () => this.setStepView(1),},
      {label: 'Image', command: () => this.setStepView(2)},
      {label: 'Days & Description', command: () => this.setStepView(3)},
    ];

    this.currentStepView = 0;
  }

  private createStepsImgAuction(): void {
    this.steps = [
      {label: 'Category', command: () => this.setStepView(0),},
      {label: 'Image', command: () => this.setStepView(1)},
      {label: 'Days & Description', command: () => this.setStepView(2)},
    ];

    this.currentStepView = 0;
  }

}

interface Option {
  name: string;
  value: string;
}

// type CurrentStepView = 'PRICE_CURRENCY' | 'AUCTION_CATEGORY_CHARACTER_TYPE' | 'IMAGE' | 'DAYS_DESCRIPTION' | '';
