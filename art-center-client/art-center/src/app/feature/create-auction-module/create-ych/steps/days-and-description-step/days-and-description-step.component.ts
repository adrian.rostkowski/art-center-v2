import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { CreateYchAuction } from '../../create-ych-model/create-ych-auction';
import { CreateAdoptAuction } from '../../create-ych-model/create-adopt-auction';
import { CreateArtTradeAuction } from '../../create-ych-model/create-art-trade-auction';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-days-and-description-step',
  templateUrl: './days-and-description-step.component.html',
  styleUrls: ['./days-and-description-step.component.css']
})
export class DaysAndDescriptionStepComponent implements OnInit {

  @Input() createAuctionImp: CreateYchAuction | CreateAdoptAuction | CreateArtTradeAuction;
  @Input() formGroup: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

}
