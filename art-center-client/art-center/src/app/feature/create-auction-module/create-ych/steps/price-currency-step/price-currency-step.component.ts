import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CreateYchAuction } from '../../create-ych-model/create-ych-auction';
import { CreateAdoptAuction } from '../../create-ych-model/create-adopt-auction';
import { CreateArtTradeAuction } from '../../create-ych-model/create-art-trade-auction';
import { Option } from '@angular/cli/models/interface';
import { AuctionKind } from '../../../../../api/dto/enum/auction-kind.enum';

@Component({
  selector: 'app-price-currency-step',
  templateUrl: './price-currency-step.component.html',
  styleUrls: ['./price-currency-step.component.css']
})
export class PriceCurrencyStepComponent implements OnInit {

  @Input() formGroup: FormGroup;
  @Input() createAuctionImp: CreateYchAuction | CreateAdoptAuction | CreateArtTradeAuction;
  @Input() bidCurrencyOptions: Option[];
  @Input() auctionKind: AuctionKind;

  constructor() {
  }

  ngOnInit(): void {
  }

  currencyBidsAreAvailable(): boolean {
    return this.auctionKind === AuctionKind.ADOPT || this.auctionKind === AuctionKind.YCH;
  }

}
