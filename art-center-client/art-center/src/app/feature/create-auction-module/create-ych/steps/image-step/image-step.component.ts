import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { AuctionKind } from '../../../../../api/dto/enum/auction-kind.enum';

@Component({
  selector: 'app-image-step',
  templateUrl: './image-step.component.html',
  styleUrls: ['./image-step.component.css']
})
export class ImageStepComponent implements OnInit {

  @Input() auctionKind: AuctionKind;
  @Output() fileAdded = new EventEmitter<any>();
  @Output() supportImgAdded = new EventEmitter<SupportItem>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onUpload(files): void {
    this.fileAdded.emit(files);
  }

  onUploadSupportImg(files, supportImgId: string): void {
    this.supportImgAdded.emit({files, supportImgId});
  }

  supportImagesAreAvailable(): boolean {
    return this.auctionKind === AuctionKind.ADOPT || this.auctionKind === AuctionKind.ART_TRADE;
  }

}

export interface SupportItem {
  files: any;
  supportImgId: string;
}
