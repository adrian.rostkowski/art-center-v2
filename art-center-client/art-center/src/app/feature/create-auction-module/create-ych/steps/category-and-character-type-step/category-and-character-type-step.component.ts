import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-category-and-character-type-step',
  templateUrl: './category-and-character-type-step.component.html',
  styleUrls: ['./category-and-character-type-step.component.css']
})
export class CategoryAndCHaracterTypeStepComponent implements OnInit {

  @Input() ychTypeOptions: Option[];
  @Input() formGroup: FormGroup;
  @Input() ychCharacterTypeOptions: Option[];

  constructor() {
  }

  ngOnInit(): void {
  }

}

interface Option {
  name: string;
  value: string;
}
