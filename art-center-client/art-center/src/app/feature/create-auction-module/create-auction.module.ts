import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateAuctionRoutingModule } from './create-auction-routing.module';
import { CreateYchComponent } from './create-ych/create-ych.component';
import { PanelModule } from 'primeng/panel';
import { StepsModule } from 'primeng/steps';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { PriceCurrencyStepComponent } from './create-ych/steps/price-currency-step/price-currency-step.component';
import { DaysAndDescriptionStepComponent } from './create-ych/steps/days-and-description-step/days-and-description-step.component';
import { ImageStepComponent } from './create-ych/steps/image-step/image-step.component';
import { CategoryAndCHaracterTypeStepComponent } from './create-ych/steps/category-and-character-type-step/category-and-character-type-step.component';
import { SharedModule } from '../../shared/shared/shared.module';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '../../app.module';
import { TooltipModule } from 'primeng/tooltip';


@NgModule({
  declarations: [
    CreateYchComponent,
    PriceCurrencyStepComponent,
    CategoryAndCHaracterTypeStepComponent,
    ImageStepComponent,
    DaysAndDescriptionStepComponent,
  ],
  exports: [
    PriceCurrencyStepComponent,
    CategoryAndCHaracterTypeStepComponent,
    ImageStepComponent,
    DaysAndDescriptionStepComponent,
  ],
  imports: [
    CommonModule,
    CreateAuctionRoutingModule,
    PanelModule,
    StepsModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule,
    TooltipModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ]
})
export class CreateAuctionModule {
}
