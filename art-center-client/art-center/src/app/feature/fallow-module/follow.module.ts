import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FollowRoutingModule } from './follow-routing.module';
import { FollowButtonComponent } from './follow-button/follow-button.component';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [FollowButtonComponent],
  imports: [
    CommonModule,
    FollowRoutingModule,
    ButtonModule,
  ],
  exports: [
    FollowButtonComponent
  ]
})
export class FollowModule {
}
