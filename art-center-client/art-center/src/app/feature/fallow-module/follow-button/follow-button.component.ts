import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { YchAuctionApiService } from '../../../api/ych-auction-api.service';

@Component({
  selector: 'app-fallow-button',
  templateUrl: './follow-button.component.html',
  styleUrls: ['./follow-button.component.css']
})
export class FollowButtonComponent implements OnInit {

  @Input() artistName: string;
  @Input() isInFollowList: boolean;

  constructor(
    private ychAuctionApiService: YchAuctionApiService
  ) {
  }

  ngOnInit(): void {
  }

  public followAnArtist(): void {
    console.log(this.isInFollowList);
    this.ychAuctionApiService.followAnArtist(this.artistName)
      .subscribe();
  }

}
