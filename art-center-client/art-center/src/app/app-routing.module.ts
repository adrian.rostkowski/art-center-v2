import {LoginComponent} from './feature/component/home/login/login.component';
import {YchAuctionDetailsComponent} from './feature/auction-details-module/ych-auction-details/ych-auction-details.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuctionDiscussionComponent} from './feature/auction-discussion-module/auction-discussion/auction-discussion.component';
import {AuthGuard} from './shared/guard/auth.guard';
import {ConfirmAccountWithSecretKeyComponent} from './feature/create-account-module/create-account/confirm-account-with-secret-key/confirm-account-with-secret-key.component';
import {HomeComponent} from './feature/component/home/home.component';
import {LogoutComponent} from './feature/component/home/logout/logout.component';
import {AuctionListComponent} from './feature/auction-list-module/auction-list/auction-list.component';
import {UserPanelComponent} from './feature/your-panel-module/your-panel/user-panel.component';
import {ArtistVisitCardComponent} from './feature/component/artist-visit-card/artist-visit-card.component';
import {NotFoundComponent} from './feature/component/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
  { path: 'won-ychs/:cardContent', component: AuctionListComponent },
  { path: 'sold-ychs/:cardContent', component: AuctionListComponent},
  // { path: 'auction-discussion/:ychId', component: AuctionDiscussionComponent, canActivate: [AuthGuard] },
  { path: 'artist-visit-card/:artistName', component: ArtistVisitCardComponent },
  { path: 'confirm-account/:userId', component: ConfirmAccountWithSecretKeyComponent },
  {
    path: 'create-account',
    loadChildren: () => import('./feature/create-account-module/create-account-module.module').then(m => m.CreateAccountModuleModule)
  },
  {
    path: 'create-ych/:auctionKind',
    loadChildren: () => import('./feature/create-auction-module/create-auction.module').then(m => m.CreateAuctionModule)
  },
  {
    path: 'user-panel',
    loadChildren: () => import('./feature/your-panel-module/your-panel.module').then(m => m.YourPanelModule)
  },
  {
    path: 'search-criteria',
    loadChildren: () => import('./feature/search-criteria-module/search-criteria.module').then(m => m.SearchCriteriaModule)
  },
  {
    path: 'auction-list',
    loadChildren: () => import('./feature/auction-list-module/auction-list.module').then(m => m.AuctionListModule)
  },
  {
    path: 'ych-details/:ychId',
    loadChildren: () => import('./feature/auction-details-module/auction-details.module').then(m => m.AuctionDetailsModule)
  },
  {
    path: 'auction-discussion/:ychId',
    loadChildren: () => import('./feature/auction-discussion-module/auction-discussion.module').then(m => m.AuctionDiscussionModule)
  },
  { path: 'fallow', loadChildren: () => import('./feature/fallow-module/follow.module').then(m => m.FollowModule) },
  // this element must be always last one
  { path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
