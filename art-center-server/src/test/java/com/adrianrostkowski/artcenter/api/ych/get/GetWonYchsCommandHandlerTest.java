package com.adrianrostkowski.artcenter.api.ych.get;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.get.won.GetWonYchsCommand;
import com.adrianrostkowski.artcenter.application.auction.get.won.GetWonYchsCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.UserTestHelper.makeUser;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetWonYchsCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private GetWonYchsCommandHandler handler;

    private User user;
    private User userToBid;
    private AuctionImage image;
    private BigDecimal maxBidValue;

    private final int countOfWonAuctions = 10;

    @BeforeEach
    public void setUp() {
        user = users.save(makeUser("Adrian", "123"));
        userToBid = users.save(makeUser("Bartek", "123"));

        image = images.save(AuctionTestHelper.makeYchImage());
        maxBidValue = new BigDecimal(50);

        TestSecurityHelper.makeUserContext(userToBid);

        for (int i = 0; i < countOfWonAuctions; i++) {
            var ych = ychs.save(AuctionTestHelper.makeYchAuctionWithMaxBid(user, image, maxBidValue));

            var maxBid = bids.save(Bid.builder()
                    .auction(ych)
                    .owner(userToBid)
                    .value(ych.getAutoBuyBidValue())
                    .build());

            ych.autoBuy(userToBid, maxBid);
            ychs.save(ych);
        }
    }

    @AfterEach
    public void tearDown() {
        bids.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldTakeAllWonYchs() {
        //given
        TestSecurityHelper.makeUserContext(userToBid);
        var cmd = GetWonYchsCommand.builder()
                .pageNumber(0)
                .pageSize(10)
                .build();
        //when
        var wonAuctions = handler.handle(cmd);
        //then
        assertThat(wonAuctions.getCards())
                .hasSize(countOfWonAuctions);
    }
}
