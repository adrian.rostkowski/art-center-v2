package com.adrianrostkowski.artcenter.api.user;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.user.update.password.ChangeUserPasswordCommand;
import com.adrianrostkowski.artcenter.application.user.update.password.ChangeUserPasswordCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ChangeUserPasswordCommandHandlerTest extends BaseTestSetup {

    @Autowired
    ChangeUserPasswordCommandHandler handler;

    private User user;

    @BeforeEach
    public void setUp() {
        var userToSave = User.builder()
                .login("Adrian")
                .password(BCrypt.hashpw("abra kadabra", BCrypt.gensalt()))
                .build();
        user = users.save(userToSave);
    }

    @AfterEach
    public void tearDown() {
        users.deleteAll();
    }

    @Test
    void shouldChangeOldPassword() {
        //given
        final var newPassword = "xyzxyz";
        TestSecurityHelper.makeUserContext(user);
        var cmd = ChangeUserPasswordCommand.builder()
                .currentPassword("abra kadabra")
                .newPassword(newPassword)
                .repeatNewPassword(newPassword)
                .build();
        //when
        handler.handle(cmd);
        //then
        var changedUser = users.findByLogin(UserContextHelper.getCurrentUser().getLogin());
        assertThat(BCrypt.checkpw(newPassword, changedUser.getPassword())).isTrue();
    }
}
