package com.adrianrostkowski.artcenter.api.comment;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.comment.add.AddCommentCommand;
import com.adrianrostkowski.artcenter.application.comment.add.AddCommentCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.comment.CommentType;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest

public class AddCommentCommandHandlerTest extends BaseTestSetup {
    @Autowired
    private AddCommentCommandHandler handler;

    private User user;
    private Auction ych;
    private AuctionImage image;

    @BeforeEach
    public void setUp() {
        user = users.save(User.builder().build());
        image = images.save(AuctionTestHelper.makeYchImage());
        ych = ychs.save(AuctionTestHelper.makeYchAuction(user, image));
    }

    @AfterEach
    public void tearDown() {
        comments.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    public void shouldAddNewCommentToYchAuction() {
        // given
        TestSecurityHelper.makeUserContext(user);
        var cmd = AddCommentCommand.builder()
                .commentType(CommentType.BIDDING)
                .entityId(ych.getId())
                .textValue("Hello Comment!")
                .build();
        // when
        handler.handle(cmd);
        // then
        assertThat(ych.getComments().size()).isEqualTo(1);
        assertThat(ych.getComments().get(0).getText()).isEqualTo(cmd.getTextValue());
        assertThat(ych.getComments().get(0).getType()).isEqualTo(cmd.getCommentType());
    }
}
