package com.adrianrostkowski.artcenter.api;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionCommand;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.UserTestHelper.makeUser;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AddBidToAuctionCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private AddBidToAuctionCommandHandler handler;

    private User user;
    private User userToBid;
    private Auction ychAuction;
    private AuctionImage image;

    private BigDecimal bidValue;

    @BeforeEach
    public void setUp() {
        user = users.save(makeUser("Bartek", "123"));

        userToBid = users.save(makeUser("Adrian", "123"));

        bidValue = new BigDecimal(100);

        image = images.save(AuctionTestHelper.makeYchImage());

        ychAuction = ychs.save(AuctionTestHelper.makeYchAuction(user, image));
    }

    @AfterEach
    public void tearDown() {
        bids.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldAddNewBid() {
        //given
        TestSecurityHelper.makeUserContext(userToBid);
        var cmd = AddBidToAuctionCommand.builder()
                .auctionId(ychAuction.getId())
                .bidValue(bidValue)
                .build();
        //when
        handler.handle(cmd);
        //then
        assertThat(ychAuction.getBids().size()).isEqualTo(1);
        assertThat(ychAuction.getBids().get(0).getValue()).isEqualTo(bidValue);
        assertThat(ychAuction.getBids().get(0).getOwner().getId()).isEqualTo(userToBid.getId());
    }
}
