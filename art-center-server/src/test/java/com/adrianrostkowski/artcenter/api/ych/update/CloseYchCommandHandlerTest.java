package com.adrianrostkowski.artcenter.api.ych.update;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.update.CloseYchCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CloseYchCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private CloseYchCommandHandler handler;

    private User user, user2;
    private Auction ychAuction;
    private AuctionImage image;
    private BigDecimal bidValue;
    private BigDecimal maxBidValue;

    @BeforeEach
    public void setUp() {
        var userToSave = User.builder().build();
        var userToSave2 = User.builder()
                .login("user2")
                .build();
        user = users.save(userToSave);
        user2 = users.save(userToSave2);
        bidValue = new BigDecimal(100);
        maxBidValue = new BigDecimal(110);

        image = images.save(AuctionTestHelper.makeYchImage());

        ychAuction = ychs.save(AuctionTestHelper.makeYchAuctionWithMaxBid(user, image, maxBidValue));
    }

    @AfterEach
    public void tearDown() {
        bids.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldCloseYchAuction() {
        //given
        TestSecurityHelper.makeUserContext(user);
        //when
        handler.handle(ychAuction.getId());
        //then
        assertThat(ychAuction.getAuctionStep()).isEqualTo(AuctionStep.CLOSED);
    }
}
