package com.adrianrostkowski.artcenter.api;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.get.byid.GetAuctionByIdQueryHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetAuctionByIdQueryHandlerTest extends BaseTestSetup {
    @Autowired
    private GetAuctionByIdQueryHandler handler;

    private Auction auction;
    private User user;

    @BeforeEach
    public void setUp() {
        var user = User.builder().build();
        this.user = users.save(user);

        AuctionImage image = images.save(AuctionTestHelper.makeYchImage());
        auction = ychs.save(AuctionTestHelper.makeYchAuction(user, image));
    }

    @AfterEach
    public void tearDown() {
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldReturnAuctionById() {
        //given
        TestSecurityHelper.makeUserContext(user);
        var auctionId = auction.getId();
        //when
        var response = handler.handle(auctionId);
        //then
        assertThat(response.getYchAuctionId()).isEqualTo(auctionId);
        assertThat(response.getImageId()).isEqualTo(auction.getImage().getId());
    }
}
