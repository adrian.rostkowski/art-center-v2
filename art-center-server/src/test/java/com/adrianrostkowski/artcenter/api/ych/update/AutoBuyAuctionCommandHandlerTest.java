package com.adrianrostkowski.artcenter.api.ych.update;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.update.AutoBuyAuctionCommand;
import com.adrianrostkowski.artcenter.application.auction.update.AutoBuyAuctionCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.UserTestHelper.makeUser;
import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_BE_ON_BIDDING_STEP;
import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_CONTAIN_MAX_BID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class AutoBuyAuctionCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private AutoBuyAuctionCommandHandler handler;

    private User user;
    private User userToBid;
    private Auction ychAuction;
    private Auction ychAuctionWithoutAutoBuy;
    private Auction ychAuctionOnClosedStep;
    private AuctionImage image;
    private BigDecimal bidValue;
    private BigDecimal maxBidValue;

    @BeforeEach
    public void setUp() {
        user = users.save(makeUser("Adrian", "123"));

        userToBid = users.save(makeUser("Bartek", "123"));

        bidValue = new BigDecimal(100);
        maxBidValue = new BigDecimal(110);

        image = images.save(AuctionTestHelper.makeYchImage());

        ychAuction = ychs.save(AuctionTestHelper.makeYchAuctionWithMaxBid(user, image, maxBidValue));

        var ychAuctionOnClosedStepToSave = ychs.save(AuctionTestHelper.makeYchAuctionWithMaxBid(user, image, maxBidValue));
        TestSecurityHelper.makeUserContext(user);
        ychAuctionOnClosedStepToSave.closeAuction();
        ychAuctionOnClosedStep = ychs.save(ychAuctionOnClosedStepToSave);

        ychAuctionWithoutAutoBuy = ychs.save(AuctionTestHelper.makeYchAuction(user, image));
    }

    @AfterEach
    public void tearDown() {
        bids.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
     void shouldEndAuctionByAutoBuy() {
        //given
        TestSecurityHelper.makeUserContext(userToBid);
        var cmd = AutoBuyAuctionCommand.builder()
                .ychId(ychAuction.getId())
                .build();
        //when
        handler.handle(cmd);
        //then
        assertThat(ychAuction.getEndedByAutoBuy())
                .isTrue();
        assertThat(ychAuction.getAuctionStep())
                .isEqualTo(AuctionStep.CONSULTATIONS);
        assertThat(ychAuction.getWinner())
                .isEqualTo(userToBid);
    }

    @Test
    @Transactional
    void shouldThrowAnErrorWhenAuctionDoesNotIncludeAutoBuy() {
        //given
        TestSecurityHelper.makeUserContext(user);
        var cmd = AutoBuyAuctionCommand.builder()
                .ychId(ychAuctionWithoutAutoBuy.getId())
                .build();
        //when
        //then
        assertThatThrownBy(() -> handler.handle(cmd))
                .isInstanceOf(BusinessException.class)
                .hasMessage(APPLICATION_MUST_CONTAIN_MAX_BID.name());
    }

    @Test
    @Transactional
    void shouldThrowAnErrorWhenAuctionIsNotOnBiddingStep() {
        //given
        TestSecurityHelper.makeUserContext(user);
        var cmd = AutoBuyAuctionCommand.builder()
                .ychId(ychAuctionOnClosedStep.getId())
                .build();
        //when
        //then
        assertThatThrownBy(() -> handler.handle(cmd))
                .isInstanceOf(BusinessException.class)
                .hasMessage(APPLICATION_MUST_BE_ON_BIDDING_STEP.name());
    }
}
