package com.adrianrostkowski.artcenter.api;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.create.CreateAuctionCommand;
import com.adrianrostkowski.artcenter.application.auction.create.CreateYchAuctionCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@SpringBootTest
class CreateAuctionCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private CreateYchAuctionCommandHandler handler;
    @MockBean
    S3ImagesService s3ImagesService;

    private User user;

    @BeforeEach
    public void setUp() {
        var userToSave = User.builder().build();
        user = users.save(userToSave);
    }

    @AfterEach
    public void tearDown() {
        images.deleteAll();
        ychs.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void dummyTest() {
        //given
        TestSecurityHelper.makeUserContext(user);

        byte[] byteArray = new byte[5];
        MultipartFile file = new MockMultipartFile("testFile.jpg", byteArray);

        var cmd = CreateAuctionCommand.builder()
                .description("dummy description")
                .minBid(new BigDecimal(10))
                .maxBid(new BigDecimal(50))
                .isMaxBid(false)
                .countOfDays(5L)
                .auctionKind(AuctionKind.YCH.name())
                .currency(BidCurrency.USD.name())
                .ychAuctionType(YchAuctionType.SAFE.name())
                .ychAuctionCharacterType(YchAuctionCharacterType.PONY.name())
                .mainArt(file)
                .build();
        doNothing().when(s3ImagesService).uploadFileToBucket(any(), (byte[]) any());
        //when
        var result = handler.handle(cmd);
        //then
        assertThat(result.getDescription()).isEqualTo(
                        ychs.findById(result.getYchAuctionId())
                                .orElseThrow()
                                .getDescription());

        assertThat(ychs.findAll().size()).isEqualTo(1);

        assertThat(images.findAll().size()).isEqualTo(1);
    }
}
