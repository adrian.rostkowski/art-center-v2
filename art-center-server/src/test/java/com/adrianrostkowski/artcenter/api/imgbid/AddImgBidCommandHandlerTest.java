package com.adrianrostkowski.artcenter.api.imgbid;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.imgbid.add.AddImgBidCommand;
import com.adrianrostkowski.artcenter.application.imgbid.add.AddImgBidCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.UserTestHelper.makeUser;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class AddImgBidCommandHandlerTest extends BaseTestSetup {
    @Autowired
    AddImgBidCommandHandler addImgBidCommandHandler;

    @MockBean
    S3ImagesService s3ImagesService;

    private User user;
    private Auction ych;
    private AuctionImage image;

    private User userToBid;
    private Auction ychAuction;

    private BigDecimal bidValue;

    @BeforeEach
    public void setUp() {
        user = users.save(makeUser("Bartek", "123"));
        userToBid = users.save(makeUser("Adrian", "123"));
        bidValue = new BigDecimal(100);
        image = images.save(AuctionTestHelper.makeYchImage());
        ychAuction = ychs.save(AuctionTestHelper.makeImgAuction(user, image));
    }

    @AfterEach
    public void tearDown() {
        imgBidRepository.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldAddImgBidToArtTradeAuction() {
        //given
        TestSecurityHelper.makeUserContext(userToBid);
        byte[] byteArray = new byte[5];
        MultipartFile file = new MockMultipartFile("testFile.jpg", byteArray);
        var cmd = AddImgBidCommand.builder()
                .auctionId(ychAuction.getId())
                .description("HELLO ART_TRADE!")
                .build();
        //when
        doNothing().when(s3ImagesService).uploadFileToBucket(any(), (byte[]) any());
        addImgBidCommandHandler.handle(cmd, file);
        //then
        assertThat(imgBidRepository.findAll()).hasSize(1);
        assertThat(imgBidRepository.findAll().get(0).getAuction().getId()).isEqualTo(ychAuction.getId());
    }
}
