package com.adrianrostkowski.artcenter.api.follow;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.follow.FollowCommand;
import com.adrianrostkowski.artcenter.application.follow.FollowCommandHandler;
import com.adrianrostkowski.artcenter.application.follow.GetFollowListQueryHandler;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest

public class GetFollowListQueryHandlerTest extends BaseTestSetup {

    @Autowired
    private GetFollowListQueryHandler handler;
    @Autowired
    private FollowCommandHandler followCommandHandler;

    private User user, artist1, artist2, artist3, artist4;

    @BeforeEach
    public void setUp() {
        user = users.save(User.builder()
                .userName("user")
                .email("user@gmail.com")
                .build());
        artist1 = users.save(User.builder()
                .userName("artist1")
                .email("artist1@gmail.com")
                .build());
        artist2 = users.save(User.builder()
                .userName("artist2")
                .email("artist@gmail.com")
                .build());
        artist3 = users.save(User.builder()
                .userName("artist3")
                .email("artist3@gmail.com")
                .build());
        artist4 = users.save(User.builder()
                .userName("artist4")
                .email("artist4@gmail.com")
                .build());
    }

    @AfterEach
    public void tearDown() {
        followGroups.deleteAll();
        users.deleteAll();
    }

    @Test
    public void name() {
        // given
        TestSecurityHelper.makeUserContext(user);
        var cmd =  new FollowCommand(artist1.getUsername());
        followCommandHandler.handle(new FollowCommand(artist1.getUsername()));
        TestSecurityHelper.makeUserContext(user);
        followCommandHandler.handle(new FollowCommand(artist2.getUsername()));
        TestSecurityHelper.makeUserContext(user);
        followCommandHandler.handle(new FollowCommand(artist3.getUsername()));
        TestSecurityHelper.makeUserContext(user);
        followCommandHandler.handle(new FollowCommand(artist4.getUsername()));
        // when
        var result = handler.handle();
        // then
        assertThat(result).hasSize(4);
    }
}
