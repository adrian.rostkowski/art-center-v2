package com.adrianrostkowski.artcenter.api.user;

import com.adrianrostkowski.artcenter.application.user.create.CreateNewUserCommand;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class UserControllerTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    ObjectMapper mapper;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext).build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void newPasswordCanNotBeEqualsToCurrentPassword() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        final var createNewUserCommand = new CreateNewUserCommand("login1234", "username", "password1", "password2", "fake@gmail.com");
        final var content = objectMapper.writeValueAsString(createNewUserCommand);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/user/create")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isIAmATeapot())
                .andExpect(jsonPath("$[0]", Matchers.is(PASSWORDS_ARE_NOT_EQUALS.getMessage())));
    }

    @Test
    void emailMustBeValid() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        final var createNewUserCommand = new CreateNewUserCommand("login1234", "username", "password1", "password1", "fakeXXXgmail.com");
        final var content = objectMapper.writeValueAsString(createNewUserCommand);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/user/create")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(content))
                .andExpect(status().isIAmATeapot())
                .andExpect(jsonPath("$[0]", Matchers.is(NEW_PASSWORD_CAN_NOT_BE_EQUALS_TO_CURRENT_PASSWORD.getMessage())));
    }
}
