package com.adrianrostkowski.artcenter.api.ych.get;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.UserTestHelper;
import com.adrianrostkowski.artcenter.application.auction.get.discussion.GetAuctionDiscussionCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetAuctionDiscussionCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private GetAuctionDiscussionCommandHandler handler;
    @MockBean
    EmailService emailService;

    private User user;
    private User userToBid;
    private AuctionImage image;
    private BigDecimal maxBidValue;
    private Auction ychAuction;
    private Bid bid;

    @BeforeEach
    public void setUp() {
        user = users.save(UserTestHelper.makeUser("Adrian", "123"));
        userToBid = users.save(UserTestHelper.makeUser("Bartek", "123"));

        TestSecurityHelper.makeUserContext(userToBid);

        image = images.save(AuctionTestHelper.makeYchImage());
        maxBidValue = new BigDecimal(50);

        ychAuction = ychs.save(AuctionTestHelper.makeYchAuctionWithMaxBid(user, image, maxBidValue));
        bid = bids.save(AuctionTestHelper.makeYchBid(BigDecimal.valueOf(100), user, ychAuction));
        ychAuction.addCurrencyBid(bid);
        ychs.save(ychAuction);
    }

    @AfterEach
    public void tearDown() {
        bids.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldTakeAllWonYchs() {
        //given
        TestSecurityHelper.makeUserContext(user);
        ychAuction.endAuctionWithAWinner(emailService, "");
        //when
        var result = handler.handle(ychAuction.getId());
        //then
        assertThat(result.getYchId()).isEqualTo(ychAuction.getId());
    }
}
