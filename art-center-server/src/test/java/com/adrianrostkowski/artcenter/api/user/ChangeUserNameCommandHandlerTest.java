package com.adrianrostkowski.artcenter.api.user;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.user.update.username.ChangeUserNameCommand;
import com.adrianrostkowski.artcenter.application.user.update.username.ChangeUserNameCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ChangeUserNameCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private ChangeUserNameCommandHandler handler;

    private User user;
    private final String currentPassword = "abra kadabra";
    private final String hashedCurrentPassword = BCrypt.hashpw("abra kadabra", BCrypt.gensalt());

    @BeforeEach
    public void setUp() {
        var userToSave = User.builder()
                .login("Adrian")
                .password(hashedCurrentPassword)
                .build();
        user = users.save(userToSave);
    }

    @AfterEach
    public void tearDown() {
        users.deleteAll();
    }

    @Test
    void shouldAddNewBid() {
        //given
        TestSecurityHelper.makeUserContext(user);
        final var newUsername = "xX*Dark_Knight*Xx";
        var cmd = ChangeUserNameCommand.builder()
                .newUsername(newUsername)
                .currentPassword(currentPassword)
                .build();
        //when
        handler.handle(cmd);
        //then
        var changedUser = users.findByLogin(user.getLogin());
        assertThat(newUsername).isEqualTo(changedUser.getUsername());
    }
}
