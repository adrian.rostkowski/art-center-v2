package com.adrianrostkowski.artcenter.api.ych.update.arttrade;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.update.arttrade.ChooseWinnerCommand;
import com.adrianrostkowski.artcenter.application.auction.update.arttrade.ChooseWinnerCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static com.adrianrostkowski.artcenter.UserTestHelper.makeUserWithUserName;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ChooseWinnerCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private ChooseWinnerCommandHandler handler;

    private User user;
    private User userToBid;
    private Auction ychAuction;
    private AuctionImage image;
    private ImgBid winningBid;

    @BeforeEach
    void setUp() {
        user = users.save(makeUserWithUserName("Adrian", "123", "xX_Adix_Xx"));

        userToBid = users.save(makeUserWithUserName("Bartek", "123", "**_Bartek_**"));

        image = images.save(AuctionTestHelper.makeYchImage());

        ychAuction = ychs.save(AuctionTestHelper.makeArtTradeAuctionWithMaxBid(user, image));

        TestSecurityHelper.makeUserContext(userToBid);
        winningBid = imgBidRepository.save(ImgBid.create(ychAuction, userToBid, image, "description art trade"));
        ychAuction.addImgBid(winningBid);
    }

    @AfterEach
    void tearDown() {
        bids.deleteAll();
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    void shouldEndAuctionByAutoBuy() {
        //given
        TestSecurityHelper.makeUserContext(user);
        var cmd = ChooseWinnerCommand.builder()
                .auctionId(ychAuction.getId())
                .winningBidId(winningBid.getId())
                .build();
        //when
        handler.handle(cmd);
        //then
        assertThat(ychAuction.getKind()).isEqualTo(AuctionKind.ART_TRADE);
        assertThat(ychAuction.getAuctionStep()).isEqualTo(AuctionStep.CONSULTATIONS);
        assertThat(ychAuction.getWinner().getUsername()).isEqualTo(userToBid.getUsername());
    }
}
