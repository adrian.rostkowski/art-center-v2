package com.adrianrostkowski.artcenter.api.follow;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.application.follow.FollowCommand;
import com.adrianrostkowski.artcenter.application.follow.FollowCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.YOU_CAN_NOT_FOLLOW_YOURSELF;
import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.YOU_HAVE_ALREADY_THIS_ARTIST_IN_YOUR_FOLLOW_LIST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest

public class FollowCommandHandlerTest extends BaseTestSetup {

    @Autowired
    private FollowCommandHandler handler;

    private User user, artist;

    @BeforeEach
    public void setUp() {
        user = users.save(User.builder()
                .userName("user")
                .email("user@gmail.com")
                .build());
        artist = users.save(User.builder()
                .userName("artist")
                .email("artist@gmail.com")
                .build());
    }

    @AfterEach
    public void tearDown() {
        followGroups.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    public void shouldAddUserToFollowGroup() {
        // given
        TestSecurityHelper.makeUserContext(user);
        var cmd =  new FollowCommand(artist.getUsername());
        // when
        handler.handle(cmd);
        // then
        var foundGroup = followGroups.getFollowGroupByArtistId(artist.getId()).get();
        assertThat(foundGroup.getUsers())
                .hasSize(1);

        assertThat(foundGroup.getUsers().get(0))
                .extracting("id")
                .isEqualTo(user.getId());
    }

    @Test
    public void shouldThrowAnErrorWhenUserIsAlreadyInArtistsGroup() {
        // given
        TestSecurityHelper.makeUserContext(user);
        var cmd =  new FollowCommand(artist.getUsername());
        // when
        handler.handle(cmd);
        // then
        assertThatThrownBy(() -> handler.handle(cmd))
                .isInstanceOf(BusinessException.class)
                .hasMessage(YOU_HAVE_ALREADY_THIS_ARTIST_IN_YOUR_FOLLOW_LIST.name());
    }

    @Test
    public void shouldThrowAnErrorWhenUserIsArtistOfGroup() {
        // given
        TestSecurityHelper.makeUserContext(artist);
        var cmd =  new FollowCommand(artist.getUsername());
        // when
        // then
        assertThatThrownBy(() -> handler.handle(cmd))
                .isInstanceOf(BusinessException.class)
                .hasMessage(YOU_CAN_NOT_FOLLOW_YOURSELF.name());
    }
}
