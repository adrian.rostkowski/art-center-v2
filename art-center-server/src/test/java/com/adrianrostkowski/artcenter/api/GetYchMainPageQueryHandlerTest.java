package com.adrianrostkowski.artcenter.api;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.get.mainpage.GetAuctionsListCommand;
import com.adrianrostkowski.artcenter.application.auction.get.mainpage.GetYchMainPageCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class GetYchMainPageQueryHandlerTest extends BaseTestSetup {

    @Autowired
    private GetYchMainPageCommandHandler handler;

    private List<Auction> ychAuctions = new ArrayList<>();
    private int countOfAuctions = 10;

    private User user;

    @BeforeEach
    public void setUp() {
        var userToSave = User.builder().build();
        user = users.save(userToSave);

        for (int i = 0; i < countOfAuctions; i++) {
            var image = images.save(AuctionTestHelper.makeYchImage());
            var auction = ychs.save(AuctionTestHelper.makeYchAuction(user, image));

            ychAuctions.add(auction);
        }
    }

    @AfterEach
    public void tearDown() {
        ychs.deleteAll();
        images.deleteAll();
        users.deleteAll();
    }

    @Test
    @Transactional
    public void shouldReturnYchMainPageAuctions() {
        //given
        TestSecurityHelper.makeUserContext(user);
        var cmd = GetAuctionsListCommand.builder()
                .findByCurrentUser(false)
                .pageNumber(0)
                .pageSize(10)
                .build();
        //when
        var result = handler.handle(cmd);
        //then
        assertThat(result.getCards().size()).isEqualTo(countOfAuctions);
    }
}
