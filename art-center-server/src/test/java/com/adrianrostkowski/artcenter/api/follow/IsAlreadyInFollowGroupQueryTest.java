package com.adrianrostkowski.artcenter.api.follow;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.follow.FollowCommand;
import com.adrianrostkowski.artcenter.application.follow.FollowCommandHandler;
import com.adrianrostkowski.artcenter.application.follow.IsAlreadyInFollowGroupQuery;
import com.adrianrostkowski.artcenter.application.follow.IsAlreadyInFollowGroupQueryHandler;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest("hibernate.query.interceptor.error-level=ERROR")
class IsAlreadyInFollowGroupQueryTest extends BaseTestSetup {

    @Autowired
    IsAlreadyInFollowGroupQueryHandler handler;
    @Autowired
    FollowCommandHandler followHandler;

    private User user1, user2, user3, user4, user5, artist1, artist2;

    @BeforeEach
    public void setUp() {
        createUsers();
        TestSecurityHelper.makeUserContext(user1);
        followHandler.handle(new FollowCommand(artist1.getUsername()));
        TestSecurityHelper.makeUserContext(user2);
        followHandler.handle(new FollowCommand(artist1.getUsername()));
        TestSecurityHelper.makeUserContext(user3);
        followHandler.handle(new FollowCommand(artist1.getUsername()));

        TestSecurityHelper.makeUserContext(user4);
        followHandler.handle(new FollowCommand(artist2.getUsername()));
        TestSecurityHelper.makeUserContext(user5);
        followHandler.handle(new FollowCommand(artist2.getUsername()));
    }

    @AfterEach
    public void tearDown() {
        followGroups.deleteAll();
        users.deleteAll();
    }


    @Test
    void shouldReturnTrueWhenUserIsInFollowGroup() {
        // given
        TestSecurityHelper.makeUserContext(user1);
        var cmd = new IsAlreadyInFollowGroupQuery(artist1.getUsername());
        // when
        var result = handler.handle(cmd);
        // then
        assertThat(result.getIsAlready()).isTrue();
    }

    @Test
    void shouldReturnFalseWhenUserIsNotInFollowGroup() {
        // given
        TestSecurityHelper.makeUserContext(user5);
        var cmd = new IsAlreadyInFollowGroupQuery(artist1.getUsername());
        // when
        var result = handler.handle(cmd);
        // then
        assertThat(result.getIsAlready()).isFalse();
    }

    private void createUsers() {
        user1 = users.save(User.builder()
                .userName("user1")
                .email("user1@gmail.com")
                .build());
        user2 = users.save(User.builder()
                .userName("user2")
                .email("user2@gmail.com")
                .build());
        user3 = users.save(User.builder()
                .userName("user3")
                .email("user3@gmail.com")
                .build());
        user4 = users.save(User.builder()
                .userName("user4")
                .email("user4@gmail.com")
                .build());
        user5 = users.save(User.builder()
                .userName("user5")
                .email("user5@gmail.com")
                .build());
        artist1 = users.save(User.builder()
                .userName("artist1")
                .email("artist1@gmail.com")
                .build());
        artist2 = users.save(User.builder()
                .userName("artist2")
                .email("artist2@gmail.com")
                .build());
    }
}
