package com.adrianrostkowski.artcenter.auctionfactory;

import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.application.auction.factory.CreateAuction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest

public class CreateAdoptAuctionTest {

    @BeforeEach
    public void setUp() {

    }

    @AfterEach
    public void tearDown() {

    }

    @Test
    @Transactional
    public void shouldCreateAdoptAuction() {
        // given
        // TODO ?
        CreateAuction.chooseStrategy(AuctionKind.ADOPT);
        // when
        // then
    }
}
