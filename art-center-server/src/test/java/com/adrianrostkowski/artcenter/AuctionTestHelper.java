package com.adrianrostkowski.artcenter;

import com.adrianrostkowski.artcenter.domain.model.auction.*;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.auction.image.ImageType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.user.User;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class AuctionTestHelper {

    public static Auction makeYchAuction(User user, AuctionImage image) {
        var base = Auction.createBase(
                user,
                image,
                "Description",
                new BigDecimal(10),
                null,
                false,
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)),
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)).plusDays(5),
                YchAuctionType.SAFE,
                YchAuctionCharacterType.HUMAN,
                BidCurrency.USD
        );
        base.setKind(AuctionKind.YCH);
        return base;
    }

    public static Auction makeImgAuction(User user, AuctionImage image) {
        var base = Auction.createBase(
                user,
                image,
                "Description",
                new BigDecimal(10),
                null,
                false,
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)),
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)).plusDays(5),
                YchAuctionType.SAFE,
                YchAuctionCharacterType.HUMAN,
                BidCurrency.USD
        );
        base.setKind(AuctionKind.ART_TRADE);
        return base;
    }

    public static Auction makeYchAuctionWithMaxBid(User user, AuctionImage image, BigDecimal maxBid) {
        var base = Auction.createBase(
                user,
                image,
                "Description",
                new BigDecimal(10),
                maxBid,
                true,
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)),
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)).plusDays(5),
                YchAuctionType.SAFE,
                YchAuctionCharacterType.HUMAN,
                BidCurrency.USD
        );
        base.setKind(AuctionKind.YCH);
        return base;
    }

    public static Auction makeArtTradeAuctionWithMaxBid(User user, AuctionImage image) {
        var base = Auction.createBase(
                user,
                image,
                "Description",
                new BigDecimal(10),
                null,
                true,
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)),
                LocalDateTime.of(LocalDate.of(2020, 10, 10), LocalTime.of(10,10)).plusDays(5),
                YchAuctionType.SAFE,
                YchAuctionCharacterType.HUMAN,
                BidCurrency.USD
        );
        base.setKind(AuctionKind.ART_TRADE);
        return base;
    }

    public static AuctionImage makeYchImage() {
        return AuctionImage.builder()
                .data(new byte[5])
                .fileName("fileName")
                .fileType("jpg")
                .imageType(ImageType.YCH_AUCTION)
                .build();
    }

    public static Bid makeYchBid(BigDecimal bidValue, User owner, Auction ychAuction) {
        return Bid.builder()
                .value(bidValue)
                .owner(owner)
                .auction(ychAuction)
                .build();
    }
}
