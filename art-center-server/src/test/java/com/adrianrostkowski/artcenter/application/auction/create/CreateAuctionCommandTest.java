package com.adrianrostkowski.artcenter.application.auction.create;

import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.*;
import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class CreateAuctionCommandTest extends BaseTestSetup {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
        users.deleteAll();
    }

    @Test
    void validateInput() {
        var cmd = CreateAuctionCommand.builder()
                .description("description")
                .minBid(new BigDecimal("10.00"))
                .maxBid(new BigDecimal("5.00"))
                .isMaxBid(false)
                .countOfDays(3L)
                .ychAuctionType(YchAuctionType.SAFE.name())
                .ychAuctionCharacterType(YchAuctionCharacterType.HUMAN.name())
                .auctionKind(AuctionKind.ART_TRADE.name())
                .currency(BidCurrency.USD.name())
                .build();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        assertThatThrownBy(() -> {
            test(cmd, validator);
        })
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessage(": " + BusinessExceptionCodes.MAX_BID_CAN_NOT_BE_LOWER_THEN_MIN_BID.name());
    }

    private void test(CreateAuctionCommand cmd, Validator validator) {
        Set<ConstraintViolation<CreateAuctionCommand>> violations = validator.validate(cmd);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
