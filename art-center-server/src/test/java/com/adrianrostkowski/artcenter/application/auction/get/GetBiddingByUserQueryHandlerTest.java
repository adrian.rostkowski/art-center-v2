package com.adrianrostkowski.artcenter.application.auction.get;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.BaseTestSetup;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.application.auction.get.bidding.GetBiddingByUserQueryHandler;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.UserTestHelper.makeUser;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetBiddingByUserQueryHandlerTest extends BaseTestSetup {

    @Autowired
    private GetBiddingByUserQueryHandler handler;

    private User auctionOwner;
    private User userToBid, otherUserToBid;

    private Auction ychAuction1, ychAuction2, ychAuction3, ychAuction4, ychAuction5;
    private AuctionImage image;
    private BigDecimal bidValue1, bidValue2, bidValue3, bidValue4, bidValue5;

    @BeforeEach
    public void setUp() {
        auctionOwner = users.save(makeUser("Bartek", "123"));
        userToBid = users.save(makeUser("Adrian", "123"));
        otherUserToBid = users.save(makeUser("Karol", "123"));

        image = images.save(AuctionTestHelper.makeYchImage());

        bidValue1 = BigDecimal.valueOf(100L);
        bidValue2 = BigDecimal.valueOf(200L);
        bidValue3 = BigDecimal.valueOf(300L);
        bidValue4 = BigDecimal.valueOf(400L);
        bidValue5 = BigDecimal.valueOf(500L);

        ychAuction1 = makeAuctionAndAddBid(bidValue1, userToBid, auctionOwner, image);
        ychAuction2 = makeAuctionAndAddBid(bidValue2, userToBid, auctionOwner, image);
        ychAuction3 = makeAuctionAndAddBid(bidValue3, userToBid, auctionOwner, image);
        ychAuction4 = makeAuctionAndAddBid(bidValue4, userToBid, auctionOwner, image);
        ychAuction5 = makeAuctionAndAddBid(bidValue5, otherUserToBid, auctionOwner, image);
    }

    @Test
    @Transactional
    void shouldGetOnlyAuctionsCurrentlyBiddingByUser() {
        // given
        TestSecurityHelper.makeUserContext(userToBid);
        // when
        var result = handler.handle(5,0);
        // then
        assertThat(result.getCards())
                .hasSize(4)
                .extracting("ychId")
                .contains(ychAuction1.getId(), ychAuction2.getId(), ychAuction3.getId(), ychAuction4.getId());
    }

    private Auction makeAuctionAndAddBid(BigDecimal bidValue, User userToBid, User auctionOwner, AuctionImage image) {
        TestSecurityHelper.makeUserContext(userToBid);
        var foundImg = images.findById(image.getId()).orElseThrow();
        var ychAuction = ychs.save(AuctionTestHelper.makeYchAuction(auctionOwner, foundImg));
        ychAuction.addCurrencyBid(new Bid(bidValue, userToBid, ychAuction));
        ychs.save(ychAuction);
        return ychAuction;
    }
}
