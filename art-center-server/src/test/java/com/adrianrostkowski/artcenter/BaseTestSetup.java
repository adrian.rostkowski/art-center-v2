package com.adrianrostkowski.artcenter;

import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroupRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import com.adrianrostkowski.artcenter.domain.model.bid.BidRepository;
import com.adrianrostkowski.artcenter.domain.model.comment.CommentRepository;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBidRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseTestSetup {
    @Autowired
    protected AuctionRepository ychs;

    @Autowired
    protected UserRepository users;

    @Autowired
    protected AuctionImageRepository images;

    @Autowired
    protected BidRepository bids;

    @Autowired
    protected CommentRepository comments;

    @Autowired
    protected ImgBidRepository imgBidRepository;

    @Autowired
    protected FollowGroupRepository followGroups;
}
