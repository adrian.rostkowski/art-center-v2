package com.adrianrostkowski.artcenter.domain.model.user;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.user.role.Role;
import org.junit.jupiter.api.*;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.*;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DisplayName("User")
class UserTest {

    private Role role;

    @BeforeEach
    void setUp() {
        role = new Role("ROLE");
    }

    @AfterEach
    void tearDown() {
    }

    @Nested
    @DisplayName("User create tests")
    class UserCreate {
        @Nested
        @DisplayName("Login tests")
        class UserLogin {
            @Test
            @DisplayName("Test for max length")
            void maxLengthOfLoginMustBeValid() {
                // given
                final var invalidLogin = "123456789012345678901";
                // when
                // then
                assertThatThrownBy(() -> User.create(
                        invalidLogin,
                        "1234",
                        "userName",
                        "fake@gmail.com",
                        "secretKey",
                        null
                ))
                        .isInstanceOf(BusinessException.class)
                        .hasMessage(MAX_LOGIN_LENGTH_IS_20.name());
            }

            @Test
            @DisplayName("Test for min length")
            void minLengthOfLoginMustBeValid() {
                // given
                final var invalidLogin = "12345";
                // when
                // then
                assertThatThrownBy(() -> User.create(
                        invalidLogin,
                        "1234",
                        "userName",
                        "fake@gmail.com",
                        "secretKey",
                        null
                ))
                        .isInstanceOf(BusinessException.class)
                        .hasMessage(MIN_LOGIN_LENGTH_IS_6.name());
            }
        }

        @Nested
        @DisplayName("UserName tests")
        class UserUserName {
            @Test
            @DisplayName("Test for max length")
            void maxLengthOfUserNameMustBeValid() {
                // given
                final var invalidUserName = "123456789012345678901";
                // when
                // then
                assertThatThrownBy(() -> User.create(
                        "login1234",
                        "1234",
                        invalidUserName,
                        "fake@gmail.com",
                        "secretKey",
                        null
                ))
                        .isInstanceOf(BusinessException.class)
                        .hasMessage(MAX_USER_NAME_LENGTH_IS_20.name());
            }

            @Test
            @DisplayName("Test for min length")
            void minLengthOfUserNameMustBeValid() {
                // given
                final var invalidUserName = "12345";
                // when
                // then
                assertThatThrownBy(() -> User.create(
                        "login1234",
                        "1234",
                        invalidUserName,
                        "fake@gmail.com",
                        "secretKey",
                        null
                ))
                        .isInstanceOf(BusinessException.class)
                        .hasMessage(MIN_USER_NAME_LENGTH_IS_6.name());
            }
        }
    }
}
