package com.adrianrostkowski.artcenter.domain.model.auction;

import com.adrianrostkowski.artcenter.AuctionTestHelper;
import com.adrianrostkowski.artcenter.TestSecurityHelper;
import com.adrianrostkowski.artcenter.UserTestHelper;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_CONTAIN_MAX_BID;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DisplayName("Auction")
class AuctionTest {

    private Auction auction, auctionWithMaxBid;
    private User owner, winner;
    private AuctionImage image;

    private BigDecimal maxBidValue;

    @BeforeEach
    void setUp() {
        maxBidValue = new BigDecimal("100.00");
        owner = UserTestHelper.makeUserWithUserName("loginOwner", "password", "userNameOwner");
        winner = UserTestHelper.makeUserWithUserName("loginWinner", "password", "userNameWinner");
        image = new AuctionImage();

        auctionWithMaxBid = AuctionTestHelper.makeYchAuctionWithMaxBid(owner, image, maxBidValue);
        auction = AuctionTestHelper.makeYchAuction(owner, image);
    }

    @AfterEach
    void tearDown() {
    }

    @Nested
    @DisplayName("Bid tests")
    class BidTest {
        @Test
        void autoBuyBid_must_be_equals_to_maxBidValue() {
            // given
            TestSecurityHelper.makeUserContext(winner);
            final var incorrectValue = new BigDecimal("500.00");
            // when
            // then
            assertThatThrownBy(
                    () -> auctionWithMaxBid.autoBuy(winner, new Bid(incorrectValue, winner, auctionWithMaxBid)))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessage("This bid is not equals to max bid of the auction, bid id - [null], auction id - [null]");
        }

        @Test
        void maxBid_must_be_not_null() {
            // given
            TestSecurityHelper.makeUserContext(winner);
            final var incorrectValue = new BigDecimal("500.00");
            // when
            // then
            assertThatThrownBy(
                    () -> auction.autoBuy(winner, new Bid(incorrectValue, winner, auctionWithMaxBid)))
                    .isInstanceOf(BusinessException.class)
                    .hasMessage(APPLICATION_MUST_CONTAIN_MAX_BID.name());
        }
    }
}
