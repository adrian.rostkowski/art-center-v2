package com.adrianrostkowski.artcenter;

import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserTestHelper {

    public static User makeUser(String login, String password) {
        final String hashedCurrentPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        return User.builder()
                .login(login)
                .password(hashedCurrentPassword)
                .build();
    }

    public static User makeUserWithUserName(String login, String password, String userName) {
        final String hashedCurrentPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        return User.builder()
                .login(login)
                .password(hashedCurrentPassword)
                .userName(userName)
                .build();
    }
}
