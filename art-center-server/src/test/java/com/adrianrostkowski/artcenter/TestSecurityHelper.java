package com.adrianrostkowski.artcenter;

import com.adrianrostkowski.artcenter.domain.model.user.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSecurityHelper {

    private static final String USER_NAME = "user01";

    public static void makeUserContext(User user) {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);

        SecurityContextHolder.setContext(securityContext);

        when(authentication.getPrincipal()).thenReturn(
                user
        );
    }
}
