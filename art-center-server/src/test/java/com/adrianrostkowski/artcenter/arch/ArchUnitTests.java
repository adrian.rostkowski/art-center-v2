package com.adrianrostkowski.artcenter.arch;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

@AnalyzeClasses(
        packages = {
                "com.adrianrostkowski.artcenter.api",
                "com.adrianrostkowski.artcenter.application",
                "com.adrianrostkowski.artcenter.domain",
                "com.adrianrostkowski.artcenter.security"
        },
        importOptions = { ImportOption.DoNotIncludeTests.class }
)
public class ArchUnitTests {
    private static final String ROOT = "com.adrianrostkowski.artcenter";

    private static final String API_PACKAGE = ROOT + ".api";
    private static final String API_LAYER = "Api";

    private static final String APPLICATION_PACKAGE = ROOT + ".application";
    private static final String APPLICATION_LAYER  = "Application";

    private static final String DOMAIN_PACKAGE = ROOT + ".domain";
    private static final String DOMAIN_LAYER  = "Domain";

    private static final String SECURITY_PACKAGE = ROOT + ".security";
    private static final String SECURITY_LAYER = "Security";

    private final JavaClasses classes = new ClassFileImporter().importPackages(ROOT + ".*");

    @ArchTest
    private final ArchRule no_jodatime = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME;

    @ArchTest
    private final ArchRule no_generic_exceptions = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

    @ArchTest
    private final ArchRule no_autowired_injections =
            noFields()
                    .should()
                    .beAnnotatedWith("org.springframework.beans.factory.annotation.Autowired")
                    .as("no classes should use filed injection");

    @ArchTest
    private final ArchRule constructors_in_abstract_classes_should_be_protected =
            constructors().that().areDeclaredInClassesThat().areProtected().should().beProtected();

    @ArchTest
    static final ArchRule controller_should_be_annotated = classes()
            .that().haveSimpleNameEndingWith("Controller")
            .and()
            .resideInAPackage(DOMAIN_PACKAGE + ".*")
            .should().beAnnotatedWith(RestController.class)
            .andShould().notBeAnnotatedWith(Controller.class)
            .because("Controllers should be annotated with @RestController, and not with @Controller");

    @ArchTest
    private final ArchRule valid_domain_layer =
            layeredArchitecture()
                    .layer(API_LAYER).definedBy(API_PACKAGE + "..")
                    .layer(APPLICATION_LAYER).definedBy(APPLICATION_PACKAGE + "..")
                    .layer(DOMAIN_LAYER).definedBy(DOMAIN_PACKAGE + "..")
                    .layer(SECURITY_LAYER).definedBy(SECURITY_PACKAGE + "..")

                    .whereLayer(API_LAYER).mayNotBeAccessedByAnyLayer()
                    .whereLayer(APPLICATION_LAYER).mayOnlyBeAccessedByLayers(API_LAYER)
                    .whereLayer(DOMAIN_LAYER).mayOnlyBeAccessedByLayers(APPLICATION_LAYER, SECURITY_LAYER);

    // it not fits well
//    @ArchTest
//    static final ArchRule methods_can_not_return_void_value =
//            methods().that().arePublic()
//                    .and().areDeclaredInClassesThat()
//                    .resideInAPackage(DOMAIN_PACKAGE + ".*")
//                    .should().notHaveRawReturnType("void")
//                    .because("Every method should return something");
}
