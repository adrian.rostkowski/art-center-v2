package com.adrianrostkowski.artcenter.setup.develop;

import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.domain.model.user.role.Privilege;
import com.adrianrostkowski.artcenter.domain.model.user.role.PrivilegeRepository;
import com.adrianrostkowski.artcenter.domain.model.user.role.Role;
import com.adrianrostkowski.artcenter.domain.model.user.role.RoleRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@Profile("dev")
@Component
@AllArgsConstructor
@Log4j2
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private static final String TEST_FIRST_USER_NAME = "test";
    private static final String TEST_FIRST_LOGIN = "test";
    private static final String TEST_FIRST_PASSWORD = "123";

    private static final String TEST_SECOND_USER_NAME = "test2";
    private static final String TEST_SECOND_LOGIN = "test2";
    private static final String TEST_SECOND_PASSWORD = "123";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PrivilegeRepository privilegeRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        log.info("Set up test users started");
        if (userRepository.findByUserName(TEST_FIRST_USER_NAME).isPresent() && userRepository.findByUserName(TEST_SECOND_USER_NAME).isPresent()) {
            return;
        }
        Privilege readPrivilege
                = createPrivilegeIfNotFound("READ_PRIVILEGE");
        Privilege writePrivilege
                = createPrivilegeIfNotFound("WRITE_PRIVILEGE");

        List<Privilege> adminPrivileges = Arrays.asList(readPrivilege, writePrivilege);
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));

        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        User firstUser = new User();
        firstUser.setUserName(TEST_FIRST_USER_NAME);
        firstUser.setLogin(TEST_FIRST_LOGIN);
        firstUser.setPassword(passwordEncoder.encode(TEST_FIRST_PASSWORD));
        firstUser.setRoles(Arrays.asList(adminRole));
        firstUser.setEnabled(true);
        userRepository.save(firstUser);

        User secondUser = new User();
        secondUser.setUserName(TEST_SECOND_USER_NAME);
        secondUser.setLogin(TEST_SECOND_LOGIN);
        secondUser.setPassword(passwordEncoder.encode(TEST_SECOND_PASSWORD));
        secondUser.setRoles(Arrays.asList(adminRole));
        secondUser.setEnabled(true);
        userRepository.save(secondUser);
        log.info("Set up test users ended");
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    Role createRoleIfNotFound(
            String name, List<Privilege> privileges) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
