package com.adrianrostkowski.artcenter.api.bid;

import com.adrianrostkowski.artcenter.api.GenericResponse;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionCommand;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionCommandHandler;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionResponse;
import com.adrianrostkowski.artcenter.api.ResponseMessage;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.adrianrostkowski.artcenter.api.GenericResponse.success;


@RestController
@Transactional
@RequestMapping("/api/bid")
@AllArgsConstructor
public class BidController {

    private final AddBidToAuctionCommandHandler addBidToAuctionCommandHandler;

    @PostMapping("/add")
    public GenericResponse<AddBidToAuctionResponse> add(@RequestBody @Valid AddBidToAuctionCommand cmd) {
        return success(
                addBidToAuctionCommandHandler.handle(cmd),
                ResponseMessage.BID_ADDED
        );
    }
}
