package com.adrianrostkowski.artcenter.api.auction;

import com.adrianrostkowski.artcenter.application.auction.create.CreateAuctionCommand;
import com.adrianrostkowski.artcenter.application.auction.create.CreateYchAuctionCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.get.bidding.GetBiddingByUserQueryHandler;
import com.adrianrostkowski.artcenter.application.auction.get.bidding.GetBiddingByUserResponse;
import com.adrianrostkowski.artcenter.application.auction.get.byid.GetAuctionByIdQueryHandler;
import com.adrianrostkowski.artcenter.application.auction.get.discussion.GetAuctionDiscussionCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.get.mainpage.GetAuctionsListResponse;
import com.adrianrostkowski.artcenter.application.auction.get.mainpage.GetAuctionsListCommand;
import com.adrianrostkowski.artcenter.application.auction.get.mainpage.GetYchMainPageCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.get.sold.GetSoldAuctionsCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.get.sold.GetSoldAuctionsResponse;
import com.adrianrostkowski.artcenter.application.auction.get.sold.GetSoldCommand;
import com.adrianrostkowski.artcenter.application.auction.get.won.GetWonYchsCommand;
import com.adrianrostkowski.artcenter.application.auction.get.won.GetWonYchsCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.get.won.GetWonYchsResponse;
import com.adrianrostkowski.artcenter.application.auction.update.AutoBuyAuctionCommand;
import com.adrianrostkowski.artcenter.application.auction.update.AutoBuyAuctionCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.update.CloseYchCommandHandler;
import com.adrianrostkowski.artcenter.application.auction.update.arttrade.ChooseWinnerCommand;
import com.adrianrostkowski.artcenter.application.auction.update.arttrade.ChooseWinnerCommandHandler;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionCommand;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionCommandHandler;
import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionResponse;
import com.adrianrostkowski.artcenter.api.ResponseMessage;
import com.adrianrostkowski.artcenter.application.dto.AuctionDiscussionDto;
import com.adrianrostkowski.artcenter.application.dto.AuctionDto;
import com.adrianrostkowski.artcenter.api.GenericResponse;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.api.ResponseMessage.*;
import static com.adrianrostkowski.artcenter.api.GenericResponse.success;

@RestController
@Transactional
@RequestMapping("/api/auction")
@AllArgsConstructor
public class AuctionController {

    private final CreateYchAuctionCommandHandler createYchAuctionCommandHandler;
    private final GetAuctionByIdQueryHandler getAuctionByIdQueryHandler;
    private final GetYchMainPageCommandHandler getAuctionsListQueryHandler;
    private final AddBidToAuctionCommandHandler addBidToAuctionCommandHandler;
    private final AutoBuyAuctionCommandHandler autoBuyAuctionCommandHandler;
    private final GetWonYchsCommandHandler getWonYchsCommandHandler;
    private final GetSoldAuctionsCommandHandler getSoldYchsCommandHandler;
    private final GetAuctionDiscussionCommandHandler getAuctionDiscussionCommandHandler;
    private final CloseYchCommandHandler closeYchCommandHandler;
    private final ChooseWinnerCommandHandler chooseWinnerCommandHandler;
    private final GetBiddingByUserQueryHandler getBiddingByUserQueryHandler;

    @GetMapping("/get/{id}")
    public AuctionDto getById(@PathVariable Long id) {
        return getAuctionByIdQueryHandler.handle(id);
    }

    @PostMapping("/get/list")
    public GetAuctionsListResponse getAuctionsList(@RequestBody @Valid GetAuctionsListCommand cmd) {
        return getAuctionsListQueryHandler.handle(cmd);
    }

    @PostMapping("/get/won")
    public GetWonYchsResponse getWonAuctions(@RequestBody @Valid GetWonYchsCommand cmd) {
        return getWonYchsCommandHandler.handle(cmd);
    }

    @PostMapping("/get/sold")
    public GetSoldAuctionsResponse getSoldAuctions(@RequestBody GetSoldCommand cmd) {
        return getSoldYchsCommandHandler.handle(cmd);
    }

    @GetMapping("/get/discussion/{ychId}")
    public AuctionDiscussionDto getDiscussion(@PathVariable Long ychId) {
        return getAuctionDiscussionCommandHandler.handle(ychId);
    }

    @PostMapping("/close/{ychId}")
    public GenericResponse<Void> closeAuction(@PathVariable Long ychId) {
        closeYchCommandHandler.handle(ychId);
        return success(THE_AUCTION_WAS_CLOSED);
    }

    @PostMapping("/create")
    public GenericResponse<AuctionDto> create(
            @NotNull
            @RequestParam(name = "mainArt") MultipartFile mainArt,
            @NotNull
            @RequestParam(name = "countOfDays") Long countOfDays,
            @RequestParam(required = false, name = "description") String description,
            @NotBlank
            @RequestParam(name = "currency") String currency,
            @NotNull
            @RequestParam(name = "isMaxBid") Boolean isMaxBid,
            @RequestParam(required = false, name = "maxBid") BigDecimal maxBid,
            @NotNull
            @RequestParam(name = "minBid") BigDecimal minBid,
            @NotBlank
            @RequestParam(name = "ychAuctionCharacterType") String ychAuctionCharacterType,
            @NotBlank
            @RequestParam(name = "ychAuctionType") String ychAuctionType
    ) {
        var cmd = CreateAuctionCommand.builder()
                .description(description)
                .minBid(minBid)
                .maxBid(maxBid)
                .isMaxBid(isMaxBid)
                .countOfDays(countOfDays)
                .ychAuctionType(ychAuctionType)
                .ychAuctionCharacterType(ychAuctionCharacterType)
                .auctionKind("YCH")
                .currency(currency)
                .mainArt(mainArt)
                .build();

        return success(createYchAuctionCommandHandler.handle(cmd), ResponseMessage.YCH_WAS_CREATED);
    }

    @PostMapping("/add")
    public AddBidToAuctionResponse add(@RequestBody AddBidToAuctionCommand cmd) {
        return addBidToAuctionCommandHandler.handle(cmd);
    }

    @PostMapping("/auto-buy")
    public GenericResponse<Void> autoBuy(@RequestBody AutoBuyAuctionCommand cmd) {
        autoBuyAuctionCommandHandler.handle(cmd);
        return success(THE_AUCTION_WAS_BOUGHT_BUY_YOU);
    }

    @PostMapping("/art-trade/add-winner")
    public GenericResponse<Void> autoBuy(@RequestBody ChooseWinnerCommand cmd) {
        chooseWinnerCommandHandler.handle(cmd);
        return success(WINNER_WAS_ADDED);
    }

    @GetMapping("/auctions-currently-bidding/by-user/page/{pageNumber}/records/{countOfRecords}")
    public GetBiddingByUserResponse getAuctionsCurrentlyBiddingByUser(
            @PathVariable Integer pageNumber,
            @PathVariable Integer countOfRecords
    ) {
        return getBiddingByUserQueryHandler.handle(
                pageNumber,
                countOfRecords
        );
    }
}
