package com.adrianrostkowski.artcenter.api.user;

import com.adrianrostkowski.artcenter.api.GenericResponse;
import com.adrianrostkowski.artcenter.application.user.create.CreateNewUserCommand;
import com.adrianrostkowski.artcenter.application.user.create.CreateNewUserCommandHandler;
import com.adrianrostkowski.artcenter.application.user.ensure.captcha.VerifyCaptchaCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.captcha.VerifyCaptchaCommandHandler;
import com.adrianrostkowski.artcenter.application.user.ensure.email.IsEmailFreeCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.email.IsEmailFreeCommandHandler;
import com.adrianrostkowski.artcenter.application.user.ensure.login.IsLoginFreeCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.login.IsLoginFreeCommandHandler;
import com.adrianrostkowski.artcenter.application.user.ensure.username.IsUserNameFreeCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.username.IsUserNameFreeCommandHandler;
import com.adrianrostkowski.artcenter.application.user.update.confirm.ConfirmAccountCommand;
import com.adrianrostkowski.artcenter.application.user.update.confirm.ConfirmAccountCommandHandler;
import com.adrianrostkowski.artcenter.application.user.update.password.ChangeUserPasswordCommand;
import com.adrianrostkowski.artcenter.application.user.update.password.ChangeUserPasswordCommandHandler;
import com.adrianrostkowski.artcenter.application.user.update.username.ChangeUserNameCommand;
import com.adrianrostkowski.artcenter.application.user.update.username.ChangeUserNameCommandHandler;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.adrianrostkowski.artcenter.api.ResponseMessage.*;
import static com.adrianrostkowski.artcenter.api.GenericResponse.success;

@RestController
@Transactional
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {

    private final ChangeUserPasswordCommandHandler changeUserPasswordCommandHandler;
    private final ChangeUserNameCommandHandler changeUserNameCommandHandler;
    private final IsEmailFreeCommandHandler isEmailFreeHandler;
    private final IsLoginFreeCommandHandler isLoginFreeCommandHandler;
    private final IsUserNameFreeCommandHandler isUserNameFreeCommandHandler;
    private final CreateNewUserCommandHandler createNewUserCommandHandler;
    private final ConfirmAccountCommandHandler confirmAccountCommandHandler;
    private final VerifyCaptchaCommandHandler verifyCaptchaCommandHandler;

    @GetMapping("/check-authentication")
    public GenericResponse<String> checkAuth() {
        return success(JWT_IS_VALID);
    }

    @PostMapping("/create")
    public GenericResponse<Void> create(@RequestBody @Valid CreateNewUserCommand cmd) {
        createNewUserCommandHandler.handle(cmd);
        return success(ACCOUNT_WAS_CREATED_CHECK_YOUR_EMAIL);
    }

    @PostMapping("/confirm-account")
    public GenericResponse<Void> confirmAccount(@RequestBody ConfirmAccountCommand cmd) {
        confirmAccountCommandHandler.handle(cmd);
        return success(ACCOUNT_WAS_ACTIVATED);
    }

    @PostMapping("/email/is-free")
    public Boolean isEmailFree(@RequestBody IsEmailFreeCommand cmd) {
        return isEmailFreeHandler.handle(cmd);
    }

    @PostMapping("/login/is-free")
    public Boolean isLoginFree(@RequestBody IsLoginFreeCommand cmd) {
        return isLoginFreeCommandHandler.handle(cmd);
    }

    @PostMapping("/user-name/is-free")
    public Boolean isUserNameFree(@RequestBody IsUserNameFreeCommand cmd) {
        return isUserNameFreeCommandHandler.handle(cmd);
    }

    @PostMapping("/change-password")
    public GenericResponse<Void> changePassword(@RequestBody ChangeUserPasswordCommand cmd) {
        changeUserPasswordCommandHandler.handle(cmd);
        return success(PASSWORD_WAS_CHANGED);
    }

    @PostMapping("/change-username")
    public GenericResponse<Void> changeUsername(@RequestBody ChangeUserNameCommand cmd) {
        changeUserNameCommandHandler.handle(cmd);
        return success(PASSWORD_WAS_CHANGED);
    }

    @PostMapping("/create-account/verify-captcha")
    public GenericResponse<Boolean> verifyCaptcha(@RequestBody VerifyCaptchaCommand cmd) {
        return verifyCaptchaCommandHandler.handle(cmd).equals(Boolean.TRUE) ?
                success(
                        Boolean.TRUE,
                        GOOD_JOB_YOU_ARE_NOT_A_ROBOT
                ) : success(
                Boolean.FALSE,
                BEEB_BOOP_ARE_YOU_A_ROBOT
        );
    }
}
