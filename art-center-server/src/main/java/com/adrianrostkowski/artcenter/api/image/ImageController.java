package com.adrianrostkowski.artcenter.api.image;

import com.adrianrostkowski.artcenter.application.image.get.GetImageByIdQueryHandler;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@Transactional
@RequestMapping("/api/images")
@AllArgsConstructor
public class ImageController {

    private final GetImageByIdQueryHandler getImageByIdQueryHandler;

    @GetMapping(value = "/{id}", produces = "image/png")
    public Resource getById(
            @PathVariable Long id,
            HttpServletResponse response
            ) {
//        response.setContentType(MediaType.parseMediaType(img.getFileType()));
        return getImageByIdQueryHandler.handle(id);
    }
}
