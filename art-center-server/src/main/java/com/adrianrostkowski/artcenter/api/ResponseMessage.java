package com.adrianrostkowski.artcenter.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseMessage {

    EMPTY(""),

    BID_ADDED("BID_ADDED"),
    IMG_BID_ADDED("IMG_BID_ADDED"),
    YCH_WAS_CREATED("Ych was created"),
    COMMENT_ADDED("Comment was added"),
    THE_AUCTION_WAS_BOUGHT_BUY_YOU("The auction was bought by you"),
    THE_AUCTION_WAS_CLOSED("The auction was closed by you"),
    WINNER_WAS_ADDED("Winner was added!"),

    PASSWORD_WAS_CHANGED("The password was changed!"),
    USERNAME_WAS_CHANGED("The username was changed!"),

    BEEB_BOOP_ARE_YOU_A_ROBOT("BEEP BOOP ARE YOU A ROBOT?"),
    GOOD_JOB_YOU_ARE_NOT_A_ROBOT("What a success, you JUST proved you are not a robot!"),

    ACCOUNT_WAS_CREATED_CHECK_YOUR_EMAIL("Account was created, check your email!"),
    ACCOUNT_WAS_ACTIVATED("Account was activated :)"),
    JWT_IS_VALID("JWT_IS_VALID"),
    WELCOME("Welcome !"),

    ADDED_TO_YOUR_FOLLOW_LIST("Added to your follow list :)");

    private final String message;
}
