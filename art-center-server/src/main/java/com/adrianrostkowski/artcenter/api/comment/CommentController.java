package com.adrianrostkowski.artcenter.api.comment;

import com.adrianrostkowski.artcenter.api.ResponseMessage;
import com.adrianrostkowski.artcenter.application.comment.add.AddCommentCommand;
import com.adrianrostkowski.artcenter.application.comment.add.AddCommentCommandHandler;
import com.adrianrostkowski.artcenter.api.GenericResponse;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.adrianrostkowski.artcenter.api.GenericResponse.success;

@RestController
@Transactional
@RequestMapping("/api/comment")
@AllArgsConstructor
public class CommentController {

    private final AddCommentCommandHandler addCommentCommandHandler;

    @PostMapping("/add")
    public GenericResponse<Void> add(@RequestBody @Valid AddCommentCommand cmd) {
        addCommentCommandHandler.handle(cmd);
        return success(ResponseMessage.COMMENT_ADDED);
    }
}
