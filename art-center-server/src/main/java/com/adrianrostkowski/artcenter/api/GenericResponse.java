package com.adrianrostkowski.artcenter.api;

import lombok.*;

@Getter
public class GenericResponse<T> {
    private T response;
    private boolean success;
    private String message;

    private GenericResponse(
            T response,
            boolean success,
            String message
    ) {
        this.response = response;
        this.success = success;
        this.message = message;
    }

    public static <T> GenericResponse<T> success(
            T response,
            ResponseMessage responseMessage
    ) {
        return new GenericResponse<>(
                response,
                true,
                responseMessage.getMessage()
        );
    }

    public static <T> GenericResponse<T> success(
            T response
    ) {
        return new GenericResponse<>(
                response,
                true,
                ResponseMessage.EMPTY.getMessage()
        );
    }

    public static <T> GenericResponse<T> success(ResponseMessage responseMessage) {
        return new GenericResponse<>(
                null,
                true,
                responseMessage.getMessage()
        );
    }
}
