package com.adrianrostkowski.artcenter.api.follow;

import com.adrianrostkowski.artcenter.api.GenericResponse;
import com.adrianrostkowski.artcenter.application.follow.*;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static com.adrianrostkowski.artcenter.api.ResponseMessage.ADDED_TO_YOUR_FOLLOW_LIST;

@RestController
@AllArgsConstructor
@RequestMapping("/api/follow-group")
public class FollowController {

    private final FollowCommandHandler followCommandHandler;
    private final IsAlreadyInFollowGroupQueryHandler isAlreadyInFollowGroupQueryHandler;

    @GetMapping("/get/{userId}")
    public void getListOfFollowingArtists(Long userId) {

    }

    @GetMapping("/is-already-in")
    public IsAlreadyInFollowGroupResponse isAlreadyIn(@RequestBody IsAlreadyInFollowGroupQuery query) {
        return isAlreadyInFollowGroupQueryHandler.handle(query);
    }

    @PostMapping("/add")
    public GenericResponse<Void> addToArtistGroup(@RequestBody FollowCommand cmd) {
        followCommandHandler.handle(cmd);
        return GenericResponse.success(ADDED_TO_YOUR_FOLLOW_LIST);
    }
}
