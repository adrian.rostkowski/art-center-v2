package com.adrianrostkowski.artcenter.api.user;

import com.adrianrostkowski.artcenter.api.GenericResponse;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.adrianrostkowski.artcenter.api.GenericResponse.success;
import static com.adrianrostkowski.artcenter.api.ResponseMessage.WELCOME;

@RestController
@Transactional
@RequestMapping("/api/login")
public class AuthenticationController {

    @PostMapping
    public GenericResponse<String> authenticate() {
        return success(WELCOME);
    }

}
