package com.adrianrostkowski.artcenter.api.imgbid;

import com.adrianrostkowski.artcenter.application.bid.add.AddBidToAuctionResponse;
import com.adrianrostkowski.artcenter.api.ResponseMessage;
import com.adrianrostkowski.artcenter.api.GenericResponse;
import com.adrianrostkowski.artcenter.application.imgbid.add.AddImgBidCommand;
import com.adrianrostkowski.artcenter.application.imgbid.add.AddImgBidCommandHandler;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static com.adrianrostkowski.artcenter.api.GenericResponse.success;

@RestController
@Transactional
@RequestMapping("/api/img-bid")
@AllArgsConstructor
public class ImgBidController {

    private final AddImgBidCommandHandler addImgBidCommandHandler;

    @PostMapping("/add")
    public GenericResponse<AddBidToAuctionResponse> add(
            @RequestParam(name = "img") MultipartFile img,
            @RequestParam("auctionId") Long auctionId,
            @RequestParam("description") String description
    ) {
        var cmd = AddImgBidCommand.builder()
                .auctionId(auctionId)
                .description(description)
                .build();

        addImgBidCommandHandler.handle(cmd, img);
        return success(ResponseMessage.IMG_BID_ADDED);
    }
}
