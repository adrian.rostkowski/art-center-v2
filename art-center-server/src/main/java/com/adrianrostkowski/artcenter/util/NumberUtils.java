package com.adrianrostkowski.artcenter.util;

import java.math.BigDecimal;

public final class NumberUtils {

    private NumberUtils() {}

    public static boolean isBiggerOrEquals(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) >= 0;
    }
}
