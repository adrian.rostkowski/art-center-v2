package com.adrianrostkowski.artcenter.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SESAWSConfig {
    @Value("${amazon.aws.ses.access-key-secret}")
    private String secretAccessKey = "AKIAYX4CVUGLOJBHW5DL";
    @Value("${amazon.aws.ses.access-key-id}")
    private String accessKeyId = "QELDBBPYq8EfiEIdV7TaQbTHbIuS6xWkXt+ZqPWj";

    @Bean
    public AmazonSimpleEmailServiceClient generateSESClient() {
        return (AmazonSimpleEmailServiceClient) AmazonSimpleEmailServiceClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(secretAccessKey, accessKeyId)))
                .build();
    }

}
