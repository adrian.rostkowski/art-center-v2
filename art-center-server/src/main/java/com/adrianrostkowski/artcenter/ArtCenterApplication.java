package com.adrianrostkowski.artcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAsync
@EnableTransactionManagement
@EnableJpaRepositories
@EnableJpaAuditing
@EnableScheduling
@EnableCaching
//@EnableAs
@SpringBootApplication()
public class ArtCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArtCenterApplication.class, args);
    System.out.println("-----------------------------------------------------------------");
    System.out.println("-----------------------------------------------------------------");
    System.out.println("-----------------------------------------------------------------");
    System.out.println("-----------------------------------------------------------------");
    System.out.println("-----------------------------------------------------------------");
    System.out.println("-----------------------------------------------------------------");
    }

}
