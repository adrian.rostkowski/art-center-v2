package com.adrianrostkowski.artcenter.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface ApplicationUserDetails extends UserDetails {
}
