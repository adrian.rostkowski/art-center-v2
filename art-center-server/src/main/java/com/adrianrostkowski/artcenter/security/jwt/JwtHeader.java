package com.adrianrostkowski.artcenter.security.jwt;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum JwtHeader {
    INVALID_JWT,
    JWT_TIME_OUT;

    public static final String JWT_HEADER = "AuthorizationError";
}
