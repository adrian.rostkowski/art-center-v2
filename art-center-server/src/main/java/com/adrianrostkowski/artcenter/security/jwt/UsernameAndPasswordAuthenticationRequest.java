package com.adrianrostkowski.artcenter.security.jwt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UsernameAndPasswordAuthenticationRequest {
    private String password;
    private String username;

    @Override
    public String toString() {
        return "UsernameAndPasswordAuthenticationRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
