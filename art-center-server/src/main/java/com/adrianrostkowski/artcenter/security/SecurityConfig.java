package com.adrianrostkowski.artcenter.security;

import com.adrianrostkowski.artcenter.security.jwt.JwtUsernameAndPasswordAuthenticationFilter;
import com.adrianrostkowski.artcenter.security.jwt.JwtVerifier;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder customBasicEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        JwtUsernameAndPasswordAuthenticationFilter jwtUsernameAndPasswordAuthenticationFilter = new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager());
        jwtUsernameAndPasswordAuthenticationFilter.setFilterProcessesUrl("/api/login");
        http
                //! now we can send post request from the same port
                .csrf().disable()
                /* *
                * JWT authorization should be STATELESS
                * */
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                /* *
                * Add our custom override AuthenticationFiler.
                * This filter will be trigger ONLY on /login POST endpoint.
                * */
                .addFilter(jwtUsernameAndPasswordAuthenticationFilter)
                /* *
                * ...
                * */
                .addFilterAfter(new JwtVerifier(), JwtUsernameAndPasswordAuthenticationFilter.class)
//                .addFilterBefore(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class)
//                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
//                .headers().frameOptions().disable()
//                    .and()
                .authorizeRequests()
                .antMatchers(
                        "/api/login",
                        "/api/login/t"
//                        "/api/s3/buckets",
//                        "/api/s3/save-img",
//                        "/api/s3/get-img",
//                        "/api/user/create-account/verify-captcha",
//                        "/api/auction/get/list",
//                        "/api/images/**",
//                        "/api/auction/get/**",
//                        "/api/user/login/is-free",
//                        "/api/user/email/is-free",
//                        "/api/user/user-name/is-free",
//                        "/api/user/confirm-account",
//                        "/api/user/create",
//                        "/api/user/authenticate",
//                        "/api/bid/get",
//                        "/api/auction/hello",
//                        "/api/user/check-authentication"
                ).permitAll()
                .anyRequest().authenticated();
//                    .and()
//                .exceptionHandling();

        http.cors().disable();
//        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    System.out.println("secutiry change");
    System.out.println("secutiry change3");
    System.out.println("secutiry change4");
    System.out.println("secutiry change5");
    System.out.println("secutiry change6");
    System.out.println("secutiry change7");
    System.out.println("secutiry change8");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(customBasicEncoder());
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOriginPatterns(Collections.singletonList("*"));
        config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept", "responseType", "Authorization"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
