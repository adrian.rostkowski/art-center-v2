package com.adrianrostkowski.artcenter.security.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.util.Strings;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.adrianrostkowski.artcenter.security.jwt.JwtHeader.*;
import static com.adrianrostkowski.artcenter.security.jwt.JwtUsernameAndPasswordAuthenticationFilter.BEARER;
import static com.adrianrostkowski.artcenter.security.jwt.JwtUsernameAndPasswordAuthenticationFilter.SECRET;

@Log4j2
public class JwtVerifier extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain
    ) throws ServletException, IOException {

        /* *
         * Get header with JWT
         * */
        String authorizationHeader = httpServletRequest.getHeader("Authorization");

        if (Strings.isNullOrEmpty(authorizationHeader) || !authorizationHeader.startsWith(BEARER)) {
            /* *
             * Let chain go forward, without set up security context
             * user never reach protected endpoints
             * */
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        try {
            String token = authorizationHeader.replace(BEARER, "");

            /* *
             * Decrypt token using secret token
             * */
            Claims body = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();

            // ! in body variable, we have method to add extra time to live for JWT!
            String userName = body.getSubject();
            /* *
             * Get, in this case, user roles
             * but this is ONLY simple map of values.
             * Well there can be any value, keep this in mind and be ready to add there any extra value :)
             * */
            var authorities = (List<Map<String, String>>) body.get("authorities");

            List<SimpleGrantedAuthority> simpleGrantedAuthorities = authorities.stream()
                    .map(n -> new SimpleGrantedAuthority(n.get("authority")))
                    .toList();

            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    userName,
                    null,
                    simpleGrantedAuthorities
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);

            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (ExpiredJwtException e) {
            log.info("JWT timeout\n" + e);
            httpServletResponse.setHeader(JWT_HEADER, JWT_TIME_OUT.name());
        } catch (JwtException e) {
            log.info("JWT invalid\n" + e);
            httpServletResponse.setHeader(JWT_HEADER, INVALID_JWT.name());
        }

    }
}
