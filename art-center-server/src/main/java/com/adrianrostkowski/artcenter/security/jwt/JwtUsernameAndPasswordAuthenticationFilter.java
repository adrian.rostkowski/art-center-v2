package com.adrianrostkowski.artcenter.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;


/* *
 * We went to override SpringBoot default UsernamePasswordAuthenticationFilter
 * This Filter will be ONLY trigger by any request on /login POST endpoint
 * */
public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public static final String BEARER = "Bearer ";
    public static final String SECRET = "RarityIsBestPony";

    public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    /* *
     * This method will be trigger every single request
     * Should return correctly setup Authentication object with basic user info data
     * */
    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response
    ) throws AuthenticationException {
        try {
            /* *
             * Map request data to an custom Object
             * */
            UsernameAndPasswordAuthenticationRequest authenticationRequest =
                    new ObjectMapper().readValue(
                            request.getInputStream(),
                            UsernameAndPasswordAuthenticationRequest.class
                    );

            /* *
             * Prepare Authentication object, with is required for the AuthenticationManager
             * */
            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(),
                    authenticationRequest.getPassword()
            );

            /* *
             * Return result of authentication
             * If something goes wrong, authentication() will throw AuthenticationException
             * For example, when passed data was wrong like username or password
             * */
            return getAuthenticationManager().authenticate(authentication);
        } catch (IOException e) {
            throw new IllegalStateException("An error occurred during user authentication\n" + e);
        }
    }

    /* *
     * This method will be invoked ONLY when attemptAuthentication() will end with success
     * the Authentication authResult parameter is an object from attemptAuthentication() method
     * */
    @Override
    protected void successfulAuthentication(
            HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult
    ) throws IOException, ServletException {

        String token = Jwts. builder()
                           /* *
                            * authResult.getName() will return username
                            * this is a header part of JWT, you can check every part of this token on jwt.io
                            * */
                           .setSubject(authResult.getName())
                           /* *
                            * Body part of JWT
                            * For example, roles of current user
                            * */
                           .claim("authorities", authResult.getAuthorities())
                           .setIssuedAt(new Date())
                           /* *
                            * Duration of JWT
                            * */
                           .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusWeeks(2)))
                           .signWith(SignatureAlgorithm.HS256, SECRET)
                           .compact();
        /* *
         * Add JWT to response header
         * */
        response.addHeader("Authorization", BEARER + token);
        /* *
         * Chain to next filters
         * */
        chain.doFilter(request, response);

        super.successfulAuthentication(request, response, chain, authResult);
    }
}
