package com.adrianrostkowski.artcenter.application.user.ensure.captcha;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;


@Service
@Log4j2
public class VerifyCaptchaCommandHandler {

    public Boolean handle(VerifyCaptchaCommand cmd) {

        String url = "https://www.google.com/recaptcha/api/siteverify";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add(
                "secret",
                "6LfNNVQaAAAAAIuyNRlTHyETtZs4FpmNwXQ-6ozt"
        );
        map.add(
                "response",
                cmd.getResponse()
        );

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(
                map,
                headers
        );

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<ReCaptchaResponse> responseEntity = restTemplate.postForEntity(
                url,
                request,
                ReCaptchaResponse.class
        );

        return Optional.ofNullable(responseEntity.getBody())
                .map(ReCaptchaResponse::isSuccess)
                .orElseGet(() -> {
                    log.warn("An error occurred during communication with [%s], result : [%s]"
                            .formatted(url, responseEntity.getBody()));
                    return false;
                });
    }

}
