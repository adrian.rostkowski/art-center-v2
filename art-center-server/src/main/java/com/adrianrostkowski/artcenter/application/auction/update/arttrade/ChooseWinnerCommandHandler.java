package com.adrianrostkowski.artcenter.application.auction.update.arttrade;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.ApplicationIsOnBiddingStep;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.ApplicationIsProcessingByOwner;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.AuctionKindEqualsTo;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.WinnerIsInBidList;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class ChooseWinnerCommandHandler {

    @Value("${email.url.auction.discussion}")
    private String discussionUrl;
    private final AuctionRepository auctions;
    private final EmailService emailService;

    public ChooseWinnerCommandHandler(
            AuctionRepository auctions,
            EmailService emailService
    ) {
        this.auctions = auctions;
        this.emailService = emailService;
    }

    public void handle(ChooseWinnerCommand cmd) {
        final var auction = auctions.findById(cmd.getAuctionId()).orElseThrow();
        final var winningBid = getWinningBid(cmd, auction);
        final var winner = winningBid.getOwner();

        validate(auction, winner);

        winningBid.setAsWinning();
        auction.endAuctionWithAWinner(emailService, discussionUrl);

        sendEmailNotifications(auction, winner);
    }

    private ImgBid getWinningBid(ChooseWinnerCommand cmd, Auction auction) {
        return auction.getImgBids().stream()
                .filter(bid -> bid.getId().equals(cmd.getWinningBidId()))
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Can not find bid with id : [%s]".formatted(cmd.getWinningBidId())));
    }

    private void validate(Auction auction, User winner) {
        AuctionKindEqualsTo.ensureIsTrueFor(auction, AuctionKind.ART_TRADE);
        ApplicationIsProcessingByOwner.ensureIsTrueFor(auction);
        WinnerIsInBidList.ensureIsTrueFor(auction, winner);
        ApplicationIsOnBiddingStep.ensureIsTrueFor(auction);
    }

    private void sendEmailNotifications(Auction auction, User winner) {
        emailService.sendAuctionEndNotification(winner.getEmail(), discussionUrl + auction.getId());
        emailService.sendAuctionEndNotification(auction.getOwner().getEmail(), discussionUrl + auction.getId());
    }
}
