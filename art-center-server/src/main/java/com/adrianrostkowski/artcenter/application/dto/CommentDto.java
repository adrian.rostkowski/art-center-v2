package com.adrianrostkowski.artcenter.application.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CommentDto {
    private String text;
    private String ownerName;
    private Long commentId;
}
