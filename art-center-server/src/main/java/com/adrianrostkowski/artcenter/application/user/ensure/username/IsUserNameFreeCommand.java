package com.adrianrostkowski.artcenter.application.user.ensure.username;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

@AllArgsConstructor
@Getter
public class IsUserNameFreeCommand {
    @Length(min = 6, max = 20)
    private String userName;
}
