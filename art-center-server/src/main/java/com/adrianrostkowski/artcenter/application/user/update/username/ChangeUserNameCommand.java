package com.adrianrostkowski.artcenter.application.user.update.username;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ChangeUserNameCommand {
    private String currentPassword;
    private String newUsername;
}
