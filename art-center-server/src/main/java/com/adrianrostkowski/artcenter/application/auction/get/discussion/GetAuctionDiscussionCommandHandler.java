package com.adrianrostkowski.artcenter.application.auction.get.discussion;

import com.adrianrostkowski.artcenter.application.dto.AuctionDiscussionDto;
import com.adrianrostkowski.artcenter.application.mapper.AuctionMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.ApplicationIsOnDiscussionOrCloseStep;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.ApplicationIsProcessingByOwnerOrWinner;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class GetAuctionDiscussionCommandHandler {

    private final AuctionRepository auctionRepository;

    public AuctionDiscussionDto handle(Long id) {
        return auctionRepository.findById(id).stream()
                .peek(ApplicationIsOnDiscussionOrCloseStep::ensureIsTrueFor)
                .peek(ApplicationIsProcessingByOwnerOrWinner::ensureIsTrueFor)
                .map(AuctionMapper::toYchDiscussionDto)
                .findAny()
                .orElseThrow();
    }
}
