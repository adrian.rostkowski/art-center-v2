package com.adrianrostkowski.artcenter.application.comment.add.addstrategy;

import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.comment.Comment;
import com.adrianrostkowski.artcenter.domain.model.comment.CommentRepository;
import com.adrianrostkowski.artcenter.domain.model.comment.CommentType;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddCommentToAuctionBiddingAuction implements AddCommentStrategy {

    private final AuctionRepository ychs;
    private final UserRepository users;
    private final CommentRepository comments;

    @Override
    public void execute(Long id, String text) {
        var user = users.findByLogin(UserContextHelper.getCurrentUser().getLogin());
        var ych = ychs.findById(id).orElseThrow();

        var comment = new Comment(user, text, ych, CommentType.BIDDING);
        var savedComment = comments.save(comment);

        ych.addComment(savedComment);
    }
}
