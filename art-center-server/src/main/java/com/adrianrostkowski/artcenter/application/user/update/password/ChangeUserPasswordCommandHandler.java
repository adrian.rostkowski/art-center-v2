package com.adrianrostkowski.artcenter.application.user.update.password;

import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.domain.model.user.specification.NewPasswordChangeIsCorrect;
import com.adrianrostkowski.artcenter.domain.model.user.specification.UserPasswordsAreEquals;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class ChangeUserPasswordCommandHandler {

    private final UserRepository users;

    @Transactional
    public void handle(ChangeUserPasswordCommand cmd) {
        var userToChange = users.findByLogin(UserContextHelper.getCurrentUser().getLogin());

        UserPasswordsAreEquals.ensureIsTrueFor(userToChange, cmd.getCurrentPassword());
        NewPasswordChangeIsCorrect.ensureIsTrueFor(userToChange, cmd.getNewPassword());

        var hashedNewPassword = BCrypt.hashpw(cmd.getNewPassword(), BCrypt.gensalt());
        userToChange.changePassword(hashedNewPassword);
    }
}
