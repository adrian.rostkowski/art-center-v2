package com.adrianrostkowski.artcenter.application.dto;

import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
public class AuctionCardDto {
    private Long ychId;
    private Long imageId;
    private String artistName;
    private BigDecimal price;
    private BigDecimal minBid;
    private Long daysToEnd;
    private String auctionStatus;
    private BidCurrency currency;
}
