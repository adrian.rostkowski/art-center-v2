package com.adrianrostkowski.artcenter.application.test.customannotation;

public class TestDummy {

    @MyAnnotation(
            value = "value1",
            name = "name1",
            age = 11,
            newNames = { "AAA1", "BBB1" },
            validator = AnnotationValidatorTestImp.class
    )
    public String nameDummy;

    @MyAnnotation(
            value = "value2",
            name = "name2",
            age = 22,
            newNames = { "AAA2", "BBB2" },
            validator = AnnotationValidatorTestImp.class
    )
    public String surname;

    public String sayHi() {
        return "The hi";
    }
}
