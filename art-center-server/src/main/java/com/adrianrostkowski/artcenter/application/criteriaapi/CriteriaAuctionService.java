package com.adrianrostkowski.artcenter.application.criteriaapi;

import com.adrianrostkowski.artcenter.application.auction.get.mainpage.GetAuctionsListCommand;
import com.adrianrostkowski.artcenter.application.auction.get.mainpage.UnitExpression;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;

@Service
public class CriteriaAuctionService {

    public Predicate getByPriceRange(
            Integer maxValue,
            CriteriaBuilder cb,
            Join<Auction, Bid> bids,
            GetAuctionsListCommand query,
            CriteriaQuery<Auction> q,
            Root<Auction> root
    ) {
        final var value = "value";
        if (maxValue < 100) {
            return cb.and(
                    cb.or(
                            cb.ge(bids.get(value), getMaxBid(cb, q, root)),
                            cb.isEmpty(root.get("bids"))
                    ),
                    cb.ge(bids.get(value), query.getPriceRangeFrom()),
                    cb.le(bids.get(value), query.getPriceRangeTo())
            );
        } else {
            return cb.and(
                    cb.or(
                            cb.ge(bids.get(value), getMaxBid(cb, q, root)),
                            cb.isEmpty(root.get("bids"))
                    ),
                    cb.ge(bids.get(value), query.getPriceRangeFrom()),
                    cb.ge(bids.get(value), query.getPriceRangeFrom())
            );
        }
    }

    public Predicate getByMinBidValue(GetAuctionsListCommand query, Root<Auction> root, CriteriaBuilder cb) {
        final var bids = "bids";
        final var minBid = "minBid";
        return cb.and(
                cb.isEmpty(root.get(bids)),
                cb.ge(root.get(minBid), query.getPriceRangeFrom()),
                cb.le(root.get(minBid), query.getPriceRangeTo())
        );
    }

    public Expression<Integer> getDaysToEnd(CriteriaBuilder cb, Root<Auction> root) {
        Expression<String> day = new UnitExpression(null, String.class, "DAY");
        return cb.function(
                "TIMESTAMPDIFF",
                Integer.class,
                day,
                cb.currentDate(),
                root.get("endDate")
        );
    }

    public Subquery<Integer> getMaxBid(CriteriaBuilder cb, CriteriaQuery<Auction> q, Root<Auction> root) {
        Subquery<Integer> subQuery = q.subquery(Integer.class);
        Root<Bid> rootSq = subQuery.from(Bid.class);
        subQuery.select(cb.max(rootSq.get("value")));
        subQuery.where(cb.equal(rootSq.get("auction"), root.get("id")));
        return subQuery;
    }

    public Predicate getByPriceRange(
            GetAuctionsListCommand query,
            Root<Auction> root,
            CriteriaBuilder cb,
            Join bids,
            CriteriaQuery q
    ) {
        return cb.or(
                getByPriceRange(query.getPriceRangeTo(), cb, bids, query, q, root),
                getByMinBidValue(query, root, cb)
        );
    }
}
