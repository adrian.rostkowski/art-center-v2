package com.adrianrostkowski.artcenter.application.validators.decimal;

import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.util.function.BiPredicate;

@AllArgsConstructor
public enum DecimalValidatorMode {
    MUST_BE_HIGHER(
            (field, fieldMatch) -> field.compareTo(fieldMatch) > 0
    );

    private final BiPredicate<BigDecimal, BigDecimal> valid;
    public boolean isValid(BigDecimal field, BigDecimal fieldMatch) {
        return valid.test(field, fieldMatch);
    }
}
