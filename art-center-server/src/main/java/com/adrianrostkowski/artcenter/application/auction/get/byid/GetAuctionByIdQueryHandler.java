package com.adrianrostkowski.artcenter.application.auction.get.byid;

import com.adrianrostkowski.artcenter.application.dto.AuctionDto;
import com.adrianrostkowski.artcenter.application.follow.IsAlreadyInFollowGroupQuery;
import com.adrianrostkowski.artcenter.application.follow.IsAlreadyInFollowGroupQueryHandler;
import com.adrianrostkowski.artcenter.application.follow.IsAlreadyInFollowGroupResponse;
import com.adrianrostkowski.artcenter.application.mapper.AuctionMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class GetAuctionByIdQueryHandler {

    private final AuctionRepository ychs;
    private final IsAlreadyInFollowGroupQueryHandler isAlreadyInFollowGroupQueryHandler;

    public AuctionDto handle(Long auctionId) {
        return ychs.findById(auctionId)
                .map(auction -> {
                    var isInFollowList = isAlreadyInFollowGroupQueryHandler.handle(
                            new IsAlreadyInFollowGroupQuery(
                                    auction.getOwner().getUsername()
                            )
                    );

                    return AuctionMapper.fromEntity(auction, isInFollowList.getIsAlready());
                })
                .orElseThrow();

    }

}
