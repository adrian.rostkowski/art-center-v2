package com.adrianrostkowski.artcenter.application.test.customannotation;

import java.lang.reflect.Field;

public class TestMain {
  public static void main(String[] args) throws IllegalAccessException {
      final var testDummy = new TestDummy();

      testDummy.nameDummy = "Adrian";
      testDummy.surname = "Rostkowski";

      for (Field field : testDummy.getClass().getDeclaredFields()) {
          MyAnnotation dbField = field.getAnnotation(MyAnnotation.class);
          System.out.println("field name: " + dbField.name());

          // changed the access to public
          field.setAccessible(true);
          Object value = field.get(testDummy);
          System.out.println("field value: " + value);

          System.out.println("field type: " + dbField.age());
          System.out.println("is primary: " + dbField.newNames());
          System.out.println("validator: " + dbField.validator());
          AnnotationValidatorTestImp validator = new AnnotationValidatorTestImp();
          dbField.validator().cast(validator);

          final var result = validator.isValid("1231");
          System.out.println("result of validator: " + result);

          System.out.println("---");
      }
  }
}
