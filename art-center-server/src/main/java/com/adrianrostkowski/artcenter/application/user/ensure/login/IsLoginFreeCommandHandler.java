package com.adrianrostkowski.artcenter.application.user.ensure.login;

import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class IsLoginFreeCommandHandler {

    private final UserRepository users;

    public Boolean handle(IsLoginFreeCommand cmd) {
        var loginCount = users.countAllByLogin(cmd.getLogin());
        return loginCount == 0L;
    }
}
