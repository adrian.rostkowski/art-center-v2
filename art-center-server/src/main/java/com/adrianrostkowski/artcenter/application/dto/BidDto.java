package com.adrianrostkowski.artcenter.application.dto;

import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
@AllArgsConstructor
public class BidDto {
    private String ownerName;
    private BigDecimal bidValue;
    private Long avatarId;
    private String currency;

    public static BidDto createInitialBid(BigDecimal startingPrice, BidCurrency currency) {
        return new BidDto(
                "Starting Price",
                startingPrice,
                null,
                currency.getViewName()
        );
    }
}
