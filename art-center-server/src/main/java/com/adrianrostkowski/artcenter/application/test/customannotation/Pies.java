package com.adrianrostkowski.artcenter.application.test.customannotation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Pies {
    private String a;
    private String b;
    private String c;
    private String d;
    private String r;
    private String f;
    private String g;
    private String k;
    private String l;
    private String o;
    private String p;
//
//    public Pies(
//            String a,
//            String b,
//            String c,
//            String d,
//            String r,
//            String f,
//            String g,
//            String k,
//            String l,
//            String o,
//            String p
//    ) {
//        this.a = a;
//        this.b = b;
//        this.c = c;
//        this.d = d;
//        this.r = r;
//        this.f = f;
//        this.g = g;
//        this.k = k;
//        this.l = l;
//        this.o = o;
//        this.p = p;
//    }
//
//    public String getA() {
//        return a;
//    }
//
//    public String getB() {
//        return b;
//    }
//
//    public String getC() {
//        return c;
//    }
//
//    public String getD() {
//        return d;
//    }
//
//    public String getR() {
//        return r;
//    }
//
//    public String getF() {
//        return f;
//    }
//
//    public String getG() {
//        return g;
//    }
//
//    public String getK() {
//        return k;
//    }
//
//    public String getL() {
//        return l;
//    }
//
//    public String getO() {
//        return o;
//    }
//
//    public String getP() {
//        return p;
//    }
//
//    public void setA(String a) {
//        this.a = a;
//    }
//
//    public void setB(String b) {
//        this.b = b;
//    }
//
//    public void setC(String c) {
//        this.c = c;
//    }
//
//    public void setD(String d) {
//        this.d = d;
//    }
//
//    public void setR(String r) {
//        this.r = r;
//    }
//
//    public void setF(String f) {
//        this.f = f;
//    }
//
//    public void setG(String g) {
//        this.g = g;
//    }
//
//    public void setK(String k) {
//        this.k = k;
//    }
//
//    public void setL(String l) {
//        this.l = l;
//    }
//
//    public void setO(String o) {
//        this.o = o;
//    }
//
//    public void setP(String p) {
//        this.p = p;
//    }
}
