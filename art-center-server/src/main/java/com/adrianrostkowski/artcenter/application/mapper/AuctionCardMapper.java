package com.adrianrostkowski.artcenter.application.mapper;

import com.adrianrostkowski.artcenter.application.dto.AuctionCardDto;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AuctionCardMapper {

    public static AuctionCardDto fromEntity(Auction entity) {
        return AuctionCardDto.builder()
                .ychId(entity.getId())
                .artistName(entity.getOwner().getUsername())
                .daysToEnd(entity.getAuctionStep() == AuctionStep.BIDDING ? entity.daysToEnd() : 0)
                .imageId(entity.getImage().getId())
                .price(entity.getHighestBidValue())
                .auctionStatus(entity.getAuctionStep().getStepName())
                .minBid(entity.getStartingBid())
                .currency(entity.getCurrency())
                .build();
    }
}
