package com.adrianrostkowski.artcenter.application.test.customannotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface MyAnnotation {
    String   value();

    String   name();
    int      age();
    String[] newNames();
    Class<? extends AnnotationValidatorTestImp> validator();
}
