package com.adrianrostkowski.artcenter.application.follow;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@Getter
public class IsAlreadyInFollowGroupQuery {
    @NotBlank
    private String artistName;
}
