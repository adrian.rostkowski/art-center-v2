package com.adrianrostkowski.artcenter.application.auction.factory;

import com.adrianrostkowski.artcenter.application.auction.create.CreateAuctionCommand;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public abstract class CreateAuction {

    public static CreateAuction chooseStrategy(AuctionKind auctionKind) {
        switch (auctionKind) {
            case YCH -> {
                return new CreateYchAuction();
            }
            case ADOPT -> {
                return new CreateAdoptAuction();
            }
            case ART_TRADE -> {
                return new CreateArtTradeAuction();
            }
            default -> throw new IllegalStateException("Create Auction Strategy not found");
        }
    }

    public abstract Auction createAuction(
            CreateAuctionCommand cmd,
            User user,
            List<MultipartFile> images,
            AuctionImageRepository imagesRepository,
            S3ImagesService s3ImagesService
    );
}
