package com.adrianrostkowski.artcenter.application.test.customannotation;

public class AnnotationValidatorTestImp implements AnnotationValidatorTestI {

    @Override
    public boolean isValid(String name) {
        return name.length() > 3;
    }
}
