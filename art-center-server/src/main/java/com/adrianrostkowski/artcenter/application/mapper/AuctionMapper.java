package com.adrianrostkowski.artcenter.application.mapper;

import com.adrianrostkowski.artcenter.application.dto.AuctionDto;
import com.adrianrostkowski.artcenter.application.dto.AuctionDiscussionDto;
import com.adrianrostkowski.artcenter.application.dto.BidDto;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.comment.Comment;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.adrianrostkowski.artcenter.application.dto.BidDto.createInitialBid;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AuctionMapper {

    public static AuctionDto fromEntity(
            Auction entity,
            Boolean isInFollowList
    ) {
        return AuctionDto.builder()
                .ychAuctionId(entity.getId())
                .beginning(entity.getBeginning())
                .endDate(entity.getEndDate())
                .description(entity.getDescription())
                .imageId(entity.getImage().getId())
                .isAutoBuy(entity.getIsMaxBid())
                .autoBuyValue(entity.getAutoBuyBidValue())
                .minBid(entity.getStartingBid())
                .artistName(entity.getOwner().getUsername())
                .bids(toBidDto(entity))
                .comments(entity.getComments().stream()
                                  .filter(isOnSpecificStep(AuctionStep.BIDDING))
                                  .map(CommentMapper::fromEntity)
                                  .collect(Collectors.toList()))
                .secondsToEnd(entity.getSecondsToEnd())
                .step(entity.getAuctionStep())
                .auctionIsOver(entity.getAuctionStep() != AuctionStep.BIDDING || entity.getSecondsToEnd() <= 0)
                .supportImagesId(getSupportImages(entity))
                .auctionKind(entity.getKind())
                .currency(Optional.ofNullable(entity.getCurrency()).map(BidCurrency::getViewName).orElse(null))
                .imgBids(entity.getImgBids().stream().map(ImgBidMapper::fromEntity).collect(Collectors.toList()))
                .isOwnerChooseTime(entity.isOwnerChooseTime())
                .availableActions(entity.getAvailableActions())
                .isUserInArtistFollowList(isInFollowList)
                .build();
    }

    @NotNull
    private static List<BidDto> toBidDto(Auction entity) {
        final var collect = entity.getSortedBids().stream()
                .map(BidMapper::fromEntity)
                .collect(Collectors.toList());

        if (entity.getStartingBid() != null) {
            collect.add(createInitialBid(entity.getStartingBid(), entity.getCurrency()));
        }

        return collect;
    }

    public static AuctionDiscussionDto toYchDiscussionDto(Auction entity) {
        return AuctionDiscussionDto.builder()
                .comments(entity.getComments().stream()
                                  .filter(isOnSpecificStep(AuctionStep.CONSULTATIONS))
                                  .map(CommentMapper::fromEntity)
                                  .collect(Collectors.toList()))
                .description(entity.getDescription())
                .ownerId(entity.getOwner().getId())
                .ownerName(entity.getOwner().getUsername())
                .price(entity.getHighestBidValue())
                .winnerId(entity.getWinner().getId())
                .winnerName(entity.getWinner().getUsername())
                .ychId(entity.getId())
                .imageId(entity.getImage().getId())
                .userIsOwner(entity.getOwner().getLogin().equals(UserContextHelper.getCurrentUser().getLogin()))
                .step(entity.getAuctionStep())
                .auctionKind(entity.getKind())
                .build();
    }

    private static List<Long> getSupportImages(Auction auction) {
        if (Objects.isNull(auction.getSupportImages())) {
            return Collections.emptyList();
        }
        return auction.getSupportImages().stream().map(BaseEntity::getId).collect(Collectors.toList());
    }

    private static Predicate<Comment> isOnSpecificStep(AuctionStep bidding) {
        return n -> n.getStep().equals(bidding);
    }

//    private static BidDto createInitialBid(BigDecimal startingPrice, BidCurrency currency) {
//        return new BidDto(
//                "Starting Price",
//                startingPrice,
//                null,
//                currency.getViewName()
//        );
//    }
}
