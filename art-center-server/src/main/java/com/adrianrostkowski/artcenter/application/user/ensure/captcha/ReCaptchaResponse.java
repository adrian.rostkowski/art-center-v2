package com.adrianrostkowski.artcenter.application.user.ensure.captcha;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ReCaptchaResponse {
    private boolean success;
    private String hostname;
    private String challengeTs;
}
