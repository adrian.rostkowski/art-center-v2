package com.adrianrostkowski.artcenter.application.auction.factory;

import com.adrianrostkowski.artcenter.application.auction.create.CreateAuctionCommand;
import com.adrianrostkowski.artcenter.domain.model.auction.*;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.image.ImageType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

class CreateAdoptAuction extends CreateAuction {
    @Override
    public Auction createAuction(
            CreateAuctionCommand cmd,
            User user,
            List<MultipartFile> images,
            AuctionImageRepository imagesRepository,
            S3ImagesService s3ImagesService
    ) {
        var mainImage = imagesRepository.save(
                AuctionImage.create(
                        images.get(0),
                        ImageType.YCH_AUCTION
                )
        );
        mainImage.sendToElectronicArchive(s3ImagesService);

        var supportImages = images.stream()
                // TODO why ADOPT AUCTION?
                .map(suppImgRaw -> imagesRepository.save(AuctionImage.create(suppImgRaw, ImageType.ADOPT_AUCTION)))
                .peek(suppImg -> suppImg.sendToElectronicArchive(s3ImagesService))
                .toList();

        var createdAt = LocalDateTime.now();
        var createdAdoptAuction = Auction.createBase(
                user,
                mainImage,
                cmd.getDescription(),
                cmd.getMinBid(),
                cmd.getMaxBid(),
                cmd.getIsMaxBid(),
                createdAt,
                createdAt.plusDays(cmd.getCountOfDays()),
                YchAuctionType.valueOf(cmd.getYchAuctionType()),
                YchAuctionCharacterType.valueOf(cmd.getYchAuctionCharacterType()),
                BidCurrency.valueOf(cmd.getCurrency())
        );
        createdAdoptAuction.setKind(AuctionKind.ADOPT);
        createdAdoptAuction.setSupportImages(supportImages);


        supportImages.forEach(n -> n.addAuctionToSupport(createdAdoptAuction));

        return createdAdoptAuction;
    }
}
