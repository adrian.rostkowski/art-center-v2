package com.adrianrostkowski.artcenter.application.auction.get.sold;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GetSoldCommand {
    private Integer pageSize;
    private Integer pageNumber;
}
