package com.adrianrostkowski.artcenter.application.user.update.username;

import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.domain.model.user.specification.UserPasswordsAreEquals;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class ChangeUserNameCommandHandler {

    private final UserRepository users;

    @Transactional
    public void handle(ChangeUserNameCommand cmd) {
        var userToChange = users.findByLogin(UserContextHelper.getCurrentUser().getLogin());

        UserPasswordsAreEquals.ensureIsTrueFor(
                userToChange,
                cmd.getCurrentPassword()
        );

        userToChange.changeUsername(cmd.getNewUsername());
    }
}
