package com.adrianrostkowski.artcenter.application.image.get;

import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class GetImageByIdQueryHandler {

    private final AuctionImageRepository images;
    private final S3ImagesService s3ImagesService;

    public Resource handle(Long id) {
        var found = images.findById(id).orElseThrow();

        return found.getData() != null ?
                new ByteArrayResource(found.getData()) :
                new ByteArrayResource(s3ImagesService.getObject(found.getId().toString()));
    }
}
