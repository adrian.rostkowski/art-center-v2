package com.adrianrostkowski.artcenter.application.auction.create;

import com.adrianrostkowski.artcenter.application.dto.AuctionDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CreateYchAuctionResult {
    private AuctionDto auctionDto;
}
