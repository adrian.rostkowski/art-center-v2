package com.adrianrostkowski.artcenter.application.auction.get.bidding;

import com.adrianrostkowski.artcenter.application.mapper.AuctionCardMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.function.Function;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class GetBiddingByUserQueryHandler {

    private final AuctionRepository auctions;

    public GetBiddingByUserResponse handle(Integer pageSize, Integer pageNumber) {
        return Stream.of(
                auctions.findAllWhereUserHaveBids(
                        UserContextHelper.getCurrentUser().getId(),
                        PageRequest.of(
                                pageNumber,
                                pageSize
                        )
                ))
                .map(toResponse())
                .findAny()
                .orElseThrow();
    }

    @NotNull
    private Function<Page<Auction>, GetBiddingByUserResponse> toResponse() {
        return n -> new GetBiddingByUserResponse(
                n.getTotalElements(),
                n.stream()
                        .map(AuctionCardMapper::fromEntity)
                        .toList()
        );
    }

}
