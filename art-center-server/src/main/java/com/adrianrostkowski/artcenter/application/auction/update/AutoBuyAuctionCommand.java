package com.adrianrostkowski.artcenter.application.auction.update;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AutoBuyAuctionCommand {
    private Long ychId;
}
