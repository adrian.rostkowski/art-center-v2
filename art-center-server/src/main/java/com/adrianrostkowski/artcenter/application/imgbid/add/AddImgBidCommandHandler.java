package com.adrianrostkowski.artcenter.application.imgbid.add;

import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.image.ImageType;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.CurrentUserIsNotAnOwner;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBidRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;


@Service
@AllArgsConstructor
public class AddImgBidCommandHandler {

    private final AuctionRepository ychs;
    private final AuctionImageRepository images;
    private final ImgBidRepository imgBids;
    private final UserRepository users;
    private final S3ImagesService s3ImagesService;

    @Transactional
    public void handle(
            AddImgBidCommand cmd,
            MultipartFile listOfImages
    ) {
        var user = users.findById(UserContextHelper.getCurrentUser().getId()).orElseThrow();
        var auction = ychs.findById(cmd.getAuctionId()).orElseThrow();
        CurrentUserIsNotAnOwner.ensureIsTrueFor(auction);

        var mainImage = images.save(
                AuctionImage.create(
                        listOfImages,
                        ImageType.ART_TRADE_AUCTION
                )
        );
        mainImage.sendToElectronicArchive(s3ImagesService);

        auction.addImgBid(imgBids.save(
                ImgBid.create(
                        auction,
                        user,
                        mainImage,
                        cmd.getDescription()
                )
        ));

    }
}
