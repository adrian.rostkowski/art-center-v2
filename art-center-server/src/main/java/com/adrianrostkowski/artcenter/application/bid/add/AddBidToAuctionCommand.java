package com.adrianrostkowski.artcenter.application.bid.add;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Builder
public class AddBidToAuctionCommand {
    @NotNull
    private Long auctionId;
    @NotNull
    private BigDecimal bidValue;
}
