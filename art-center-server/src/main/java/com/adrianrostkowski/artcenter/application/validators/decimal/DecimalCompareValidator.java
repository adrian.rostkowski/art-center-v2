package com.adrianrostkowski.artcenter.application.validators.decimal;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.stream.Stream;

public class DecimalCompareValidator implements ConstraintValidator<DecimalCompare, Object> {
    private String field;
    private String fieldMatch;
    private String mode;

    @Override
    public void initialize(DecimalCompare constraintAnnotation) {
        this.field = constraintAnnotation.field();
        this.fieldMatch = constraintAnnotation.fieldMatch();
        this.mode = constraintAnnotation.mode();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        final var target = new BeanWrapperImpl(value);

        final DecimalValidatorMode validatorMode = DecimalValidatorMode.valueOf(mode);
        final BigDecimal fieldValue = (BigDecimal) target.getPropertyValue(field);
        final BigDecimal fieldMatchValue = (BigDecimal) target.getPropertyValue(fieldMatch);

        if (anyValueIsNull(validatorMode, fieldValue, fieldMatchValue)) {
            return false;
        }

        return validatorMode.isValid(fieldValue, fieldMatchValue);
    }

    private boolean anyValueIsNull(DecimalValidatorMode validatorMode, BigDecimal fieldValue, BigDecimal fieldMatchValue) {
        return Stream.of(validatorMode, fieldValue, fieldMatchValue)
                .anyMatch(Objects::isNull);
    }
}
