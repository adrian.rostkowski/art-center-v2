package com.adrianrostkowski.artcenter.application.user.ensure.login;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

@AllArgsConstructor
@Getter
public class IsLoginFreeCommand {
    @Length(min = 6, max = 20)
    private String login;
}
