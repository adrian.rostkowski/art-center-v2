package com.adrianrostkowski.artcenter.application.user.create;

import com.adrianrostkowski.artcenter.application.user.ensure.email.IsEmailFreeCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.email.IsEmailFreeCommandHandler;
import com.adrianrostkowski.artcenter.application.user.ensure.login.IsLoginFreeCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.login.IsLoginFreeCommandHandler;
import com.adrianrostkowski.artcenter.application.user.ensure.username.IsUserNameFreeCommand;
import com.adrianrostkowski.artcenter.application.user.ensure.username.IsUserNameFreeCommandHandler;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.domain.model.user.role.RoleRepository;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Stream;

@Service
@PropertySource("classpath:application.properties")
@RequiredArgsConstructor
public class CreateNewUserCommandHandler {

    private final IsEmailFreeCommandHandler isEmailFreeCommandHandler;
    private final IsLoginFreeCommandHandler isLoginFreeCommandHandler;
    private final IsUserNameFreeCommandHandler userNameFreeCommandHandler;
    private final UserRepository users;
    private final RoleRepository roles;
    private final EmailService emailService;
    private final PasswordEncoder passwordEncoder;

    @Value("${email.url.account.confirm}")
    private String accountConfirm;

    public void handle(CreateNewUserCommand cmd) {
        // ensure passwords are equals
        // ensure login / username / email is unique
        validate(cmd);
        // generate accessKey and add it to user
        var accessKey = generateAccessKey();
        // save user
        var savedUser = users.save(
                User.create(
                        cmd.getLogin(),
                        passwordEncoder.encode(cmd.getPassword()),
                        cmd.getUserName(),
                        cmd.getEmail(),
                        accessKey,
                        roles
                )
        );
        // send email with the code and url to paste it
        emailService.sendConfirmRegistration(
                cmd.getEmail(),
                accessKey,
                accountConfirm + savedUser.getId()
        );
        // send success toast!
    }

    private void validate(CreateNewUserCommand cmd) {
        isPasswordEquals(
                cmd.getPassword(),
                cmd.getRepeatPassword()
        );
        Stream.of(
                        isEmailFreeCommandHandler.handle(new IsEmailFreeCommand(cmd.getEmail())),
                        isLoginFreeCommandHandler.handle(new IsLoginFreeCommand(cmd.getLogin())),
                        userNameFreeCommandHandler.handle(new IsUserNameFreeCommand(cmd.getUserName()))
                )
                .filter(n -> !n)
                .findAny()
                .ifPresent(invalid -> {
                    throw new IllegalStateException(
                            "Passwords are not equals during user create. Current user id [%s]"
                                    .formatted(UserContextHelper.getCurrentUser().getId())
                    );
                });
    }

    private String generateAccessKey() {
        return RandomStringUtils.randomAlphanumeric(20);
    }

    private void isPasswordEquals(
            String passwordOne,
            String passwordTwo
    ) {
        if (!Objects.equals(
                passwordOne,
                passwordTwo
        )) {
            throw new IllegalStateException(
                    "Passwords are not equals during user create. Current user id [%s]".formatted(UserContextHelper.getCurrentUser().getId())
            );
        }
    }
}
