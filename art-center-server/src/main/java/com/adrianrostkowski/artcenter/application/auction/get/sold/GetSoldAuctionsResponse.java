package com.adrianrostkowski.artcenter.application.auction.get.sold;

import com.adrianrostkowski.artcenter.application.dto.AuctionCardDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GetSoldAuctionsResponse {
    private Long totalElements;
    private List<AuctionCardDto> cards;
}
