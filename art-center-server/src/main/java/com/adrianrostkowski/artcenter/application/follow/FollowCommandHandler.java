package com.adrianrostkowski.artcenter.application.follow;

import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroup;
import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroupRepository;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class FollowCommandHandler {

    private final UserRepository users;
    private final FollowGroupRepository followGroups;

    @Transactional
    public void handle(FollowCommand cmd) {
        var artist = users.findByUserName(cmd.getArtistToFollow()).orElseThrow();
        var group = getOrCreateGroupByArtist(artist);
        var savedGroup = followGroups.save(group);
        savedGroup.addCurrentUser();
    }

    private FollowGroup getOrCreateGroupByArtist(User artist) {
        Optional<FollowGroup> artistGroup = followGroups.getFollowGroupByArtistId(artist.getId());
        return artistGroup.orElseGet(() -> FollowGroup.createOf(artist));
    }
}
