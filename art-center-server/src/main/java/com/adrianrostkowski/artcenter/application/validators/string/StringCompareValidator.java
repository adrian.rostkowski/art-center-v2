package com.adrianrostkowski.artcenter.application.validators.string;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static org.apache.commons.lang3.ObjectUtils.anyNull;

public class StringCompareValidator implements ConstraintValidator<StringCompare, Object> {
    private String field;
    private String fieldMatch;
    private String mode;

    @Override
    public void initialize(StringCompare constraintAnnotation) {
        this.field = constraintAnnotation.field();
        this.fieldMatch = constraintAnnotation.fieldMatch();
        this.mode = constraintAnnotation.mode();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        final var target = new BeanWrapperImpl(value);

        final StringValidatorMode validatorMode = StringValidatorMode.valueOf(mode);
        final String fieldValue = (String) target.getPropertyValue(field);
        final String fieldMatchValue = (String) target.getPropertyValue(fieldMatch);

        if (anyNull(validatorMode, fieldValue, fieldMatchValue)) {
            return false;
        }

        return validatorMode.isValid(fieldValue, fieldMatchValue);
    }
}
