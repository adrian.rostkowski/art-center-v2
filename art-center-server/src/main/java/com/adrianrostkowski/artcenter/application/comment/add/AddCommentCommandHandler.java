package com.adrianrostkowski.artcenter.application.comment.add;


import com.adrianrostkowski.artcenter.application.comment.add.addstrategy.AddCommentStrategy;
import com.adrianrostkowski.artcenter.application.comment.add.addstrategy.AddCommentToAuctionBiddingAuction;
import com.adrianrostkowski.artcenter.domain.model.comment.CommentType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.EnumMap;

@Service
@AllArgsConstructor
public class AddCommentCommandHandler {

    private final AddCommentToAuctionBiddingAuction addCommentToAuctionBiddingAuction;

    @Transactional
    public void handle(AddCommentCommand cmd) {
        getStrategies()
                .get(cmd.getCommentType())
                .execute(cmd.getEntityId(), cmd.getTextValue());
    }

    private EnumMap<CommentType, AddCommentStrategy> getStrategies() {
        EnumMap<CommentType, AddCommentStrategy> strategies = new EnumMap<>(CommentType.class);
        strategies.put(CommentType.BIDDING, addCommentToAuctionBiddingAuction);
        return strategies;
    }
}
