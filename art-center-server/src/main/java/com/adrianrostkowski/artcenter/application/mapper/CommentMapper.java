package com.adrianrostkowski.artcenter.application.mapper;

import com.adrianrostkowski.artcenter.application.dto.CommentDto;
import com.adrianrostkowski.artcenter.domain.model.comment.Comment;

public class CommentMapper {
    public static CommentDto fromEntity(Comment entity) {
        return CommentDto.builder()
                .commentId(entity.getId())
                .ownerName(entity.getUser().getUsername())
                .text(entity.getText())
                .build();
    }
}
