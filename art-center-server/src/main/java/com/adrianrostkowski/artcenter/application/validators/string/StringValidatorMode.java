package com.adrianrostkowski.artcenter.application.validators.string;

import lombok.AllArgsConstructor;

import java.util.function.BiPredicate;

@AllArgsConstructor
public enum StringValidatorMode {
    MUST_BE_EQUALS(
            String::equals
    );

    private final BiPredicate<String, String> valid;
    public boolean isValid(String field, String fieldMatch) {
        return valid.test(field, fieldMatch);
    }
}
