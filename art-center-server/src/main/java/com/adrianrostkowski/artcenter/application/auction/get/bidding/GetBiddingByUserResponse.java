package com.adrianrostkowski.artcenter.application.auction.get.bidding;

import com.adrianrostkowski.artcenter.application.dto.AuctionCardDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GetBiddingByUserResponse {
    private Long totalElements;
    private List<AuctionCardDto> cards;
}
