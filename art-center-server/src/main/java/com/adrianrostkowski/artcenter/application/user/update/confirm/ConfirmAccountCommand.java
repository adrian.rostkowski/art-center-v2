package com.adrianrostkowski.artcenter.application.user.update.confirm;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Getter
public class ConfirmAccountCommand {
    @NotNull
    private Long userId;
    @NotBlank
    private String secretKey;
}
