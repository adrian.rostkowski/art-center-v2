package com.adrianrostkowski.artcenter.application.follow;

import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroup;
import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroupRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class GetFollowListQueryHandler {

    private final FollowGroupRepository followGroups;

    public List<FollowGroup> handle() {
        return followGroups.getFollowGroupByUserId(UserContextHelper.getCurrentUser().getId());
    }
}
