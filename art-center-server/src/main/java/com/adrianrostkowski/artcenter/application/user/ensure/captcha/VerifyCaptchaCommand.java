package com.adrianrostkowski.artcenter.application.user.ensure.captcha;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class VerifyCaptchaCommand {
    private String response;
}
