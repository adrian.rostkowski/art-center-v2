package com.adrianrostkowski.artcenter.application.auction.get.mainpage;

import com.adrianrostkowski.artcenter.application.criteriaapi.AuctionStatusToSearch;
import com.adrianrostkowski.artcenter.application.criteriaapi.CriteriaAuctionService;
import com.adrianrostkowski.artcenter.application.mapper.AuctionCardMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Objects;

import static java.util.Objects.nonNull;
import static org.hibernate.internal.util.StringHelper.isBlank;

@Service
@Transactional
@AllArgsConstructor
public class GetYchMainPageCommandHandler {

    private final AuctionRepository auctionRepository;
    private final CriteriaAuctionService criteriaService;
    private final UserRepository users;

    public GetAuctionsListResponse handle(GetAuctionsListCommand query) {
        var user = loadUser(query);
        var result = auctionRepository.findAll(
                buildQuery(query, user),
                buildPageRequest(query)
                );

        return GetAuctionsListResponse.builder()
                .cards(result.stream()
                        .map(AuctionCardMapper::fromEntity)
                        .toList())
                .totalRecords(result.getTotalElements())
                .build();
    }

    private User loadUser(GetAuctionsListCommand query) {
        if (findBySpecificUser(query)) {
            return users.findByUserName(query.getUserName()).orElseThrow();
        } else if (query.getFindByCurrentUser()) {
            return users.findByLogin(UserContextHelper.getCurrentUser().getLogin());
        } else {
            return null;
        }
    }

    private boolean findBySpecificUser(GetAuctionsListCommand query) {
        return query.getFindByCurrentUser() &&
                !isBlank(query.getUserName());
    }

    private Specification<Auction> buildQuery(GetAuctionsListCommand query, User owner) {
        return (root, q, cb) -> {
            var predicates = new ArrayList<Predicate>();

            final var winnerField = "winner";
            if (Objects.equals(query.getFindByCurrentUser(), Boolean.TRUE) || !isBlank(query.getUserName())) {
                predicates.add(
                        cb.or(
                                cb.equal(root.get("owner"), owner.getId()),
                                cb.equal(root.get(winnerField), owner.getId())
                        )
                );
            }

            if (nonNull(query.getAuctionKind())) {
                predicates.add(
                        cb.like(root.get("kind").as(String.class), query.getAuctionKind().getName())
                );
            }

            if (nonNull(query.getYchStatus())) {
                if (query.getYchStatus() == AuctionStatusToSearch.SOLD) {
                    predicates.add(
                            cb.and(
                                    cb.equal(root.get("owner"), owner.getId()),
                                    cb.isNotNull(root.get(winnerField))
                            )
                    );
                } else if (query.getYchStatus() == AuctionStatusToSearch.WON) {
                    predicates.add(
                            cb.equal(root.get(winnerField), owner.getId())
                    );
                }
            }

            if (nonNull(query.getAuctionStep())) {
                predicates.add(
                        cb.like(root.get("auctionStep").as(String.class), query.getAuctionStep().getStepName())
                );
            }

            if (nonNull(query.getYchType()) && query.getYchType() != YchAuctionType.ALL) {
                predicates.add(
                        cb.like(root.get("ychAuctionType").as(String.class), query.getYchType().name())
                );
            }

            if (nonNull(query.getYchCharacterType()) && query.getYchCharacterType() != YchAuctionCharacterType.ALL) {
                predicates.add(
                        cb.like(root.get("ychAuctionCharacterType").as(String.class),
                                query.getYchCharacterType().name()
                        )
                );
            }

            if (nonNull(query.getDaysToEnd())) {
                predicates.add(
                        cb.le(criteriaService.getDaysToEnd(cb, root), query.getDaysToEnd())
                );
            }

            if (nonNull(query.getPriceRangeFrom()) && nonNull(query.getPriceRangeTo())) {
                var joinBids = root.join("bids", JoinType.LEFT);

                predicates.add(
                        criteriaService.getByPriceRange(query, root, cb, joinBids, q)
                );
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    private PageRequest buildPageRequest(GetAuctionsListCommand query) {
        return PageRequest.of(
                query.getPageNumber(),
                query.getPageSize(),
                Sort.by(query.getOrderDirection(), query.getOrderBy())
        );
    }
}
