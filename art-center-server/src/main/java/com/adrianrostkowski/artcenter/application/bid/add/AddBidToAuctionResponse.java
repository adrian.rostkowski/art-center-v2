package com.adrianrostkowski.artcenter.application.bid.add;

import com.adrianrostkowski.artcenter.application.dto.BidDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class AddBidToAuctionResponse {
    private boolean added;
    private List<BidDto> bids;
}
