package com.adrianrostkowski.artcenter.application.user.ensure.username;

import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class IsUserNameFreeCommandHandler {

    private final UserRepository users;

    public Boolean handle(IsUserNameFreeCommand cmd) {
        var loginCount = users.countAllByUserName(cmd.getUserName());
        return loginCount == 0L;
    }
}
