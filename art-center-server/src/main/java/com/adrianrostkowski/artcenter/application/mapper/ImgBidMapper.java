package com.adrianrostkowski.artcenter.application.mapper;

import com.adrianrostkowski.artcenter.application.dto.ImgBidDto;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;

public class ImgBidMapper {
    public static ImgBidDto fromEntity(ImgBid entity) {
        return ImgBidDto.builder()
                .description(entity.getDescription())
                .imgId(entity.getImage().getId())
                .ownerName(entity.getOwner().getUsername())
                .build();
    }
}
