package com.adrianrostkowski.artcenter.application.bid.add;

import com.adrianrostkowski.artcenter.application.dto.BidDto;
import com.adrianrostkowski.artcenter.application.mapper.BidMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.add.AddBidResult;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.bid.BidRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.adrianrostkowski.artcenter.application.dto.BidDto.createInitialBid;


@Service
public class AddBidToAuctionCommandHandler {

    private final AuctionRepository auctionRepository;
    private final BidRepository bids;
    private final UserRepository users;
    private final EmailService emailService;

    @Value("${email.url.auction.discussion}")
    private String discussionUrl;

    public AddBidToAuctionCommandHandler(
            AuctionRepository auctionRepository,
            BidRepository bids,
            UserRepository users,
            EmailService emailService
    ) {
        this.auctionRepository = auctionRepository;
        this.bids = bids;
        this.users = users;
        this.emailService = emailService;
    }

    @Transactional
    public AddBidToAuctionResponse handle(AddBidToAuctionCommand cmd) {
        var auction = auctionRepository.findById(cmd.getAuctionId()).orElseThrow();
        var user = users.findByLogin(UserContextHelper.getCurrentUser().getLogin());

        final var addBidResult = auction.addCurrencyBid(bids.save(
                Bid.create(
                        cmd.getBidValue(),
                        user,
                        auction
                )
        ));

        if (addBidResult == AddBidResult.ADDED_CURRENCY_BID_AND_AUCTION_SHOULD_BE_CLOSED) {
            auction.endAuctionWithAWinner(emailService, discussionUrl);
        }

        return AddBidToAuctionResponse.builder()
                .added(true)
                .bids(prepareBids(auction))
                .build();
    }

    @NotNull
    private List<BidDto> prepareBids(Auction ych) {
        final var sortedBids = ych.getSortedBids().stream().map(BidMapper::fromEntity).collect(Collectors.toList());
        if (ych.getStartingBid() != null) {
            sortedBids.add(createInitialBid(ych.getStartingBid(), ych.getCurrency()));
        }
        return sortedBids;
    }

}
