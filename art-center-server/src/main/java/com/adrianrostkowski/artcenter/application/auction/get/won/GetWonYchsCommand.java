package com.adrianrostkowski.artcenter.application.auction.get.won;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GetWonYchsCommand {
    private Integer pageSize;
    private Integer pageNumber;

    public GetWonYchsCommand(Integer pageSize, Integer pageNumber) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }
}
