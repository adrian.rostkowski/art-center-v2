package com.adrianrostkowski.artcenter.application.auction.update;

import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CloseYchCommandHandler {

    private final AuctionRepository ychs;

    public void handle(Long ychId) {
        var ych = ychs.findById(ychId).orElseThrow();
        ych.closeAuction();
    }
}
