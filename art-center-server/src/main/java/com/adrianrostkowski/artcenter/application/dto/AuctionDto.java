package com.adrianrostkowski.artcenter.application.dto;

import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class AuctionDto {

    @NotNull
    private Long ychAuctionId;
    @NotNull
    private AuctionKind auctionKind;
    @NotNull
    private Long imageId;
    private List<Long> supportImagesId;
    @NotNull
    private String artistName;

    @NotNull
    private String description;

    @NotNull
    private BigDecimal minBid;
    @NotNull
    private BigDecimal autoBuyValue;
    @NotNull
    private Boolean isAutoBuy;
    @NotNull
    private AuctionStep step;
    @NotNull
    private String currency;

    @NotNull
    private LocalDateTime beginning;
    @NotNull
    private LocalDateTime endDate;

    @NotNull
    private Boolean isUserInArtistFollowList;

    private Boolean isOwnerChooseTime;

    private Long secondsToEnd;
    private Boolean auctionIsOver;
    private List<BidDto> bids;
    private List<ImgBidDto> imgBids;
    private List<CommentDto> comments;

    private List<String> availableActions;
}
