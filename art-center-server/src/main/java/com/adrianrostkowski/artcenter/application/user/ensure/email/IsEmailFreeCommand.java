package com.adrianrostkowski.artcenter.application.user.ensure.email;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;

@AllArgsConstructor
@Getter
@Builder
public class IsEmailFreeCommand {
    @Email
    @Length(max = 100)
    private String email;
}
