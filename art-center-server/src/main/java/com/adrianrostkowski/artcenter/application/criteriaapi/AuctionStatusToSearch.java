package com.adrianrostkowski.artcenter.application.criteriaapi;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AuctionStatusToSearch {
    WON("WON"),
    SOLD("SOLD"),
    ALL("ALL");
    private final String name;
}
