package com.adrianrostkowski.artcenter.application.follow;

import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper.getCurrentUser;

@Service
@AllArgsConstructor
public class IsAlreadyInFollowGroupQueryHandler {

    private final FollowGroupRepository followGroups;

    public IsAlreadyInFollowGroupResponse handle(IsAlreadyInFollowGroupQuery cmd) {
        final var userNamesByFollowGroup = followGroups.getFollowersNamesUsingArtistName(cmd.getArtistName());

        return new IsAlreadyInFollowGroupResponse(
                userNamesByFollowGroup.stream()
                        .anyMatch(name -> Objects.equals(name, getCurrentUser().getUsername()))
        );
    }
}
