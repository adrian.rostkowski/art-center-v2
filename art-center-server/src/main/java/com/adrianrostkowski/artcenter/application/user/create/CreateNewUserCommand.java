package com.adrianrostkowski.artcenter.application.user.create;

import com.adrianrostkowski.artcenter.application.validators.string.StringCompare;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@Getter
@StringCompare.List({
        @StringCompare(
                message = "PASSWORDS_ARE_NOT_EQUALS",
                field = "password",
                fieldMatch = "repeatPassword",
                mode = "MUST_BE_EQUALS"
        )
})
public class CreateNewUserCommand {
    @Length(max = 20, min = 6)
    @NotBlank
    private String login;
    @Length(max = 20, min = 6)
    @NotBlank
    private String userName;
    @Length(max = 20, min = 6)
    @NotBlank
    private String password;
    @Length(max = 20, min = 6)
    @NotBlank
    private String repeatPassword;
    @Email(message = "NEW_PASSWORD_CAN_NOT_BE_EQUALS_TO_CURRENT_PASSWORD")
    @Length(max = 100)
    private String email;
}
