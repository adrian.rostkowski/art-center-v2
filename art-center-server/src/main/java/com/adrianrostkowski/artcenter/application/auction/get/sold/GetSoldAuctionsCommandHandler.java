package com.adrianrostkowski.artcenter.application.auction.get.sold;

import com.adrianrostkowski.artcenter.application.mapper.AuctionCardMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GetSoldAuctionsCommandHandler {

    private final AuctionRepository ychs;
    private final UserRepository users;

    public GetSoldAuctionsResponse handle(GetSoldCommand cmd) {
        return users.findByLogin(UserContextHelper.getCurrentUser().getLogin())
                .map(user -> ychs.findAllSoldByOwner(user.getId(), PageRequest.of(cmd.getPageNumber(), cmd.getPageSize())))
                .map(auctions ->
                    GetSoldAuctionsResponse.builder()
                            .cards(auctions.stream()
                                           .map(AuctionCardMapper::fromEntity)
                                           .toList())
                            .totalElements(auctions.getTotalElements())
                            .build()
                )
                .orElseThrow();
    }
}
