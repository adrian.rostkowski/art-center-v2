package com.adrianrostkowski.artcenter.application.imgbid.add;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AddImgBidCommand {
    private String description;
    private Long auctionId;
}
