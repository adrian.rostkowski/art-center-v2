package com.adrianrostkowski.artcenter.application.mapper;

import com.adrianrostkowski.artcenter.application.dto.BidDto;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;

public class BidMapper {

    public static BidDto fromEntity(Bid entity) {
        return BidDto.builder()
                .ownerName(entity.getOwner().getUsername())
                .bidValue(entity.getValue())
                .avatarId(entity.getOwner().getAvatarId())
                .currency(entity.getAuction().getCurrency().getViewName())
                .build();
    }
}
