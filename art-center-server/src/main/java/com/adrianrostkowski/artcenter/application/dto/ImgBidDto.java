package com.adrianrostkowski.artcenter.application.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ImgBidDto {
    private Long imgId;
    private String ownerName;
    private String description;
}
