package com.adrianrostkowski.artcenter.application.auction.get.won;

import com.adrianrostkowski.artcenter.application.mapper.AuctionCardMapper;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GetWonYchsCommandHandler {

    private final AuctionRepository ychs;
    private final UserRepository users;

    public GetWonYchsResponse handle(GetWonYchsCommand cmd) {
        var user = users.findByLogin(
                UserContextHelper.getCurrentUser().getLogin());

        var result = ychs.findAllByWinner(user.getId(),
                                          PageRequest.of(
                                                  cmd.getPageNumber(),
                                                  cmd.getPageSize()
                                          )
        );

        var cards = result.stream()
                .map(AuctionCardMapper::fromEntity)
                .collect(Collectors.toList());

        var countOfPages = result.getTotalElements();

        return GetWonYchsResponse.builder()
                .cards(cards)
                .countOfPages(countOfPages)
                .build();
    }
}
