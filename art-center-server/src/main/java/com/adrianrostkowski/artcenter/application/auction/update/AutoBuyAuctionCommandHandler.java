package com.adrianrostkowski.artcenter.application.auction.update;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.bid.BidRepository;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AutoBuyAuctionCommandHandler {

    @Value("${email.url.auction.discussion}")
    private String discussionUrl;

    private final AuctionRepository ychs;
    private final UserRepository users;
    private final BidRepository bids;
    private final EmailService emailService;


    public AutoBuyAuctionCommandHandler(
            AuctionRepository ychs,
            UserRepository users,
            BidRepository bids,
            EmailService emailService
    ) {
        this.ychs = ychs;
        this.users = users;
        this.bids = bids;
        this.emailService = emailService;
    }

    public void handle(AutoBuyAuctionCommand cmd) {
        var ych = ychs.findById(cmd.getYchId()).orElseThrow();
        var winner = users.findByLogin(UserContextHelper.getCurrentUser().getLogin());
        var bid = createMaxBid(ych, winner);

        ych.autoBuy(winner, bid);

        emailService.sendAuctionEndNotification(winner.getEmail(), discussionUrl + ych.getId());
        emailService.sendAuctionEndNotification(ych.getOwner().getEmail(), discussionUrl + ych.getId());
    }

    private Bid createMaxBid(Auction ych, User user) {
        return bids.save(
                Bid.builder()
                        .value(ych.getAutoBuyBidValue())
                        .owner(user)
                        .auction(ych)
                        .build()
        );
    }
}
