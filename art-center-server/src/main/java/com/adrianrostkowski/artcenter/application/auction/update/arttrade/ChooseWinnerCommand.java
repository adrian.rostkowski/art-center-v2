package com.adrianrostkowski.artcenter.application.auction.update.arttrade;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ChooseWinnerCommand {
    private final Long winningBidId;
    private final Long auctionId;
}
