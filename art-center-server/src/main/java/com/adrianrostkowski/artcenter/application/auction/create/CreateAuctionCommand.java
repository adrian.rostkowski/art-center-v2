package com.adrianrostkowski.artcenter.application.auction.create;

import com.adrianrostkowski.artcenter.application.validators.decimal.DecimalCompare;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@Getter
@DecimalCompare.List({
        @DecimalCompare(
                message = "MAX_BID_CAN_NOT_BE_LOWER_THEN_MIN_BID",
                field = "maxBid",
                fieldMatch = "minBid",
                mode = "MUST_BE_HIGHER"
        ),
        @DecimalCompare(
                message = "MAX_BID_CAN_NOT_BE_LOWER_THEN_MIN_BID",
                field = "maxBid",
                fieldMatch = "minBid",
                mode = "MUST_BE_HIGHER"
        )
})
public class CreateAuctionCommand {
//    @NotBlank
    private String auctionKind;
    private String currency;
//    @NotNull
    private String description;

//    @DecimalMin(value = "0.0")
    private BigDecimal minBid;
    private BigDecimal maxBid;
    private Boolean isMaxBid;

//    @NotNull
//    @Max(10)
    private Long countOfDays;

//    @NotBlank
    private String ychAuctionType;
//    @NotBlank
    private String ychAuctionCharacterType;
    private MultipartFile mainArt;
}
