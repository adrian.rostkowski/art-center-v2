package com.adrianrostkowski.artcenter.application.comment.add;

import com.adrianrostkowski.artcenter.domain.model.comment.CommentType;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Getter
public class AddCommentCommand {
    @NotNull
    private CommentType commentType;
    @NotNull
    private Long entityId;
    @NotBlank
    private String textValue;
}
