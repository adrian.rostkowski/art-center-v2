package com.adrianrostkowski.artcenter.application.follow;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class IsAlreadyInFollowGroupResponse {
    private Boolean isAlready;
}
