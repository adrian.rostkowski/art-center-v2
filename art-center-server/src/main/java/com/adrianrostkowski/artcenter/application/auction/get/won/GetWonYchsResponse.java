package com.adrianrostkowski.artcenter.application.auction.get.won;

import com.adrianrostkowski.artcenter.application.dto.AuctionCardDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GetWonYchsResponse {
    private Long countOfPages;
    private List<AuctionCardDto> cards;
}
