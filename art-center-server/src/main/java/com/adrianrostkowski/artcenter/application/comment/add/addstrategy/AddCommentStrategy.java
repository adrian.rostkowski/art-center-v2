package com.adrianrostkowski.artcenter.application.comment.add.addstrategy;

public interface AddCommentStrategy {
    void execute(Long id, String text);
}
