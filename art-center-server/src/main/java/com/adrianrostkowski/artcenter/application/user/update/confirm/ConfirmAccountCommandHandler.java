package com.adrianrostkowski.artcenter.application.user.update.confirm;

import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import com.adrianrostkowski.artcenter.domain.model.user.specification.SecretKeyIsCorrect;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ConfirmAccountCommandHandler {

    private final UserRepository users;

    public void handle(ConfirmAccountCommand cmd) {
        var userToActivate = users.findById(cmd.getUserId()).orElseThrow();
        SecretKeyIsCorrect.ensureIsTrueFor(
                userToActivate,
                cmd.getSecretKey()
        );

        userToActivate.setEnabled(true);
    }

}
