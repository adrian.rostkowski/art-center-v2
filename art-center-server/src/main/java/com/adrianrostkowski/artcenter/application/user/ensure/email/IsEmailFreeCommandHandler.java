package com.adrianrostkowski.artcenter.application.user.ensure.email;

import com.adrianrostkowski.artcenter.domain.model.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class IsEmailFreeCommandHandler {

    private final UserRepository users;

    public Boolean handle(IsEmailFreeCommand cmd) {
        var emailCount = users.countAllByEmail(cmd.getEmail());
        return emailCount == 0L;
    }
}
