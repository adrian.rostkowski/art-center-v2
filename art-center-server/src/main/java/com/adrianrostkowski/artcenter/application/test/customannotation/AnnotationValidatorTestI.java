package com.adrianrostkowski.artcenter.application.test.customannotation;

public interface AnnotationValidatorTestI {

    boolean isValid(String name);
}
