package com.adrianrostkowski.artcenter.application.auction.get.mainpage;

import com.adrianrostkowski.artcenter.application.criteriaapi.AuctionStatusToSearch;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Builder
@Getter
public class GetAuctionsListCommand {
    private AuctionStep auctionStep;
    private Integer daysToEnd;
    private Integer priceRangeFrom;
    private Integer priceRangeTo;
    private YchAuctionType ychType;
    private YchAuctionCharacterType ychCharacterType;
    @NotNull
    private Boolean findByCurrentUser;
    private AuctionStatusToSearch ychStatus;
    private String userName;
    private AuctionKind auctionKind;

    private int pageNumber;
    @Max(50)
    private int pageSize;
    @Builder.Default
    private final String[] orderBy = new String[]{"endDate"};
    @Builder.Default
    private final Sort.Direction orderDirection = Sort.Direction.ASC;
}
