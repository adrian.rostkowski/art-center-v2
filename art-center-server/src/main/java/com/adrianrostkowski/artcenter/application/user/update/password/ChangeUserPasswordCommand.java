package com.adrianrostkowski.artcenter.application.user.update.password;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@Builder
public class ChangeUserPasswordCommand {
    @NotBlank
    private String newPassword;
    @NotBlank
    private String repeatNewPassword;
    @NotBlank
    private String currentPassword;
}
