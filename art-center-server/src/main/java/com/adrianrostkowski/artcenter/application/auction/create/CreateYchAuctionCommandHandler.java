package com.adrianrostkowski.artcenter.application.auction.create;

import com.adrianrostkowski.artcenter.application.auction.factory.CreateAuction;
import com.adrianrostkowski.artcenter.application.dto.AuctionDto;
import com.adrianrostkowski.artcenter.application.mapper.AuctionMapper;
import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroupRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class CreateYchAuctionCommandHandler {

    private final AuctionRepository auctions;
    private final AuctionImageRepository images;
    private final S3ImagesService s3ImagesService;
    private final EmailService emailService;
    private final FollowGroupRepository followGroups;

    @Value("${email.url.created.auction}")
    private String createdAuctionUrl;

    @Transactional
    public AuctionDto handle(CreateAuctionCommand cmd) {
        return Stream.of(
                        CreateAuction.chooseStrategy(
                                AuctionKind.valueOf(cmd.getAuctionKind()))
                )
                .map(chosenStrategy -> auctions.save(chosenStrategy.createAuction(
                        cmd,
                        UserContextHelper.getCurrentUser(),
                        List.of(cmd.getMainArt()),
                        images,
                        s3ImagesService
                )))
                .peek(createdAuction ->
                              sendNotificationsToFollowers(
                                      createdAuction.getOwner().getUsername(),
                                      createdAuction.getOwner().getId(),
                                      createdAuction.getId()
                              ))
                .map(n -> AuctionMapper.fromEntity(n, Boolean.FALSE))
                .findAny()
                .orElseThrow();
    }

    private void sendNotificationsToFollowers(String artistName, Long artistId, Long auctionId) {
        List<String> followersEmails = followGroups.getFollowersEmailsUsingArtistName(artistId);
        emailService.sendAuctionCreatedToFollowers(
                followersEmails,
                artistName,
                createdAuctionUrl + auctionId
        );
    }
}
