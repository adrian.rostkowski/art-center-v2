package com.adrianrostkowski.artcenter.application.dto;

import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
public class AuctionDiscussionDto {
    private List<CommentDto> comments;
    private Long winnerId;
    private String winnerName;
    private Long ownerId;
    private String ownerName;
    private String description;
    private Long ychId;
    private BigDecimal price;
    private Long imageId;
    private Boolean userIsOwner;
    private AuctionStep step;
    private AuctionKind auctionKind;
}
