package com.adrianrostkowski.artcenter.integractions.aws.email;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class EmailController {

    private final AmazonSimpleEmailServiceClient amazonSimpleEmailServiceClient;
    private final EmailService emailService;

    // TODO is this ONLY for tests?
    @GetMapping("/sendemail")
    public void sendEmail1() {
        emailService.sendConfirmRegistration("aaadik0013@gmail.com", "33446600909", "fake url");
    }
    // TODO is this ONLY for tests?
    @GetMapping("/auctionend")
    public void auctionEnd() {
        emailService.sendAuctionEndNotification("aaadik0013@gmail.com", "http://localhost:4200/ych-details/19");
    }
}
