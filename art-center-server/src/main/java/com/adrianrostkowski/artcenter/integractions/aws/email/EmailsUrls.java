package com.adrianrostkowski.artcenter.integractions.aws.email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailsUrls {
    @Value("${email.url.auction.discussion}")
    private String DISCUSSION;

    public String getDiscussionUrl() {
        return DISCUSSION;
    }
}
