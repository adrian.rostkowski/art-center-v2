package com.adrianrostkowski.artcenter.integractions.aws.email;

import lombok.Getter;

@Getter
public enum EmailBody {
    CONFIRM_ACCOUNT_REGISTRATION_BODY(
            "<h1>Welcome to Art-Center</h1>" +
                    "<p>Paste this code : %s to confirm your account</p>" +
                    "<p><a href='%s'>here confirm your account</a></p>"
    ),
    AUCTION_END_NOTIFICATION_BODY(
            "<h1>Your auction has ended</h1>" +
                    "<h3>Click to visit your auction -> <a href='%s'>your auction</a></h3>"
    ),
    AUCTION_CREATED_NOTIFICATION_BODY(
            """
            <h1>%s has created a new auction!</h1>
            <h3>Click to visit the auction -> <a href='%s'>click me :)</a></h3>
            """
    );

    private final String body;

    EmailBody(String body) {
        this.body = body;
    }
}
