package com.adrianrostkowski.artcenter.integractions.aws.images;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
@Slf4j
@PropertySource("classpath:application.properties")
public class S3ImagesService {
    @Value("${amazon.s3.default-bucket}")
    private String bucket;

    private final AmazonS3 s3client;

    public S3ImagesService(AmazonS3 s3client) {
        this.s3client = s3client;
    }

    public void uploadFileToBucket(String keyOfFile, MultipartFile multiFile) {
        var file = convertMultipartFileToFile(multiFile);
        s3client.putObject(
                bucket,
                keyOfFile,
                file
        );
        file.delete();
    }

    public void uploadFileToBucket(String keyOfFile, byte[] content) {
        var file = convertByteArrayToFile(content);
        s3client.putObject(
                bucket,
                keyOfFile,
                file
        );
        file.delete();
    }

    public byte[] getObject(String keyOfFile) {
        S3Object s3object = s3client.getObject(bucket, keyOfFile);
        S3ObjectInputStream inputStream = s3object.getObjectContent();
        try {
            return inputStream.readAllBytes();
        } catch (IOException e) {
            log.error(String.format("Can not get item from s3 bucket, item key : [%s]", keyOfFile));
        }
        return null;
    }

    private File convertMultipartFileToFile(MultipartFile file) {
        File convertedFile = new File(bucket);
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error during convertMultipartFileToFile");
        }
        return convertedFile;
    }

    private File convertByteArrayToFile(byte[] file) {
        File convertedFile = new File(bucket);
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file);
        } catch (IOException e) {
            log.error("Error during convertMultipartFileToFile");
        }
        return convertedFile;
    }
}
