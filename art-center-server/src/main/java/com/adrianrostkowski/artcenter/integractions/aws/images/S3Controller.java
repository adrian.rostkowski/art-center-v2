package com.adrianrostkowski.artcenter.integractions.aws.images;

import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImageRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/s3/img")
@AllArgsConstructor
public class S3Controller {

    private final S3ImagesService s3ImagesService;
    private final AuctionImageRepository images;

    @GetMapping("/save-img")
    public void saveImg() {
        var img = images.findById(3L).orElseThrow();
        BASE64DecodedMultipartFile multipartFile = new BASE64DecodedMultipartFile(img.getData());
        s3ImagesService.uploadFileToBucket(img.getId().toString(), multipartFile);
    }

    @GetMapping(value = "/get-img", produces = "image/png")
    public Resource getById() {
        var result = s3ImagesService.getObject("3");
        return new ByteArrayResource(result);
    }

}
