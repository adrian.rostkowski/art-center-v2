package com.adrianrostkowski.artcenter.integractions.aws.email;

import lombok.Getter;

import static com.adrianrostkowski.artcenter.integractions.aws.email.EmailBody.*;

@Getter
public enum EmailTemplate {
    CONFIRM_ACCOUNT_REGISTRATION(CONFIRM_ACCOUNT_REGISTRATION_BODY, "Art-Center register account") {
        @Override
        public String getBody(String... variables) {
            return String.format(getBodyEnum().getBody(), (Object[]) variables);
        }

        @Override
        public String getSubject(String... variables) {
            return String.format(getSubjectEnum(), (Object[]) variables);
        }
    },
    AUCTION_END_NOTIFICATION(AUCTION_END_NOTIFICATION_BODY, "Your auction has ended") {
        @Override
        public String getBody(String... variables) {
            return String.format(getBodyEnum().getBody(), (Object[]) variables);
        }

        @Override
        public String getSubject(String... variables) {
            return String.format(getSubjectEnum(), (Object[]) variables);
        }
    },
    AUCTION_CREATED_NOTIFICATION(AUCTION_CREATED_NOTIFICATION_BODY, "%s has created new auction") {
        @Override
        public String getBody(String... variables) {
            return String.format(getBodyEnum().getBody(), (Object[]) variables);
        }

        @Override
        public String getSubject(String... variables) {
            return String.format(getSubjectEnum(), (Object[]) variables);
        }
    };

    private final EmailBody body;
    private final String subject;

    public abstract String getBody(String... variables);
    public abstract String getSubject(String... variables);

    protected EmailBody getBodyEnum(String... variables) {
        return this.body;
    }

    protected String getSubjectEnum(String... variables) {
        return this.subject;
    }

    EmailTemplate(EmailBody body, String subject) {
        this.body = body;
        this.subject = subject;
    }
}
