package com.adrianrostkowski.artcenter.integractions.aws.email;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.adrianrostkowski.artcenter.integractions.aws.email.EmailTemplate.*;

@Service
@AllArgsConstructor
@Slf4j
public class EmailService {

    private final AmazonSimpleEmailServiceClient amazonSimpleEmailServiceClient;

    public void sendConfirmRegistration(String emailTo, String confirmCode, String confirmURL) {
        this.sendEmail(
                emailTo,
                CONFIRM_ACCOUNT_REGISTRATION.getBody(confirmCode, confirmURL),
                CONFIRM_ACCOUNT_REGISTRATION.getSubject()
        );
    }

    public void sendAuctionEndNotification(String emailTo, String auctionURL) {
        this.sendEmail(
                emailTo,
                AUCTION_END_NOTIFICATION.getBody(auctionURL),
                AUCTION_END_NOTIFICATION.getSubject()
        );
    }

    @Async
    public void sendAuctionCreatedToFollowers(List<String> emails, String artistUserName, String auctionURL) {
        emails.forEach(
               email -> this.sendEmail(
                       email,
                       AUCTION_CREATED_NOTIFICATION.getBody(auctionURL),
                       AUCTION_CREATED_NOTIFICATION.getSubject(artistUserName)
               )
        );
    }

    @Async
    public void testAsync(String text) {
        for (int i = 0; i < 100; i++) {
            System.out.println(text + " " + i);
        }
    }

    private void sendEmail(String emailTo, String htmlBody, String subject) {
        try {
            String emailFrom = "art.center.contact@gmail.com";
            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(new Destination().withToAddresses(emailTo))
                    .withMessage(
                            new Message()
                                    .withBody(new Body()
                                            .withHtml(new Content()
                                                    .withCharset("UTF-8").withData(htmlBody)))
                                    .withSubject(new Content()
                                            .withCharset("UTF-8").withData(subject)))
                    .withSource(emailFrom);

            amazonSimpleEmailServiceClient.sendEmail(request);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
