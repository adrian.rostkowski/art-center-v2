package com.adrianrostkowski.artcenter.domain.model.auction.image;

public enum ImageType {
    YCH_AUCTION,
    ADOPT_AUCTION,
    ART_TRADE_AUCTION
}
