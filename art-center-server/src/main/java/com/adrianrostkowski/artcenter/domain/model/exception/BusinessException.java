package com.adrianrostkowski.artcenter.domain.model.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BusinessException extends RuntimeException {

    public BusinessException(BusinessExceptionCodes code) {
        super(code.name());
    }
}
