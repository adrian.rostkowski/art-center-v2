package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionBidList;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.util.NumberUtils.isBiggerOrEquals;
import static java.util.Objects.isNull;

class AddBidAndCloseAuctionStrategy implements AddBidStrategy {

    private final boolean isMaxBid;
    private final BigDecimal autoBuyBidValue;
    private final Bid newBid;
    private final AuctionBidList auctionBidList;
    private final Auction auction;


    public AddBidAndCloseAuctionStrategy(
            boolean isMaxBid,
            BigDecimal autoBuyBidValue,
            Bid newBid,
            AuctionBidList auctionBidList,
            Auction auction
    ) {
        this.isMaxBid = isMaxBid;
        this.autoBuyBidValue = autoBuyBidValue;
        this.newBid = newBid;
        this.auctionBidList = auctionBidList;
        this.auction = auction;
    }

    @Override
    public void add() {
        auctionBidList.addNewBid(newBid);
    }

    @Override
    public boolean isRequired() {
        return !isNull(newBid) && auction.isCurrencyAuction() && isMaxBid && isBiggerOrEquals(newBid.getValue(), autoBuyBidValue);
    }

    @Override
    public AddBidResult result() {
        return AddBidResult.ADDED_CURRENCY_BID_AND_AUCTION_SHOULD_BE_CLOSED;
    }
}
