package com.adrianrostkowski.artcenter.domain.model.auction.values;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum AuctionStep {
    BIDDING("BIDDING"),
    CONSULTATIONS("CONSULTATIONS"),
    CLOSED("CLOSED");

    private final String stepName;
}
