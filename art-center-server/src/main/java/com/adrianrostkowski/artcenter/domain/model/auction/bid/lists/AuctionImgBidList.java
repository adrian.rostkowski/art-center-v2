package com.adrianrostkowski.artcenter.domain.model.auction.bid.lists;

import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;

import java.util.List;
import java.util.Optional;

public class AuctionImgBidList {
    private final List<ImgBid> bids;

    public AuctionImgBidList(List<ImgBid> bids) {
        this.bids = bids;
    }

    public static AuctionImgBidList of(List<ImgBid> bids) {
        return new AuctionImgBidList(bids);
    }

    public Optional<ImgBid> getWinningBid() {
        return this.bids.stream()
                .filter(ImgBid::getIsWinning)
                .findAny();
    }

    public void add(ImgBid imgBid) {
        this.bids.add(imgBid);
    }
}
