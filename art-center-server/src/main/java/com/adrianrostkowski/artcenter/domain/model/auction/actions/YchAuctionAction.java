package com.adrianrostkowski.artcenter.domain.model.auction.actions;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.user.User;

public enum YchAuctionAction {

    CANCEL_APPLICATION {
        @Override
        public boolean canBeExecuteBy(User user, Auction auction) {
            return auction.getOwner().getLogin().equals(user.getLogin());
        }
    };

    public abstract boolean canBeExecuteBy(User user, Auction auction);

    public void ensureCanBeExecutedBy(User user, Auction auction) {
        if (!canBeExecuteBy(user, auction)) {
            throw new IllegalStateException(String.format("User [%s] is not allowed to call given action", user.getUsername()));
        }
    }

}
