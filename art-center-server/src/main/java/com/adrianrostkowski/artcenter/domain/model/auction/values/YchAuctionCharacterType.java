package com.adrianrostkowski.artcenter.domain.model.auction.values;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum YchAuctionCharacterType {
    PONY("PONY"),
    FURRY("FURRY"),
    HUMAN("HUMAN"),
    ALL("ALL");

    private final String name;
}
