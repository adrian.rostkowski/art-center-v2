package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.YOU_ARE_NOT_OWNER_OR_WINNER_ARE_YOU;

@RequiredArgsConstructor
public class ApplicationIsProcessingByOwnerOrWinner extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (
                !Objects.equals(ychAuction.getOwner().getLogin(), UserContextHelper.getCurrentUser().getLogin())
                && !Objects.equals(ychAuction.getWinner().getLogin(), UserContextHelper.getCurrentUser().getLogin())
        ) {
            throw new BusinessException(YOU_ARE_NOT_OWNER_OR_WINNER_ARE_YOU);
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new ApplicationIsProcessingByOwnerOrWinner(ychAuction).ensureIsTrue();
    }
}
