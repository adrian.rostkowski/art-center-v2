package com.adrianrostkowski.artcenter.domain.model.auction.end.end_with_winner;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;

class CloseCurrencyAuction implements CloseAuctionWithWinnerStrategy {

    private final Auction auction;
    private final EmailService emailService;
    private final String discussionUrl;

    public CloseCurrencyAuction(
            Auction auction,
            EmailService emailService,
            String discussionUrl
    ) {
        this.auction = auction;
        this.emailService = emailService;
        this.discussionUrl = discussionUrl;
    }

    @Override
    public void close() {
        auction.endCurrencyAuctionWithAWinner();

        emailService.sendAuctionEndNotification(auction.getWinner().getEmail(), discussionUrl + auction.getId());
        emailService.sendAuctionEndNotification(auction.getOwner().getEmail(), discussionUrl + auction.getId());
    }

    @Override
    public boolean isRequired() {
        return auction.isCurrencyAuction();
    }
}
