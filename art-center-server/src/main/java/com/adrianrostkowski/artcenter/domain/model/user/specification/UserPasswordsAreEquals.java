package com.adrianrostkowski.artcenter.domain.model.user.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCrypt;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_CONTAIN_MAX_BID;
import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.USER_OLD_PASSWORD_MUST_BE_EQUALS_TO_CURRENT_PASSWORD;

@RequiredArgsConstructor
public class UserPasswordsAreEquals extends CriticalRule {
    private final User user;
    private final String oldPassword;

    @Override
    protected void ensureIsTrue() {
        if (!BCrypt.checkpw(oldPassword, user.getPassword())) {
            throw new BusinessException(USER_OLD_PASSWORD_MUST_BE_EQUALS_TO_CURRENT_PASSWORD);
        }
    }

    public static void ensureIsTrueFor(User user, String oldPassword) {
        new UserPasswordsAreEquals(user, oldPassword).ensureIsTrue();
    }
}
