package com.adrianrostkowski.artcenter.domain.model.auction.bid;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BidCurrency {
    USD("USD", "USD"),
    DEVIANT_ART_POINTS("DEVIANT_ART_POINTS", "DeviantArt points");

    private final String name;
    private final String viewName;
}
