package com.adrianrostkowski.artcenter.domain.model.shared;

public abstract class CriticalRule {

    protected abstract void ensureIsTrue();

}
