package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.function.Function;

@RequiredArgsConstructor
public class WinnerIsInBidList extends CriticalRule {
    private final Auction ychAuction;
    private final User winner;

    @Override
    protected void ensureIsTrue() {
        if (isCurrencyAuction()) {
            if (ychAuction.getBids().stream()
                    .map((Function<Bid, Object>) Bid::getOwner)
                    .noneMatch(n -> ((User) n).getUsername().equals(winner.getUsername()))
            ) {
                throw new IllegalStateException(
                        String.format("Winner should be on bid list, winner userName : [%s], ID : [%s], auction id : [%s]",
                                winner.getUsername(),
                                winner.getId(),
                                ychAuction.getId())
                );
            }
        } else {
            if (ychAuction.getImgBids().stream()
                    .map((Function<ImgBid, Object>) ImgBid::getOwner)
                    .noneMatch(n -> ((User) n).getUsername().equals(winner.getUsername()))
            ) {
                throw new IllegalStateException(
                        String.format("Winner should be on bid list, winner userName : [%s], ID : [%s], auction id : [%s]",
                                winner.getUsername(),
                                winner.getId(),
                                ychAuction.getId())
                );
            }
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction, User winner) {
        new WinnerIsInBidList(ychAuction, winner).ensureIsTrue();
    }

    private boolean isCurrencyAuction() {
        return !Objects.equals(ychAuction.getKind(), AuctionKind.ART_TRADE);
    }
}
