package com.adrianrostkowski.artcenter.domain.model.imgbid;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "img_bid")
public class ImgBid extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "auction_id", referencedColumnName = "id")
    private Auction auction;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private AuctionImage image;

    @Column(columnDefinition = "TEXT", name = "description")
    private String description;

    @Column(name = "is_winning")
    private Boolean isWinning;

    public static ImgBid create(Auction auction, User owner, AuctionImage image, String description) {
        return new ImgBid(owner, auction, image, description, false);
    }

    public void setAsWinning() {
        this.isWinning = true;
    }
}
