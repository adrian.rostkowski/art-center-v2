package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionBidList;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;

import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.util.NumberUtils.isBiggerOrEquals;
import static java.util.Objects.isNull;

class AddIFinancialBidStrategy implements AddBidStrategy {

    private final AuctionBidList auctionBidList;
    private final Bid newBid;
    private final BigDecimal startingBid;
    private final Auction auction;

    public AddIFinancialBidStrategy(
            AuctionBidList auctionBidList,
            Bid newBid,
            BigDecimal startingBid,
            Auction auction
    ) {
        this.auctionBidList = auctionBidList;
        this.newBid = newBid;
        this.startingBid = startingBid;
        this.auction = auction;
    }

    @Override
    public void add() {
        auctionBidList.addNewBid(newBid);
    }

    @Override
    public boolean isRequired() {
        return !isNull(newBid) &&
                auction.isCurrencyAuction() &&
                (auctionBidList.isEmpty() ||
                auctionBidList.isGreaterThenHighestBid(newBid.getValue()) ||
                isBiggerOrEquals(newBid.getValue(), startingBid));
    }

    @Override
    public AddBidResult result() {
        return AddBidResult.ADDED_CURRENCY_BID;
    }
}
