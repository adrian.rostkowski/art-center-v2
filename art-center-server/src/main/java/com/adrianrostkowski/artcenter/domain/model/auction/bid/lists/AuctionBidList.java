package com.adrianrostkowski.artcenter.domain.model.auction.bid.lists;

import com.adrianrostkowski.artcenter.application.dto.BidDto;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.util.*;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AuctionBidList {
    private final List<Bid> bids;

    public static AuctionBidList of(List<Bid> bids) {
        return new AuctionBidList(bids);
    }

    public boolean isGreaterThenHighestBid(BigDecimal bid) {
        // TODO: 08.12.2021 is there a bug?
        //  if list is empty every bid should be bigger
//        return !bids.isEmpty() && bid.compareTo(getHighestBidValue()) > 0;
//        return bids.isEmpty() && isHighestBid(bid);
        return isHighestBid(bid);
    }

    private boolean isHighestBid(BigDecimal bid) {
        return bid.compareTo(getHighestBidValue()) > 0;
    }

    public BigDecimal getHighestBidValue() {
        return bids.stream()
                .map(Bid::getValue)
                .max(BigDecimal::compareTo)
                .orElse(BigDecimal.ZERO);
    }

    public Optional<Bid> getHighestBid() {
        return bids.stream()
                .max(Comparator.comparing(Bid::getValue));
    }

    public boolean isEmpty() {
        return this.bids.isEmpty();
    }

    public void addNewBid(Bid bid) {
        this.bids.add(bid);
    }
}
