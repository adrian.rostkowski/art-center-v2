package com.adrianrostkowski.artcenter.domain.model.imgbid;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgBidRepository extends JpaRepository<ImgBid, Long> {
}
