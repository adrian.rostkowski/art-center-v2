package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

public enum AddBidResult {
    ADDED_IMG_BID,
    ADDED_CURRENCY_BID,
    ADDED_CURRENCY_BID_AND_AUCTION_SHOULD_BE_CLOSED,
}
