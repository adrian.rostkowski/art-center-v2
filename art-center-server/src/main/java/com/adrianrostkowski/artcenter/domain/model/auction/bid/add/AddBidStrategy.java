package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

interface AddBidStrategy {
    void add();
    boolean isRequired();
    AddBidResult result();
}
