package com.adrianrostkowski.artcenter.domain.model.user;


import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.user.role.Role;
import com.adrianrostkowski.artcenter.domain.model.user.role.RoleRepository;
import com.adrianrostkowski.artcenter.domain.model.user.specification.CurrentUserIsEquals;
import com.adrianrostkowski.artcenter.domain.model.user.specification.LoginIsCorrect;
import com.adrianrostkowski.artcenter.domain.model.user.specification.PasswordIsCorrect;
import com.adrianrostkowski.artcenter.domain.model.user.specification.UserNameIsCorrect;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class User extends BaseEntity implements UserDetails {

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "email")
    private String email;

    @Column(name = "secret_key")
    private String secretKey;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "avatar_id")
    private Long avatarId;

    @ManyToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

    public static User create(String login, String password, String userName, String email, String secretKey, RoleRepository roleRepository) {

        LoginIsCorrect.ensureIsTrueFor(login);
        UserNameIsCorrect.ensureIsTrueFor(userName);
        // todo add email validation or add validation annotation and return business ex

        return new User(
                login,
                password,
                userName,
                email,
                secretKey,
                false,
                null,
                List.of(roleRepository.findByName("ROLE_USER"))
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("ROLE_USER"));
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void changePassword(String newPassword) {
        CurrentUserIsEquals.ensureIsTrueFor(this);
        this.password = newPassword;
    }

    public void changeUsername(String newUsername) {
        CurrentUserIsEquals.ensureIsTrueFor(this);
        this.userName = newUsername;
    }

//    public static User createNewUser(String login, String password, String userName, Long facebookId) {
//        return new User(null, login, password, userName, facebookId);
//    }
}
