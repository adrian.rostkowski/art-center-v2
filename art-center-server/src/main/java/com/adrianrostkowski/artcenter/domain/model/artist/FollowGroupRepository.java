package com.adrianrostkowski.artcenter.domain.model.artist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FollowGroupRepository extends JpaRepository<FollowGroup, Long> {
    Optional<FollowGroup> findById(Long id);

    Optional<FollowGroup> getFollowGroupByArtistId(Long artistId);

    // todo why this "id" is never use?
    @Query(value = """
            SELECT * FROM follow_group fg
            left join follow_groups fgs on fgs.group_id = fg.id
            left join user us on us.id = fgs.user_id
            """,
            nativeQuery = true)
    List<FollowGroup> getFollowGroupByUserId(Long id);

    @Query(value = """
            SELECT follower.user_name
            FROM user follower
            	LEFT JOIN user artist on artist.user_name = :userName
            	LEFT JOIN follow_group on follow_group.artist_id = artist.id
            	LEFT JOIN follow_groups on follow_groups.group_id = follow_group.id
            where follower.id = follow_groups.user_id
            """,
            nativeQuery = true)
    List<String> getFollowersNamesUsingArtistName(String userName);

    @Query(value = """
            SELECT follower.email
            FROM user follower
            	LEFT JOIN user artist on artist.id = :artistId
            	LEFT JOIN follow_group on follow_group.artist_id = artist.id
            	LEFT JOIN follow_groups on follow_groups.group_id = follow_group.id
            where follower.id = follow_groups.user_id
            """,
            nativeQuery = true)
    List<String> getFollowersEmailsUsingArtistName(Long artistId);
}
