package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

@RequiredArgsConstructor
public class AuctionKindEqualsTo extends CriticalRule {
    private final Auction ychAuction;
    private final AuctionKind auctionKind;

    @Override
    protected void ensureIsTrue() {
        if (!Objects.equals(ychAuction.getKind(), auctionKind)) {
            throw new IllegalStateException(
                    String.format("Application should be kind : [%s], application id : [%s]",
                            auctionKind.getName(), ychAuction.getId()));
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction, AuctionKind auctionKind) {
        new AuctionKindEqualsTo(ychAuction, auctionKind).ensureIsTrue();
    }
}
