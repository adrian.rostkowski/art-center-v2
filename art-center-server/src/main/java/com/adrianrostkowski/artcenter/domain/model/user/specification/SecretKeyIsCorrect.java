package com.adrianrostkowski.artcenter.domain.model.user.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AllArgsConstructor;

import java.util.Objects;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_CONTAIN_MAX_BID;
import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.SECRET_KEY_IS_INCORRECT;


@AllArgsConstructor
public class SecretKeyIsCorrect extends CriticalRule {
    private final User user;
    private final String secretKey;

    @Override
    protected void ensureIsTrue() {
        if (user.isEnabled()) {
            throw new IllegalStateException(String.format("During account activation user should be disable, user id : [%s]", UserContextHelper.getCurrentUser().getId()));
        }

        if (!Objects.equals(user.getSecretKey(), secretKey)) {
            throw new BusinessException(SECRET_KEY_IS_INCORRECT);
        }
    }

    public static void ensureIsTrueFor(User user, String secretKey) {
        new SecretKeyIsCorrect(user, secretKey).ensureIsTrue();
    }
}
