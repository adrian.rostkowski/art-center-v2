package com.adrianrostkowski.artcenter.domain.model.auction.image;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.integractions.aws.images.S3ImagesService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "auction_image")
public class AuctionImage extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "image_type")
    private ImageType imageType;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_type")
    private String fileType;

    @Lob
    private byte[] data;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "auction_id", referencedColumnName = "id")
    private Auction auction;

    public static AuctionImage create(MultipartFile file, ImageType imageType) {

        if (file.getOriginalFilename() == null) {
            throw new IllegalArgumentException("Sorry! Filename is null");
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new IllegalArgumentException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            return AuctionImage.builder()
                    .fileName(fileName)
                    .fileType(file.getContentType())
                    .data(file.getBytes())
                    .imageType(imageType)
                    .build();

        } catch (IOException ex) {
            // todo change this exception
            throw new IllegalArgumentException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public void addAuctionToSupport(Auction ychAuction) {
        this.auction = ychAuction;
    }

    public void sendToElectronicArchive(S3ImagesService s3ImagesService) {
        s3ImagesService.uploadFileToBucket(this.getId().toString(), this.getData());
        this.data = null;
    }

}
