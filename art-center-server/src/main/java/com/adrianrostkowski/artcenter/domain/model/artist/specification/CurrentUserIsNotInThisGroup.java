package com.adrianrostkowski.artcenter.domain.model.artist.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroup;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AllArgsConstructor;

import java.util.Objects;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.YOU_HAVE_ALREADY_THIS_ARTIST_IN_YOUR_FOLLOW_LIST;

@AllArgsConstructor
public class CurrentUserIsNotInThisGroup extends CriticalRule {

    private final FollowGroup followGroup;

    @Override
    protected void ensureIsTrue() {
        var currentUser = UserContextHelper.getCurrentUser();
        var isUserAlreadyInGroup = followGroup.getUsers().stream()
                .map(BaseEntity::getId)
                .anyMatch(n -> Objects.equals(n, currentUser.getId()));

        if (isUserAlreadyInGroup) {
            throw new BusinessException(YOU_HAVE_ALREADY_THIS_ARTIST_IN_YOUR_FOLLOW_LIST);
        }
    }

    public static void ensureIsTrueFor(FollowGroup followGroup) {
        new CurrentUserIsNotInThisGroup(followGroup).ensureIsTrue();
    }
}
