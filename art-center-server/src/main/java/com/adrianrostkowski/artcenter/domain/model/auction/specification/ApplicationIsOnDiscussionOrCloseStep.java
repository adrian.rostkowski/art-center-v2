package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ApplicationIsOnDiscussionOrCloseStep extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (!ychAuction.getAuctionStep().equals(AuctionStep.CONSULTATIONS) && !ychAuction.getAuctionStep().equals(AuctionStep.CLOSED)) {
            throw new IllegalArgumentException(String.format("Ych auction step should be equals to CONSULTATIONS, auction id [%s]", ychAuction.getId()));
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new ApplicationIsOnDiscussionOrCloseStep(ychAuction).ensureIsTrue();
    }
}
