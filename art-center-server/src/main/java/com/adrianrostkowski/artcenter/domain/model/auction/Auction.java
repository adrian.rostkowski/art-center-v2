package com.adrianrostkowski.artcenter.domain.model.auction;

import com.adrianrostkowski.artcenter.domain.model.auction.actions.ApplicationActions;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.add.AddBidResult;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.add.AddBidToAuction;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionBidList;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionImgBidList;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.BidCurrency;
import com.adrianrostkowski.artcenter.domain.model.auction.end.end_with_winner.CloseAuctionWithWinner;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.*;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionCharacterType;
import com.adrianrostkowski.artcenter.domain.model.auction.values.YchAuctionType;
import com.adrianrostkowski.artcenter.domain.model.bid.specification.BidIsEqualsMaxBidValue;
import com.adrianrostkowski.artcenter.domain.model.comment.Comment;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.auction.image.AuctionImage;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "auction")
public class Auction extends BaseEntity {

    private Auction(
            AuctionStep step,
            User owner,
            AuctionImage image,
            String description,
            BigDecimal startingBid,
            BigDecimal autoBuyBidValue,
            Boolean isMaxBid,
            LocalDateTime beginning,
            LocalDateTime endDate,
            YchAuctionType ychAuctionType,
            YchAuctionCharacterType ychAuctionCharacterType,
            BidCurrency currency
    ) {
        this.auctionStep = step;
        this.image = image;
        this.description = description;
        this.startingBid = startingBid;
        this.autoBuyBidValue = autoBuyBidValue;
        this.isMaxBid = isMaxBid;
        this.beginning = beginning;
        this.owner = owner;
        this.endDate = endDate;
        this.ychAuctionType = ychAuctionType;
        this.ychAuctionCharacterType = ychAuctionCharacterType;
        this.currency = currency;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "auction_step")
    private AuctionStep auctionStep;

    @Enumerated(EnumType.STRING)
    @Column(name = "auction_kind")
    private AuctionKind kind;

    @Enumerated(EnumType.STRING)
    @Column(name = "auction_type")
    private YchAuctionType ychAuctionType;

    @Enumerated(EnumType.STRING)
    @Column(name = "character_type")
    private YchAuctionCharacterType ychAuctionCharacterType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "winner_id", referencedColumnName = "id")
    private User winner;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private AuctionImage image;

    @OneToMany(mappedBy = "auction")
    private List<AuctionImage> supportImages;

    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL)
    private final List<Bid> bids = new ArrayList<>();

    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL)
    private final List<ImgBid> imgBids = new ArrayList<>();

    @Column(name = "description")
    private String description;

    @Column(name = "starting_bid")
    private BigDecimal startingBid;

    @Column(name = "auto_buy_bid_value")
    private BigDecimal autoBuyBidValue;

    @Column(name = "is_max_bid")
    private Boolean isMaxBid;

    @Column(name = "beginning")
    private LocalDateTime beginning;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "ended_by_auto_buy")
    private Boolean endedByAutoBuy;

    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL)
    private final List<Comment> comments = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private BidCurrency currency;

    public static Auction createBase(
            User user,
            AuctionImage image,
            String description,
            BigDecimal minBid,
            BigDecimal maxBid,
            Boolean isMaxBid,
            LocalDateTime beginning,
            LocalDateTime endDate,
            YchAuctionType ychAuctionType,
            YchAuctionCharacterType ychAuctionCharacterType,
            BidCurrency currency
    ) {
        return new Auction(
                AuctionStep.BIDDING,
                user,
                image,
                description,
                minBid,
                maxBid,
                isMaxBid,
                beginning,
                endDate,
                ychAuctionType,
                ychAuctionCharacterType,
                currency
        );
    }

    public long daysToEnd() {
        // TODO: 06.01.2022 there is already endDate, why then create new one?
//        final var endDate = beginning.toLocalDate().plusDays(getCountOfDays());

        if (endDate.isBefore(LocalDateTime.now())) {
            return 0;
        } else {
            return (beginning.getDayOfYear() + getCountOfDays()) - LocalDate.now().getDayOfYear();
        }
    }

    public AddBidResult addCurrencyBid(Bid bid) {
        return AddBidToAuction.addCurrencyBid(
                bid,
                this,
                startingBid,
                AuctionBidList.of(bids),
                AuctionImgBidList.of(imgBids),
                isMaxBid,
                autoBuyBidValue
        );
    }

    public void addImgBid(ImgBid imgBid) {
        AddBidToAuction.addImgBid(
                imgBid,
                this,
                startingBid,
                AuctionBidList.of(bids),
                AuctionImgBidList.of(imgBids),
                isMaxBid,
                autoBuyBidValue
        );
    }

    public boolean isGreaterThenHighestBid(BigDecimal bid) {
        return AuctionBidList.of(bids).isGreaterThenHighestBid(bid);
    }

    public boolean isGreaterEqualsThenStartingBid(BigDecimal bid) {
        return bid.compareTo(startingBid) >= 1;
    }

    public BigDecimal getHighestBidValue() {
        return AuctionBidList.of(bids).getHighestBidValue();
    }

    public void addComment(Comment comment) {
        if (!comment.getText().isBlank()) {
            this.comments.add(comment);
        } else {
            throw new IllegalArgumentException("Comment can not have empty text");
        }
    }

    public Long getSecondsToEnd() {
        var endDateTime = beginning.plusDays(getCountOfDays());
        return ChronoUnit.SECONDS.between(LocalDateTime.now(), endDateTime);
    }

    public Long getSecondsToEndArtTrade() {
        var endDateTime = beginning.plusDays(getCountOfDays());
        return ChronoUnit.SECONDS.between(LocalDateTime.now().minusDays(1), endDateTime);
    }

    public void autoBuy(User winner, Bid maxBid) {
        ApplicationIsOnBiddingStep.ensureIsTrueFor(this);
        ApplicationContainsMaxBid.ensureIsTrueFor(this);
        BidIsEqualsMaxBidValue.ensureIsTrueFor(this, maxBid);

        final var addBidResult = this.addCurrencyBid(maxBid);

        if (addBidResult != AddBidResult.ADDED_CURRENCY_BID_AND_AUCTION_SHOULD_BE_CLOSED) {
            throw new IllegalStateException(
                    "During auto buy addBidResult should be equals to ADDED_CURRENCY_BID_AND_AUCTION_SHOULD_BE_CLOSED, but was [%s]"
                            .formatted(addBidResult));
        }

        auctionStep = AuctionStep.CONSULTATIONS;
        endedByAutoBuy = Boolean.TRUE;
        this.winner = winner;
    }

    public void closeAuction() {
        ApplicationIsProcessingByOwner.ensureIsTrueFor(this);
        auctionStep = AuctionStep.CLOSED;
    }

    private long getCountOfDays() {
        return Duration.between(beginning, endDate).toDays();
    }

    public void setKind(AuctionKind kind) {
        this.kind = kind;
    }

    public void setSupportImages(List<AuctionImage> supportImages) {
        this.supportImages = supportImages;
    }

    public List<String> getAvailableActions() {
        return ApplicationActions.getAvailableActionsFor(this);
    }

    public Boolean isOwnerChooseTime() {
        return kind.equals(AuctionKind.ART_TRADE) && auctionStep.equals(AuctionStep.BIDDING) ?
                getSecondsToEnd() <= 0 :
                Boolean.FALSE;
    }

    public List<Bid> getSortedBids() {
        bids.sort(Comparator.comparing(Bid::getValue).reversed());
        return bids;
    }

    public boolean isCurrencyAuction() {
        return !Objects.equals(getKind(), AuctionKind.ART_TRADE);
    }

    public boolean isArtTrade() {
        return Objects.equals(getKind(), AuctionKind.ART_TRADE);
    }

    public void endAuctionWithAWinner(EmailService emailService, String discussionUrl) {
        new CloseAuctionWithWinner(this, emailService, discussionUrl).close();
    }

    public void endCurrencyAuctionWithAWinner() {
        ApplicationIsOnBiddingStep.ensureIsTrueFor(this);
        ApplicationContainsAnyBid.ensureIsTrueFor(this);

        var highestBid = AuctionBidList.of(bids).getHighestBid()
                .orElseThrow(() -> new IllegalStateException(
                        "Auction during closing, should contains at least one bid, auction id [%s]".formatted(this.getId()))
                );

        auctionStep = AuctionStep.CONSULTATIONS;
        this.winner = highestBid.getOwner();
    }

    public void endArtTradeWithAWinner() {
        auctionStep = AuctionStep.CONSULTATIONS;
        this.winner = AuctionImgBidList.of(this.imgBids)
                .getWinningBid()
                .orElseThrow(() -> new IllegalStateException("Can not find any winning img bid"))
                .getOwner();
    }
}
