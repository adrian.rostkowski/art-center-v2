package com.adrianrostkowski.artcenter.domain.model.artist.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.artist.FollowGroup;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AllArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.YOU_CAN_NOT_FOLLOW_YOURSELF;

@AllArgsConstructor
public class CurrentUserIsNotArtistOfGroup extends CriticalRule {

    private final FollowGroup followGroup;

    @Override
    protected void ensureIsTrue() {
        var currentUser = UserContextHelper.getCurrentUser();

        if (followGroup.getArtist().getId().equals(currentUser.getId())) {
            throw new BusinessException(YOU_CAN_NOT_FOLLOW_YOURSELF);
        }
    }

    public static void ensureIsTrueFor(FollowGroup followGroup) {
        new CurrentUserIsNotArtistOfGroup(followGroup).ensureIsTrue();
    }
}
