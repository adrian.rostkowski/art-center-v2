package com.adrianrostkowski.artcenter.domain.model.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BusinessExceptionCodes {
    TEST_BUSINESS_EXCEPTION_CODE("Test Business Exception Code"),
    APPLICATION_MUST_CONTAIN_MAX_BID("Application does not contain max bid"),
    APPLICATION_MUST_BE_ON_BIDDING_STEP("Application is not in bidding"),

    USER_OLD_PASSWORD_MUST_BE_EQUALS_TO_CURRENT_PASSWORD("Wrong old password!"),
    NEW_PASSWORD_CAN_NOT_BE_EQUALS_TO_CURRENT_PASSWORD("New password can not be equals to current password!"),
    PASSWORDS_ARE_NOT_EQUALS("Passwords are not equals!"),
    MAX_PASSWORD_LENGTH_IS_20("Max password length is equals to 20!"),
    MIN_PASSWORD_LENGTH_IS_6("Min password length is equals to 6!"),
    MAX_LOGIN_LENGTH_IS_20("Max login length is equals to 20!"),
    MIN_LOGIN_LENGTH_IS_6("Min login length is equals to 6!"),
    MAX_USER_NAME_LENGTH_IS_20("Max user name length is equals to 20!"),
    MIN_USER_NAME_LENGTH_IS_6("Min user name length is equals to 6!"),

    BID_VALUE_IS_TOO_LOW("Bid value is too low"),
    USER_CAN_NOT_ADD_BID_TO_HIS_OWN_AUCTION("Owner can not add bid to his own auction :)"),
    YOU_ARE_NOT_OWNER_OR_WINNER_ARE_YOU("You are not owner or winner, are you? :P"),
    MAX_BID_CAN_NOT_BE_LOWER_THEN_MIN_BID("Max bid can not be lower then min bid"),

    SECRET_KEY_IS_INCORRECT("Secret key is incorrect! >:("),

    YOU_HAVE_ALREADY_THIS_ARTIST_IN_YOUR_FOLLOW_LIST("You have already this artist in your follow list :/"),
    YOU_CAN_NOT_FOLLOW_YOURSELF("You can not follow yourself :P"),

    WRONG_USER_NAME_OR_PASSWORD("Wrong username or password");

    private final String message;
}
