package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import lombok.RequiredArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_BE_ON_BIDDING_STEP;

@RequiredArgsConstructor
public class ApplicationIsOnBiddingStep extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (!ychAuction.getAuctionStep().equals(AuctionStep.BIDDING)) {
            throw new BusinessException(APPLICATION_MUST_BE_ON_BIDDING_STEP);
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new ApplicationIsOnBiddingStep(ychAuction).ensureIsTrue();
    }
}
