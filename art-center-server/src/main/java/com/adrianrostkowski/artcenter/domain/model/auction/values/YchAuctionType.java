package com.adrianrostkowski.artcenter.domain.model.auction.values;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum YchAuctionType {
    SAFE("SAFE"),
    EXPLICIT("EXPLICIT"),
    NOT_SAFE_FOR_WORK("NOT_SAFE_FOR_WORK"),
    ALL("ALL");

    private final String name;
}
