package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionBidList;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionImgBidList;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.List;

public class AddBidToAuction {

    private final Bid newBid;
    private final ImgBid newImgBid;
    private final Auction auction;
    private final BigDecimal startingBid;
    private final AuctionBidList auctionBidList;
    private final AuctionImgBidList auctionImgBidList;
    private final boolean isMaxBid;
    private final BigDecimal autoBuyBidValue;

    private AddBidToAuction(
            Bid bid,
            ImgBid newImgBid,
            Auction auction,
            BigDecimal startingBid,
            AuctionBidList auctionBidList,
            AuctionImgBidList auctionImgBidList,
            boolean isMaxBid,
            BigDecimal autoBuyBidValue
    ) {
        this.newBid = bid;
        this.newImgBid = newImgBid;
        this.auction = auction;
        this.startingBid = startingBid;
        this.auctionBidList = auctionBidList;
        this.auctionImgBidList = auctionImgBidList;
        this.isMaxBid = isMaxBid;
        this.autoBuyBidValue = autoBuyBidValue;
    }

    public static AddBidResult addCurrencyBid(
            Bid bid,
            Auction auction,
            BigDecimal startingBid,
            AuctionBidList auctionBidList,
            AuctionImgBidList auctionImgBidList,
            boolean isMaxBid,
            BigDecimal autoBuyBidValue
    ) {
        return new AddBidToAuction(
                bid,
                null,
                auction,
                startingBid,
                auctionBidList,
                auctionImgBidList,
                isMaxBid,
                autoBuyBidValue
        ).doAdd();
    }

    public static AddBidResult addImgBid(
            ImgBid newImgBid,
            Auction auction,
            BigDecimal startingBid,
            AuctionBidList auctionBidList,
            AuctionImgBidList auctionImgBidList,
            boolean isMaxBid,
            BigDecimal autoBuyBidValue
    ) {
        return new AddBidToAuction(
                null,
                newImgBid,
                auction,
                startingBid,
                auctionBidList,
                auctionImgBidList,
                isMaxBid,
                autoBuyBidValue
        ).doAdd();
    }

    private AddBidResult doAdd() {
        ValidateBidAddition.ensureIsTrueFor(auction);

        final var strategies = List.of(
                createAddBidAndCloseAuction(),
                createAddFinancialBid(),
                createAddImgBid()
        );

        final var addBidStrategy = strategies.stream()
                .filter(AddBidStrategy::isRequired)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        "An error occurred during add new bid to auction id : [%s] and bid id : [%s]. No one strategy was chose"
                                .formatted(newBid.getAuction().getId(), newBid.getId())));

        addBidStrategy.add();
        return addBidStrategy.result();
    }

    @NotNull
    private AddBidAndCloseAuctionStrategy createAddBidAndCloseAuction() {
        return new AddBidAndCloseAuctionStrategy(isMaxBid, autoBuyBidValue, newBid, auctionBidList, auction);
    }

    @NotNull
    private AddIFinancialBidStrategy createAddFinancialBid() {
        return new AddIFinancialBidStrategy(auctionBidList, newBid, startingBid, auction);
    }

    @NotNull
    private AddImgBidStrategy createAddImgBid() {
        return new AddImgBidStrategy(auctionImgBidList, newImgBid, auction);
    }
}
