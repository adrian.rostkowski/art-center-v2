package com.adrianrostkowski.artcenter.domain.model.comment;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionStep;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Comment extends BaseEntity {

    public Comment(User user, String text, Auction auction, CommentType type) {

        if (text.isBlank()) {
            throw new IllegalArgumentException("Comment can not contain empty text");
        }

        this.step = auction.getAuctionStep();
        this.user = user;
        this.text = text;
        this.type = type;
        this.auction = auction;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User user;

    @Column(name = "text", columnDefinition = "TEXT")
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "auction", referencedColumnName = "id")
    private Auction auction;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private CommentType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "step")
    private AuctionStep step;
}
