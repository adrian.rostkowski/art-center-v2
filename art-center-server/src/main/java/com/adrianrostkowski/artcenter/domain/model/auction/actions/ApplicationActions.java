package com.adrianrostkowski.artcenter.domain.model.auction.actions;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@AllArgsConstructor
public enum ApplicationActions {
    CHOOSE_A_WINNER(
            (auction, user) -> Objects.equals(auction.getOwner().getLogin(), user.getLogin())
    );

    public final BiFunction<Auction, User, Boolean> isAvailableFor;

    public static List<String> getAvailableActionsFor(Auction auction) {
        return Arrays.stream(ApplicationActions.values())
                .filter(n -> n.isAvailableFor.apply(auction, UserContextHelper.getCurrentUser()))
                .map(Enum::toString)
                .collect(Collectors.toList());
    }
}
