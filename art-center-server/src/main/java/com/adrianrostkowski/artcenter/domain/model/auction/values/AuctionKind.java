package com.adrianrostkowski.artcenter.domain.model.auction.values;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AuctionKind {
    YCH("YCH"),
    ADOPT("ADOPT"),
    ART_TRADE("ART_TRADE");

    private final String name;
}
