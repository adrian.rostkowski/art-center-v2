package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.RequiredArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.APPLICATION_MUST_CONTAIN_MAX_BID;

@RequiredArgsConstructor
public class ApplicationContainsMaxBid extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (!this.ychAuction.getIsMaxBid()) {
            throw new BusinessException(APPLICATION_MUST_CONTAIN_MAX_BID);
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new ApplicationContainsMaxBid(ychAuction).ensureIsTrue();
    }
}
