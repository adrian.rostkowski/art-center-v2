package com.adrianrostkowski.artcenter.domain.model.comment;

public enum CommentType {
    BIDDING,
    AUCTION_BIDDING_ENDED
}
