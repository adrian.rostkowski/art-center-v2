package com.adrianrostkowski.artcenter.domain.model.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByLogin(String userName);
    Optional<User> findByUserName(String userName);
    Long countAllByEmail(String email);
    Long countAllByLogin(String login);
    Long countAllByUserName(String userName);
}
