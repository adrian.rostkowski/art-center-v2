package com.adrianrostkowski.artcenter.domain.model.user.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.RequiredArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.*;

@RequiredArgsConstructor
public class LoginIsCorrect extends CriticalRule {
    private final String login;

    @Override
    protected void ensureIsTrue() {
        if (login.length() > 20) {
            throw new BusinessException(MAX_LOGIN_LENGTH_IS_20);
        }
        if (login.length() < 6) {
            throw new BusinessException(MIN_LOGIN_LENGTH_IS_6);
        }
    }

    public static void ensureIsTrueFor(String oldPassword) {
        new LoginIsCorrect(oldPassword).ensureIsTrue();
    }
}
