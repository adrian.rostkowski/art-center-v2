package com.adrianrostkowski.artcenter.domain.model.auction.end.end_with_winner;

interface CloseAuctionWithWinnerStrategy {
    void close();
    boolean isRequired();
}
