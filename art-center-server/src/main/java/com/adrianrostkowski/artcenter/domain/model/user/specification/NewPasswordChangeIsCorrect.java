package com.adrianrostkowski.artcenter.domain.model.user.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import lombok.RequiredArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.*;

@RequiredArgsConstructor
public class NewPasswordChangeIsCorrect extends CriticalRule {
    private final User user;
    private final String newPassword;

    @Override
    protected void ensureIsTrue() {
        if (user.getPassword().equals(newPassword)) {
            throw new BusinessException(NEW_PASSWORD_CAN_NOT_BE_EQUALS_TO_CURRENT_PASSWORD);
        }
        if (newPassword.length() > 20) {
            throw new BusinessException(MAX_PASSWORD_LENGTH_IS_20);
        }
        if (newPassword.length() < 6) {
            throw new BusinessException(MIN_PASSWORD_LENGTH_IS_6);
        }
    }

    public static void ensureIsTrueFor(User user, String oldPassword) {
        new NewPasswordChangeIsCorrect(user, oldPassword).ensureIsTrue();
    }
}
