package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.ApplicationIsOnBiddingStep;
import com.adrianrostkowski.artcenter.domain.model.auction.specification.CurrentUserIsNotAnOwner;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;

class ValidateBidAddition extends CriticalRule {
    private final Auction auction;

    public ValidateBidAddition(Auction auction) {
        this.auction = auction;
    }

    @Override
    protected void ensureIsTrue() {
        ApplicationIsOnBiddingStep.ensureIsTrueFor(auction);
        CurrentUserIsNotAnOwner.ensureIsTrueFor(auction);
    }

    public static void ensureIsTrueFor(Auction auction) {
        new ValidateBidAddition(auction).ensureIsTrue();
    }
}
