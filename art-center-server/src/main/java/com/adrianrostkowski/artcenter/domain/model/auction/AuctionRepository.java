package com.adrianrostkowski.artcenter.domain.model.auction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;

import java.util.Optional;

@Repository
public interface AuctionRepository extends JpaRepository<Auction, Long>, JpaSpecificationExecutor<Auction> {
    @Override
    @EntityGraph(attributePaths = {"owner"})
    Page<Auction> findAll(@Nullable Specification var1, Pageable var2);

    @Override
    @EntityGraph(attributePaths = {"owner"})
    Optional<Auction> findById(Long id);

    @Query("from Auction y where y.auctionStep = 'BIDDING'")
    Page<Auction> findAllByBiddingStep(PageRequest pageable);

    @Query("from Auction y where y.winner.id = :user_id")
    Page<Auction> findAllByWinner(@Param("user_id") Long userId, Pageable pageable);

    @Query("from Auction y where y.owner.id = :user_id " +
            "and y.winner is not NULL " +
            "and y.auctionStep <> 'BIDDING'")
    Page<Auction> findAllSoldByOwner(@Param("user_id") Long userId, Pageable pageable);

    @Query(value =
            "SELECT * FROM auction a where " +
            "exists (" +
                    "select a.id from bid b where " +
                    "b.auction_id = a.id " +
                    "            and b.owner_id = :user_id " +
                    "            and a.auction_step = 'BIDDING' " +
                    "union " +
                    "select a.id from img_bid imgb where " +
                    "imgb.auction_id = a.id" +
                    "            and imgb.owner_id = :user_id " +
                    "            and a.auction_step = 'BIDDING' " +
                    "    )",
            nativeQuery = true)
    Page<Auction> findAllWhereUserHaveBids(@Param("user_id") Long userId, Pageable pageable);
}
