package com.adrianrostkowski.artcenter.domain.model.user.specification;

import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CurrentUserIsEquals extends CriticalRule {
    private final User user;

    @Override
    protected void ensureIsTrue() {
        if (!user.getLogin().equals(UserContextHelper.getCurrentUser().getLogin())) {
            throw new IllegalStateException(
                    String.format("Current user is not allowed to change specific user, current user login = [%s], user to change login = [%s]",
                            UserContextHelper.getCurrentUser().getLogin(),
                            user.getLogin()
                    ));
        }
    }

    public static void ensureIsTrueFor(User user) {
        new CurrentUserIsEquals(user).ensureIsTrue();
    }
}
