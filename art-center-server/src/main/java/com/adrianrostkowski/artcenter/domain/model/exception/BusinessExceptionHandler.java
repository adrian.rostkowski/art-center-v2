package com.adrianrostkowski.artcenter.domain.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class BusinessExceptionHandler {

    @ResponseBody
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @ExceptionHandler(BusinessException.class)
    public String handleWebException(BusinessException e) {
        return e.getMessage();
    }

}
