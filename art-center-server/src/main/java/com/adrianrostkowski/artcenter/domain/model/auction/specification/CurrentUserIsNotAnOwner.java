package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.RequiredArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.USER_CAN_NOT_ADD_BID_TO_HIS_OWN_AUCTION;

@RequiredArgsConstructor
public class CurrentUserIsNotAnOwner extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (ychAuction.getOwner().getLogin().equals(UserContextHelper.getCurrentUser().getLogin())) {
            throw new BusinessException(USER_CAN_NOT_ADD_BID_TO_HIS_OWN_AUCTION);
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new CurrentUserIsNotAnOwner(ychAuction).ensureIsTrue();
    }
}
