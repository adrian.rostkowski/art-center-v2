package com.adrianrostkowski.artcenter.domain.model.auction.end.end_with_winner;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;

import java.util.List;

public class CloseAuctionWithWinner {

    private final Auction auction;
    private final EmailService emailService;
    private final String discussionUrl;

    public CloseAuctionWithWinner(
            Auction auction,
            EmailService emailService,
            String discussionUrl
    ) {
        this.auction = auction;
        this.emailService = emailService;
        this.discussionUrl = discussionUrl;
    }

    public void close() {
        final var strategies = List.of(
                new CloseCurrencyAuction(auction, emailService, discussionUrl),
                new CloseArtTrade(auction)
        );
        strategies.stream()
                .filter(CloseAuctionWithWinnerStrategy::isRequired)
                .findFirst()
                .orElseThrow()
                .close();
    }
}
