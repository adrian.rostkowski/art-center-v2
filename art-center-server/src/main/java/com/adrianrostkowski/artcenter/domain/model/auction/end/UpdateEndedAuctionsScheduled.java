package com.adrianrostkowski.artcenter.domain.model.auction.end;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.AuctionRepository;
import com.adrianrostkowski.artcenter.integractions.aws.email.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class UpdateEndedAuctionsScheduled {

    @Value("${email.url.auction.discussion}")
    private String discussionUrl;

    public UpdateEndedAuctionsScheduled(AuctionRepository ychs, EmailService emailService) {
        this.ychs = ychs;
        this.emailService = emailService;
    }

    @PersistenceContext
    private EntityManager em;
    private final AuctionRepository ychs;
    private static final int PAGE_SIZE = 10;

    private final EmailService emailService;

    @Scheduled(fixedRate = 5000000)
    @Transactional
    public void scheduleFixedDelayTask() {
        deactivateEndedAuctions();
    }


    private void deactivateEndedAuctions() {
        var countOfPages = 1;

        for (int i = 0; i <= countOfPages; i++) {
            var auctions = ychs.findAllByBiddingStep(PageRequest.of(i, PAGE_SIZE));
            countOfPages = auctions.getTotalPages();
            closeAuctions(auctions.getContent());
            em.flush();
            em.clear();
        }
    }

    private void closeAuctions(List<Auction> auctions) {
        for (Auction auction : auctions) {
            if (auction.isCurrencyAuction()) {
                closeCurrencyAuction(auction);
            } else {
                closeArtTrade(auction);
            }
        }
    }

    private void closeArtTrade(Auction auction) {
        if (auction.getSecondsToEndArtTrade() <= 0) {
            auction.closeAuction();
        }
    }

    private void closeCurrencyAuction(Auction auction) {
        if (auction.getSecondsToEnd() <= 0 && auction.getBids().isEmpty()) {
            auction.closeAuction();
        } else if (auction.getSecondsToEnd() <= 0 && !auction.getBids().isEmpty()) {
            auction.endAuctionWithAWinner(emailService, discussionUrl);
        }
    }
}
