package com.adrianrostkowski.artcenter.domain.model.auction.bid.add;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.bid.lists.AuctionImgBidList;
import com.adrianrostkowski.artcenter.domain.model.imgbid.ImgBid;

import static java.util.Objects.isNull;

class AddImgBidStrategy implements AddBidStrategy {

    private final AuctionImgBidList auctionImgBidList;
    private final ImgBid newImgBid;
    private final Auction auction;

    public AddImgBidStrategy(
            AuctionImgBidList auctionImgBidList,
            ImgBid imgBid,
            Auction auction
    ) {
        this.auctionImgBidList = auctionImgBidList;
        this.newImgBid = imgBid;
        this.auction = auction;
    }

    @Override
    public void add() {
        isArtTradeAuction();
        auctionImgBidList.add(newImgBid);
    }

    @Override
    public boolean isRequired() {
        return !isNull(newImgBid) && auction.isArtTrade();
    }

    @Override
    public AddBidResult result() {
        return AddBidResult.ADDED_IMG_BID;
    }

    private void isArtTradeAuction() {
        if (!auction.isArtTrade()) {
            throw new IllegalStateException("Current auction should be an ART TRADE, right now is : [%s]".formatted(auction.getKind()));
        }
    }
}
