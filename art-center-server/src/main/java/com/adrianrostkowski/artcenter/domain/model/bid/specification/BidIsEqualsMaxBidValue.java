package com.adrianrostkowski.artcenter.domain.model.bid.specification;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.bid.Bid;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.AllArgsConstructor;

import static java.util.Objects.isNull;

@AllArgsConstructor
public class BidIsEqualsMaxBidValue extends CriticalRule {
    private final Auction ychAuction;
    private final Bid bid;

    @Override
    protected void ensureIsTrue() {
        if (isNull(ychAuction.getAutoBuyBidValue())) {
            throw new IllegalArgumentException(String.format("When checking the max bid, max bid was null, bid id - [%s], auction id - [%s]", bid.getId(), ychAuction.getId()));
        }
        if (!ychAuction.getAutoBuyBidValue().equals(bid.getValue())) {
            throw new IllegalArgumentException(String.format("This bid is not equals to max bid of the auction, bid id - [%s], auction id - [%s]", bid.getId(), ychAuction.getId()));
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction, Bid bid) {
        new BidIsEqualsMaxBidValue(ychAuction, bid).ensureIsTrue();
    }
}
