package com.adrianrostkowski.artcenter.domain.model.auction.end.end_with_winner;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.auction.values.AuctionKind;

import java.util.Objects;

class CloseArtTrade implements CloseAuctionWithWinnerStrategy {

    private final Auction auction;

    public CloseArtTrade(
            Auction auction
    ) {
        this.auction = auction;
    }

    @Override
    public void close() {
        auction.endArtTradeWithAWinner();
    }

    @Override
    public boolean isRequired() {
        return Objects.equals(auction.getKind(), AuctionKind.ART_TRADE);
    }
}
