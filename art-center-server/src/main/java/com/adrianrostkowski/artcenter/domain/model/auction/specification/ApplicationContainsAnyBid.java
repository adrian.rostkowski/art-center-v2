package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ApplicationContainsAnyBid extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (ychAuction.getBids().isEmpty()) {
            throw new IllegalArgumentException(String.format("Ych auction should contain any bid [%s]", ychAuction));
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new ApplicationContainsAnyBid(ychAuction).ensureIsTrue();
    }
}
