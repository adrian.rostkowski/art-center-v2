package com.adrianrostkowski.artcenter.domain.model.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Log4j2
public class DtoValidationExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    String onConstraintValidationException(ConstraintViolationException e) {
        return e.getMessage() + "xxxx";
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<String> handleWebException(MethodArgumentNotValidException e) {
        return e.getBindingResult().getAllErrors().stream()
                .map(n -> {
                    final var isBusinessExc = Arrays.stream(BusinessExceptionCodes.values())
                            .anyMatch(businessCode -> businessCode.name().equals(n.getDefaultMessage()));

                    if (isBusinessExc) {
                        return BusinessExceptionCodes.valueOf(n.getDefaultMessage()).getMessage();
                    } else {
                        log.error("Not BusinessException was found in DtoValidationExceptionHandler.handleWebException()");
                        return n.getDefaultMessage();
                    }
                })
                .collect(Collectors.toList());
    }
}
