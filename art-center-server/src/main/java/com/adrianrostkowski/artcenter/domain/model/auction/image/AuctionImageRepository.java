package com.adrianrostkowski.artcenter.domain.model.auction.image;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuctionImageRepository extends JpaRepository<AuctionImage, Long> {

    @Override
    @EntityGraph(attributePaths = { "auction" })
    Optional<AuctionImage> findById(Long id);

}
