package com.adrianrostkowski.artcenter.domain.model.artist;

import com.adrianrostkowski.artcenter.domain.model.artist.specification.CurrentUserIsNotArtistOfGroup;
import com.adrianrostkowski.artcenter.domain.model.artist.specification.CurrentUserIsNotInThisGroup;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import com.adrianrostkowski.artcenter.domain.model.user.role.Privilege;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "follow_group")
public class FollowGroup extends BaseEntity {
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "artist_id", referencedColumnName = "id")
    private User artist;

    @ManyToMany
    @JoinTable(
            name = "follow_groups",
            joinColumns = @JoinColumn(
                    name = "group_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"))
    private List<User> users;

    public static FollowGroup createOf(User artist) {
        return new FollowGroup(artist, new ArrayList<>());
    }

    public void addCurrentUser() {
        CurrentUserIsNotArtistOfGroup.ensureIsTrueFor(this);
        CurrentUserIsNotInThisGroup.ensureIsTrueFor(this);
        var currentUser = UserContextHelper.getCurrentUser();
        users.add(currentUser);
    }
}
