package com.adrianrostkowski.artcenter.domain.model.bid;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.BaseEntity;
import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.user.User;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.BID_VALUE_IS_TOO_LOW;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Bid extends BaseEntity {

    @Column(name = "value")
    private BigDecimal value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "auction_id", referencedColumnName = "id")
    private Auction auction;

    public static Bid create(BigDecimal value, User owner, Auction auction) {
        if (auction.isGreaterThenHighestBid(value) || auction.isGreaterEqualsThenStartingBid(value)) {
            return new Bid(value, owner, auction);
        } else {
            throw new BusinessException(BID_VALUE_IS_TOO_LOW);
        }
    }
}
