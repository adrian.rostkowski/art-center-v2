package com.adrianrostkowski.artcenter.domain.model.auction.specification;

import com.adrianrostkowski.artcenter.domain.model.auction.Auction;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import com.adrianrostkowski.artcenter.domain.model.user.UserContextHelper;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

@RequiredArgsConstructor
public class ApplicationIsProcessingByOwner extends CriticalRule {
    private final Auction ychAuction;

    @Override
    protected void ensureIsTrue() {
        if (!Objects.equals(ychAuction.getOwner().getLogin(), UserContextHelper.getCurrentUser().getLogin())) {
            throw new IllegalArgumentException(String.format("Ych auction should be processing by owner, auction id [%s], user login - [%s]", ychAuction.getId(), UserContextHelper.getCurrentUser().getLogin()));
        }
    }

    public static void ensureIsTrueFor(Auction ychAuction) {
        new ApplicationIsProcessingByOwner(ychAuction).ensureIsTrue();
    }
}
