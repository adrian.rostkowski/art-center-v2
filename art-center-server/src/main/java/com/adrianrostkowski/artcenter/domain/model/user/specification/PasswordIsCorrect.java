package com.adrianrostkowski.artcenter.domain.model.user.specification;

import com.adrianrostkowski.artcenter.domain.model.exception.BusinessException;
import com.adrianrostkowski.artcenter.domain.model.shared.CriticalRule;
import lombok.RequiredArgsConstructor;

import static com.adrianrostkowski.artcenter.domain.model.exception.BusinessExceptionCodes.*;

@RequiredArgsConstructor
public class PasswordIsCorrect extends CriticalRule {
    private final String newPassword;

    @Override
    protected void ensureIsTrue() {
        if (newPassword.length() > 20) {
            throw new BusinessException(MAX_PASSWORD_LENGTH_IS_20);
        }
        if (newPassword.length() < 6) {
            throw new BusinessException(MIN_PASSWORD_LENGTH_IS_6);
        }
    }

    public static void ensureIsTrueFor(String oldPassword) {
        new PasswordIsCorrect(oldPassword).ensureIsTrue();
    }
}
