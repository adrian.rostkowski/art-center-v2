import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { TabMenuModule } from 'primeng/tabmenu';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorsModule } from './form-controls/control-errors/control-errors.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { MessageService } from 'primeng/api';
import { TokenInterceptorService } from './api/token-interceptor.service';
import { NumberInputModule } from './form-controls/number-input/number-input.module';
import { SwitchInputModule } from './form-controls/switch-input/switch-input.module';
import { EditorInputModule } from './form-controls/editor-input/editor-input.module';
import { WavesAnimationModule } from './visual-components/waves-animation/waves-animation.module';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    TabMenuModule,
    ReactiveFormsModule,
    ControlErrorsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NumberInputModule,
    SwitchInputModule,
    EditorInputModule,
    WavesAnimationModule,
    ToastModule
  ],
  providers: [
    MessageService,
    {
      provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true
    }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

export class CustomLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of({KEY: 'value'});
  }
}
