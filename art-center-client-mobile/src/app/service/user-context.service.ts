import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { UserApiService } from '../api/user/user-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserContextService {

  private userNameSubject = new BehaviorSubject<string>('');
  private isUserLoggedSubject = new BehaviorSubject<boolean>(false);
  private isFirstRun = true;

  constructor(
    private localStorageService: LocalStorageService,
    private userApiService: UserApiService
  ) {
    // this.isUserLoggedSubject.next(!!localStorageService.getAuthorizationData());
    //
    // if (this.isFirstRun) {
    //   this.userApiService.checkAuth().subscribe(
    //     success => {
    //       this.isUserLoggedSubject.next(true);
    //     },
    //     error => {
    //       this.localStorageService.removeAuthorizationData();
    //       this.isUserLoggedSubject.next(false);
    //     }
    //   );
    // }
  }

  getIsUserLogged(): BehaviorSubject<boolean> {
    return this.isUserLoggedSubject;
  }

  getUserName(): BehaviorSubject<string> {
    return this.userNameSubject;
  }
}
