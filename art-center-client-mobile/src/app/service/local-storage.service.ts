import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  public setAuthorizationData(auth: string): void {
    localStorage.setItem('AuthenticationACBarer', auth);
  }

  public removeAuthorizationData(): void {
    localStorage.removeItem('AuthenticationACBarer');
  }

  public getAuthorizationData(): string {
    return localStorage.getItem('AuthenticationACBarer');
  }
}
