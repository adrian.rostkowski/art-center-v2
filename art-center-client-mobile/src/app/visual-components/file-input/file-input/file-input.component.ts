import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.css']
})
export class FileInputComponent {

  @Input() countOfImages = 1;
  @Input() multiple = '';
  @Input() files: File[] = [];

  @Output() filesWasChanged = new EventEmitter<File[]>();

  onSelect(event: any): void {
    for (const file of event.files) {
      this.files.push(file);
    }
    this.emitFilesWasChanged();
  }

  onRemove(event: any): void {
    this.files = this.files.filter(n => n.name !== event.file.name);
    this.emitFilesWasChanged();
  }

  emitFilesWasChanged(): void {
    this.filesWasChanged.emit(this.files);
  }
}
