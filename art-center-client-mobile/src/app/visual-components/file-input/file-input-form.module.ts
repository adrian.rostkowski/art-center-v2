import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileInputComponent } from './file-input/file-input.component';
import { FileUploadModule } from 'primeng/fileupload';



@NgModule({
  declarations: [
    FileInputComponent
  ],
  exports: [
    FileInputComponent
  ],
  imports: [
    CommonModule,
    FileUploadModule
  ]
})
export class FileInputFormModule { }
