import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-auction-filter',
  templateUrl: './auction-filter.component.html',
  styleUrls: ['./auction-filter.component.css']
})
export class AuctionFilterComponent implements OnInit {
  auctionTypes: SelectItem[] = [
    {name: 'YCH', code: 'YCH'},
    {name: 'Adopt', code: 'ADOPT'},
    {name: 'ArtTrade', code: 'ART_TRADE'},
  ];
  selectedAuctionTypes: SelectItem[] = [];

  constructor() { }

  ngOnInit(): void {
    this.auctionTypes = [
      {name: 'YCH', code: 'YCH'},
      {name: 'Adopt', code: 'ADOPT'},
      {name: 'ArtTrade', code: 'ART_TRADE'},
    ];
  }

}

export interface SelectItem {
  name: string;
  code: string;
}
