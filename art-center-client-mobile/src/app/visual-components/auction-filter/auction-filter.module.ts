import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuctionFilterComponent } from './auction-filter/auction-filter.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        AuctionFilterComponent
    ],
    exports: [
        AuctionFilterComponent
    ],
    imports: [
      CommonModule,
      MultiSelectModule,
      FormsModule,

      ReactiveFormsModule
    ]
})
export class AuctionFilterModule { }
