import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.css']
})
export class ActionButtonComponent {

  @Input() pIcon: string;
  @Input() label: string;
  @Input() text: string;
  @Input() color = '--action-button';
  @Input() isOff = false;

  @Output() buttonClicked = new EventEmitter<void>();

  buttonWasClicked(): void {
    this.buttonClicked.emit();
  }
}
