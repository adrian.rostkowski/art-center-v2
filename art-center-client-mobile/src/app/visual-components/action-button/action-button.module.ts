import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionButtonComponent } from './action-button/action-button.component';
import { RippleModule } from 'primeng/ripple';


@NgModule({
  declarations: [
    ActionButtonComponent
  ],
  exports: [
    ActionButtonComponent
  ],
  imports: [
    CommonModule,
    RippleModule,
  ]
})
export class ActionButtonModule {
}
