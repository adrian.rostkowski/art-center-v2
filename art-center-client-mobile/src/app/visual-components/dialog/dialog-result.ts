export interface DialogResult {
  success: boolean;
  body: any;
}
