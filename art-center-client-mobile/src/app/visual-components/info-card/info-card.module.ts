import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoCardComponent } from './info-card/info-card.component';



@NgModule({
    declarations: [
        InfoCardComponent
    ],
    exports: [
        InfoCardComponent
    ],
    imports: [
        CommonModule
    ]
})
export class InfoCardModule { }
