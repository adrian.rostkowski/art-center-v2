import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css']
})
export class InfoCardComponent implements OnInit {

  @Input() text: string;
  @Input() label: string;
  @Input() width: string;
  @Output() click = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

}
