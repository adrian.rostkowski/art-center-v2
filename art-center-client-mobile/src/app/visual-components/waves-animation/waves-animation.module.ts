import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WavesAnimationComponent } from './waves-animation/waves-animation.component';



@NgModule({
  declarations: [
    WavesAnimationComponent
  ],
  exports: [
    WavesAnimationComponent
  ],
  imports: [
    CommonModule
  ]
})
export class WavesAnimationModule { }
