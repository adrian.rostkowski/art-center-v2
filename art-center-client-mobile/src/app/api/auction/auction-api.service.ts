import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import {
  AuctionDto,
  CreateAuctionCommand,
  GetAuctionsListCommand,
  GetAuctionsListResponse
} from '../backend-model/backend-model';
import { GenericResponse } from '../generic-response';

@Injectable({
  providedIn: 'root'
})
export class AuctionApiService {
  PREFIX = 'api/auction';

  constructor(private apiService: ApiService) {}

  create(cmd: CreateAuctionCommand, file2: File): Observable<GenericResponse<AuctionDto>> {
    const data: FormData = new FormData();

    data.append('mainArt', file2);
    Object.keys(cmd)
      .filter(key => cmd[key] != null)
      .forEach(key => data.append(key, cmd[key]));

    return this.apiService.post(
      this.PREFIX + '/create',
      data,
    );
  }

  getAuctionsList(cmd: GetAuctionsListCommand): Observable<GetAuctionsListResponse> {
    return this.apiService.post(
      this.PREFIX + '/get/list',
      cmd,
    );
  }

  getById(id: number): Observable<AuctionDto> {
    return this.apiService.get(
      this.PREFIX + `/get/${id}`
    );
  }
}
