import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, finalize, map, tap } from 'rxjs/operators';
import { GenericResponse } from './generic-response';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from '../service/local-storage.service';
import { UserContextService } from '../service/user-context.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private static REQUEST_STATUS_FINISHED_UPDATE_TIMEOUT = 1000;
  private requestExecuting = new BehaviorSubject<boolean>(false);

  readonly baseUrl: string;
  private numberOfRequestExecuting = 0;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private translate: TranslateService,
    private localStorageService: LocalStorageService,
    // private userContextService: UserContextService,
  ) {
    this.baseUrl = environment.proxyUrl;
  }

  get<T>(
    path: string,
    successMessageCode: string = null,
    errorMessageCode: string = null,
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: 'body';
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
  ): Observable<T> {
    this.requestStarted();
    return this.http
      .get<T>(`${this.baseUrl}/${path}`, options)
      .pipe(map(response => this.successHandler(response)))
      .pipe(catchError((error: HttpErrorResponse) => this.errorHandler(error, errorMessageCode)))
      .pipe(finalize(() => this.requestFinished()));
  }

  post<T>(
    path: string,
    data: any,
    successMessageCode?: string,
    errorMessageCode?: string,
    showSpinner: boolean = true,
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: 'body';
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
  ): Observable<T> {
    this.requestStarted();
    // @ts-ignore
    return this.http
      .post(`${this.baseUrl}/${path}`, data, {observe: 'response'})
      .pipe(tap((res: any) => {
        this.setJwt(res.headers.get('Authorization'));
      }))
      .pipe(map(response => this.successHandler(response)))
      .pipe(catchError((error: HttpErrorResponse) => this.errorHandler(error, errorMessageCode)))
      .pipe(finalize(() => this.requestFinished()));
  }

  showSuccess(response: GenericResponse<any>): void {
    this.translate.get('SERVER.SUCCESS.' + response.message).subscribe((translated) => {
      this.messageService.add({severity: 'success', summary: 'Success', detail: translated});
    });
  }

  showError(errorMessage: string): void {
    this.messageService.add({severity: 'error', summary: 'Error', detail: errorMessage});
  }

  showBusinessError(errorMessage: string): void {
    this.translate.get('SERVER.BUSINESS_EXCEPTION.' + errorMessage).subscribe((translated) => {
      this.messageService.add({severity: 'error', summary: 'Error', detail: translated});
    });
  }

  private errorHandler(errorResponse: HttpErrorResponse, errorMessageCode: string, hideSpinner: boolean = false): Observable<never> {
    if (errorResponse.status === 500) {
      this.showError('Server Error');
    } else if (errorResponse.status === 418) {
      this.showBusinessError(errorResponse.error);
    } else if (errorResponse.status === 403) {
      if (!!errorResponse.headers.get('AuthorizationError')) {
        this.showAuthorizationErrors(errorResponse);
      } else {
        this.showError('Action forbidden, please login, relog or create account');
      }
    } else {
      this.showError(errorResponse.error);
    }

    return throwError(errorResponse);
  }

  private successHandler(response: any): any {
    if (response.message) {
      this.showSuccess(response);
    }
    debugger;
    return response;
  }

  private requestStarted(): void {
    this.numberOfRequestExecuting++;
    this.updateRequestStatus();
  }

  private requestFinished(): void {
    this.numberOfRequestExecuting--;
    this.updateRequestStatus();
  }

  private updateRequestStatus(): void {
    if (this.numberOfRequestExecuting > 0) {
      this.requestExecuting.next(true);
    } else {
      setTimeout(() => {
        this.requestExecuting.next(false);
      }, ApiService.REQUEST_STATUS_FINISHED_UPDATE_TIMEOUT);
    }
  }

  private showAuthorizationErrors(errorResponse: HttpErrorResponse): void {
    switch (errorResponse.headers.get('AuthorizationError')) {
      case 'INVALID_JWT': {
        this.showError('Something goes wrong, please try to logout and login again :)');
        break;
      }
      case 'JWT_TIME_OUT': {
        // this.logoutService.logoutUser();
        break;
      }
      default: {
        this.showError('Something goes wrong, please try to logout and login again :)');
        break;
      }
    }
  }

  private setJwt(jwt: string): void {
    if (!jwt) {
      return;
    }

    if (jwt.length === 0) {
      return;
    }

    if (!jwt.startsWith('Bearer ')) {
      return;
    }

    this.localStorageService.setAuthorizationData(jwt);
  }
}
