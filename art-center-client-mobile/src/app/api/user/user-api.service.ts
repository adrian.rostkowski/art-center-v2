import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import { GenericResponse } from '../generic-response';
import { AuctionDto, ResponseMessage } from '../backend-model/backend-model';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {
  PREFIX = 'api/user';

  constructor(private apiService: ApiService) { }

  checkAuth(): Observable<GenericResponse<ResponseMessage>> {
    return this.apiService.get(this.PREFIX + '/check-authentication');
  }
}
