/* tslint:disable */
/* eslint-disable */

export interface AddBidToAuctionCommand {
    auctionId: number;
    bidValue: number;
}

export interface AddBidToAuctionResponse {
    added: boolean;
    bids: BidDto[];
}

export interface AddCommentCommand {
    commentType: CommentType;
    entityId: number;
    textValue: string;
}

export interface AddImgBidCommand {
    auctionId: number;
    description: string;
}

export interface AddImgBidResponse {
}

export interface AuctionCardDto {
    artistName: string;
    auctionStatus: string;
    currency: BidCurrency;
    daysToEnd: number;
    imageId: number;
    minBid: number;
    price: number;
    ychId: number;
}

export interface AuctionCardDtoBuilder {
}

export interface AuctionDiscussionDto {
    auctionKind: AuctionKind;
    comments: CommentDto[];
    description: string;
    imageId: number;
    ownerId: number;
    ownerName: string;
    price: number;
    step: AuctionStep;
    userIsOwner: boolean;
    winnerId: number;
    winnerName: string;
    ychId: number;
}

export interface AuctionDiscussionDtoBuilder {
}

export interface AuctionDto {
    artistName: string;
    auctionIsOver: boolean;
    auctionKind: AuctionKind;
    autoBuyValue: number;
    availableActions: string[];
    beginning: DateAsString;
    bids: BidDto[];
    comments: CommentDto[];
    currency: string;
    description: string;
    endDate: DateAsString;
    imageId: number;
    imgBids: ImgBidDto[];
    isAutoBuy: boolean;
    isOwnerChooseTime: boolean;
    isUserInArtistFollowList: boolean;
    minBid: number;
    secondsToEnd: number;
    step: AuctionStep;
    supportImagesId: number[];
    ychAuctionId: number;
}

export interface AuctionDtoBuilder {
}

export interface AuthCommand {
    login: string;
    password: string;
}

export interface AuthenticationCommandResponse {
    jwt: string;
    userName: string;
}

export interface AutoBuyAuctionCommand {
    ychId: number;
}

export interface BidDto {
    avatarId: number;
    bidValue: number;
    currency: string;
    ownerName: string;
}

export interface BidDtoBuilder {
}

export interface ChangeUserNameCommand {
    currentPassword: string;
    newUsername: string;
}

export interface ChangeUserPasswordCommand {
    currentPassword: string;
    newPassword: string;
    repeatNewPassword: string;
}

export interface ChooseWinnerCommand {
    auctionId: number;
    winnerUserName: string;
}

export interface CommentDto {
    commentId: number;
    ownerName: string;
    text: string;
}

export interface CommentDtoBuilder {
}

export interface Comparable<T> {
}

export interface ConfirmAccountCommand {
    secretKey: string;
    userId: number;
}

export interface CreateAuctionCommand {
    auctionKind: string;
    countOfDays: number;
    currency: string;
    description: string;
    isMaxBid: boolean;
    mainArt: MultipartFile;
    maxBid: number;
    minBid: number;
    ychAuctionCharacterType: string;
    ychAuctionType: string;
}

export interface CreateNewUserCommand {
    email: string;
    login: string;
    password: string;
    repeatPassword: string;
    userName: string;
}

export interface CreateYchAuctionResult {
    auctionDto: AuctionDto;
}

export interface FollowCommand {
    artistToFollow: string;
}

export interface GenericResponse<T> {
    message: string;
    response: T;
    success: boolean;
}

export interface GetAuctionsListCommand {
    auctionKind: AuctionKind;
    auctionStep: AuctionStep;
    daysToEnd: number;
    findByCurrentUser: boolean;
    orderBy: string[];
    orderDirection: Direction;
    pageNumber: number;
    pageSize: number;
    priceRangeFrom: number;
    priceRangeTo: number;
    userName: string;
    ychCharacterType: YchAuctionCharacterType;
    ychStatus: AuctionStatusToSearch;
    ychType: YchAuctionType;
}

export interface GetAuctionsListResponse {
    cards: AuctionCardDto[];
    totalRecords: number;
}

export interface GetBiddingByUserResponse {
    cards: AuctionCardDto[];
    totalElements: number;
}

export interface GetSoldAuctionsResponse {
    cards: AuctionCardDto[];
    totalElements: number;
}

export interface GetSoldCommand {
    pageNumber: number;
    pageSize: number;
}

export interface GetWonYchsCommand {
    pageNumber: number;
    pageSize: number;
}

export interface GetWonYchsResponse {
    cards: AuctionCardDto[];
    countOfPages: number;
}

export interface ImgBidDto {
    description: string;
    imgId: number;
    ownerName: string;
}

export interface ImgBidDtoBuilder {
}

export interface InputStreamSource {
    inputStream: any;
}

export interface IsAlreadyInFollowGroupQuery {
    artistName: string;
}

export interface IsAlreadyInFollowGroupResponse {
    isAlready: boolean;
}

export interface IsEmailFreeCommand {
    email: string;
}

export interface IsLoginFreeCommand {
    login: string;
}

export interface IsUserNameFreeCommand {
    userName: string;
}

export interface MultipartFile extends InputStreamSource {
    bytes: any;
    contentType: string;
    empty: boolean;
    name: string;
    originalFilename: string;
    resource: Resource;
    size: number;
}

export interface ReCaptchaResponse {
    challengeTs: string;
    hostname: string;
    success: boolean;
}

export interface Resource extends InputStreamSource {
    description: string;
    file: any;
    filename: string;
    open: boolean;
    readable: boolean;
    uri: URI;
    url: URL;
}

export interface Serializable {
}

export interface URI extends Comparable<URI>, Serializable {
}

export interface URL extends Serializable {
}

export interface VerifyCaptchaCommand {
    response: string;
}

export type AuctionKind = "YCH" | "ADOPT" | "ART_TRADE";

export type AuctionStatusToSearch = "WON" | "SOLD" | "ALL";

export type AuctionStep = "BIDDING" | "CONSULTATIONS" | "CLOSED";

export type BidCurrency = "USD" | "DEVIANT_ART_POINTS";

export type CommentType = "BIDDING" | "AUCTION_BIDDING_ENDED";

export type DateAsString = string;

export type Direction = "ASC" | "DESC";

export type ResponseMessage = "EMPTY" | "BID_ADDED" | "IMG_BID_ADDED" | "YCH_WAS_CREATED" | "COMMENT_ADDED" | "THE_AUCTION_WAS_BOUGHT_BUY_YOU" | "THE_AUCTION_WAS_CLOSED" | "WINNER_WAS_ADDED" | "PASSWORD_WAS_CHANGED" | "USERNAME_WAS_CHANGED" | "BEEB_BOOP_ARE_YOU_A_ROBOT" | "GOOD_JOB_YOU_ARE_NOT_A_ROBOT" | "ACCOUNT_WAS_CREATED_CHECK_YOUR_EMAIL" | "ACCOUNT_WAS_ACTIVATED" | "JWT_IS_VALID" | "ADDED_TO_YOUR_FOLLOW_LIST";

export type YchAuctionCharacterType = "PONY" | "FURRY" | "HUMAN" | "ALL";

export type YchAuctionType = "SAFE" | "EXPLICIT" | "NOT_SAFE_FOR_WORK" | "ALL";
