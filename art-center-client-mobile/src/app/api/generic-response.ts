export class GenericResponse<T> {
  public response: T;
  // public errors: ErrorInfo[];
  public success: boolean;
  public message: string;
}
