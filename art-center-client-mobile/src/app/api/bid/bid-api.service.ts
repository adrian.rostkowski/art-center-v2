import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import {
  AddBidToAuctionCommand,
  AddBidToAuctionResponse,
} from '../backend-model/backend-model';
import { Observable } from 'rxjs';
import { GenericResponse } from '../generic-response';

@Injectable({
  providedIn: 'root'
})
export class BidApiService {
  PREFIX = 'api/bid';

  constructor(private apiService: ApiService) {}

  add(cmd: AddBidToAuctionCommand): Observable<GenericResponse<AddBidToAuctionResponse>> {
    return this.apiService.post(
      this.PREFIX + '/add',
      cmd,
    );
  }
}
