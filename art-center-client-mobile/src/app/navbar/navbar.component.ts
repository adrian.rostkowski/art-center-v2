import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { UserContextService } from '../service/user-context.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  menuItem: MenuItem[];
  activeTab: MenuItem;
  userIsLoggedIn = false;
  readonly menuItemUrls: MenuItemUrl[] = [
    'auctions',
    'create-auction',
    'home',
    'sign-in',
    'sign-out',
    'account'
  ];

  constructor(
    private userContextService: UserContextService,
    private changeDetectionRef: ChangeDetectorRef,
    private router: Router,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.setUpMenuItems();
    this.userContextService.getIsUserLogged().subscribe(
      isUserLogin => {

        this.userIsLoggedIn = isUserLogin;
        this.setUpMenuItems();

        this.setActiveTab();
      }
    );

    this.location.onUrlChange(url => {
      if (this.menuItemUrls.every(menuItemUrl => !url.includes(menuItemUrl))) {
        this.setActiveTabByLabel('EMPTY');
      }
    });
  }

  setActiveTab(): void {
    const url = window.location.href;

    if (url.includes('auctions')) {
      this.setActiveTabByLabel('Auctions');
    } else if (url.includes('create-auction')) {
      this.setActiveTabByLabel('Create Auction');
    } else if (url.includes('home')) {
      this.setActiveTabByLabel('Home');
    } else if (url.includes('login')) {
      this.setActiveTabByLabel('Sign in');
    } else if (url.includes('logout')) {
      this.setActiveTabByLabel('Sign out');
    } else {
      this.activeTab = this.userIsLoggedIn ?
        this.menuItem.filter(n => n.label === 'Home')[0] :
        this.menuItem.filter(n => n.label === 'Sign in')[0];
      this.changeDetectionRef.detectChanges();
    }
  }

  private setUpMenuItems(): void {
    this.menuItem = [
      {
        label: 'Home',
        icon: 'pi pi-fw pi-home',
        command: () => this.navigate('/home', 'Home')
      },
      {
        label: 'Auctions',
        icon: 'pi pi-fw pi-th-large',
        command: () => this.navigate('/auctions', 'Auctions')
      },
      {
        label: 'Create Auction',
        icon: 'pi pi-fw pi-plus',
        visible: this.userIsLoggedIn,
        command: () => this.navigate('/create-auction', 'Create Auction')
      },
      {
        label: 'Create Account',
        icon: 'pi pi-fw pi-user-plus',
        visible: !this.userIsLoggedIn
      },
      {
        label: 'Account',
        icon: 'pi pi-fw pi-pencil',
        visible: this.userIsLoggedIn,
        command: () => this.navigate('/account', 'Account', '1')
      },
      {
        label: 'Sign in',
        icon: 'pi pi-fw pi-sign-in',
        visible: !this.userIsLoggedIn,
        command: () => this.navigate('/login', 'Sign in')
      },
      {
        label: 'Sign out',
        icon: 'pi pi-fw pi-sign-out',
        visible: this.userIsLoggedIn,
        command: () => this.navigate('/logout', 'Sign out')
      },
      {
        label: 'EMPTY',
        visible: false,
      },
    ];
  }

  private navigate(url: string, label: string, param: string = ''): void {
    if (this.activeTab?.label === label) {
      return;
    }

    if (param.length === 0) {
      this.router.navigate([url]);
    } else {
      this.router.navigate([url, param]);
    }

    this.setActiveTabByLabel(label);
  }

  private setActiveTabByLabel(label: string): void {
    this.activeTab = this.menuItem.filter(n => n.label === label)[0];
  }
}

type MenuItemUrl = 'auctions' |
  'create-auction' |
  'home' |
  'account' |
  'sign-in' |
  'sign-out';
