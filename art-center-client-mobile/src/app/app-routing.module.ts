import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./feature-modules/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'auctions',
    loadChildren: () => import('./feature-modules/auction-list/auctions.module').then(m => m.AuctionsModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./feature-modules/logout/logout.module').then(m => m.LogoutModule)
  },
  {
    path: 'create-auction',
    loadChildren: () => import('./feature-modules/auction-create/create-auction.module').then(m => m.CreateAuctionModule)
  },
  {
    path: 'details-auction/:auctionId',
    loadChildren: () => import('./feature-modules/auction-details/auction-details.module').then(m => m.AuctionDetailsModule)
  },
  {
    path: 'account/:userId',
    loadChildren: () => import('./feature-modules/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./feature-modules/home/home.module').then(m => m.HomeModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
