import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-editor-input',
  templateUrl: './editor-input.component.html',
  styleUrls: ['./editor-input.component.css']
})
export class EditorInputComponent implements OnInit {

  text: string;
  @Input() controlName = '';
  @Input() parentForm: FormGroup;
  @Input() label: string;

  constructor() { }

  ngOnInit(): void {
  }
}
