import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorInputComponent } from './editor-input/editor-input.component';
import { EditorModule } from 'primeng/editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    EditorInputComponent
  ],
  exports: [
    EditorInputComponent
  ],
  imports: [
    CommonModule,
    EditorModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class EditorInputModule { }
