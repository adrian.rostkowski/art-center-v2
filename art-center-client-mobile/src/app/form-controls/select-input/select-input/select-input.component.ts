import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.css']
})
export class SelectInputComponent {

  @Input() controlName = '';
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;
  @Input() selectOptions: SelectOption[];
  @Input() isPassword = false;

  getFormControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }

}

export interface SelectOption {
  name: string;
  code: string;
}
