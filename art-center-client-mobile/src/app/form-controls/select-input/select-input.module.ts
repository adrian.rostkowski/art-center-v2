import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectInputComponent } from './select-input/select-input.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorsModule } from '../control-errors/control-errors.module';



@NgModule({
  declarations: [
    SelectInputComponent
  ],
  exports: [
    SelectInputComponent
  ],
  imports: [
    CommonModule,
    SelectButtonModule,
    ReactiveFormsModule,
    ControlErrorsModule,
  ]
})
export class SelectInputModule { }
