import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileInputFormComponent } from './file-input/file-input-form.component';
import { FileInputFormModule } from '../../visual-components/file-input/file-input-form.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ControlErrorsModule } from '../control-errors/control-errors.module';
import { FileUploadModule } from 'primeng/fileupload';



@NgModule({
  declarations: [
    FileInputFormComponent
  ],
  exports: [
    FileInputFormComponent
  ],
  imports: [
    CommonModule,
    FileInputFormModule,
    ReactiveFormsModule,
    FormsModule,
    ControlErrorsModule,
    FileUploadModule,
  ]
})
export class FileInputModule { }
