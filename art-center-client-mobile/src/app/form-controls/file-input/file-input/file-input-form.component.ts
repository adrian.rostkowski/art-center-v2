import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-file-form-input',
  templateUrl: './file-input-form.component.html',
  styleUrls: ['./file-input-form.component.css']
})
export class FileInputFormComponent {

  @Input() controlName = '';
  @Input() parentForm: FormGroup;
  @Input() label: string;
  @Input() errorKeys: string[] = [];

  @Input() isMultiple = false;
  @Input() countOfImages = 1;

  files: File[] = [];

  onUpload(files: FileList): void {
    const file = files.item(0);
    this.parentForm.get(this.controlName).setValue(file);
  }

  getFormControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }

}
