import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderInputComponent } from './slider-input/slider-input.component';
import { SliderModule } from 'primeng/slider';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorsModule } from '../control-errors/control-errors.module';


@NgModule({
  declarations: [
    SliderInputComponent
  ],
  exports: [
    SliderInputComponent
  ],
  imports: [
    CommonModule,
    SliderModule,
    ReactiveFormsModule,
    ControlErrorsModule,
  ]
})
export class SliderInputModule {
}
