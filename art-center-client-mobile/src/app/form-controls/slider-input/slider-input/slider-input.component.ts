import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-slider-input',
  templateUrl: './slider-input.component.html',
  styleUrls: ['./slider-input.component.css']
})
export class SliderInputComponent {

  @Input() controlName = '';
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;

  @Input() min: number;
  @Input() max: number;
  @Input() step: number;
  @Input() label: string;

  getFormControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }
}
