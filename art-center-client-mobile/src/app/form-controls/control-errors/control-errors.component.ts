import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-control-errors',
  templateUrl: './control-errors.component.html',
  styleUrls: ['./control-errors.component.css']
})
export class ControlErrorsComponent {

  @Input() control: FormControl;
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;

  buildParameters(errors: ValidationErrors): any {
    const translateParams = {};
    Object.values(errors).forEach((n) => {
      Object.values(n).forEach((m, index) => {
        Object.defineProperty(translateParams, index, {
          value: m,
          writable: false
        });
      });
    });
    return translateParams;
  }
}
