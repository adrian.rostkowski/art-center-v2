import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwitchInputComponent } from './switch-input/switch-input.component';
import { ControlErrorsModule } from '../control-errors/control-errors.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    SwitchInputComponent
  ],
  exports: [
    SwitchInputComponent
  ],
  imports: [
    CommonModule,
    ControlErrorsModule,
    InputSwitchModule,
    ReactiveFormsModule,
  ]
})
export class SwitchInputModule { }
