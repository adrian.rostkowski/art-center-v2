import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-switch-input',
  templateUrl: './switch-input.component.html',
  styleUrls: ['./switch-input.component.css']
})
export class SwitchInputComponent {

  @Input() controlName = '';
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;
  @Input() labelValue = '';
  @Input() isPassword = false;

  getFormControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }

}
