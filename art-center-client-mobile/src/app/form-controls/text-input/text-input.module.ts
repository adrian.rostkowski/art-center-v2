import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextInputComponent } from './text-input.component';
import {ControlErrorsModule} from '../control-errors/control-errors.module';
import {ReactiveFormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';



@NgModule({
  declarations: [
    TextInputComponent
  ],
  imports: [
    CommonModule,
    ControlErrorsModule,
    ReactiveFormsModule,
    InputTextModule,
  ],
  exports: [
    TextInputComponent
  ]
})
export class TextInputModule { }
