import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})
export class TextInputComponent {

  @Input() controlName = '';
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;
  @Input() labelValue = '';
  @Input() isPassword = false;

  getFormControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }
}
