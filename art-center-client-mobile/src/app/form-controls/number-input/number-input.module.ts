import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberInputComponent } from './number-input/number-input.component';
import { ControlErrorsModule } from '../control-errors/control-errors.module';
import { ReactiveFormsModule } from '@angular/forms';
import { InputNumberModule } from 'primeng/inputnumber';



@NgModule({
  declarations: [
    NumberInputComponent
  ],
  exports: [
    NumberInputComponent
  ],
  imports: [
    CommonModule,
    ControlErrorsModule,
    ReactiveFormsModule,
    InputNumberModule,
  ]
})
export class NumberInputModule { }
