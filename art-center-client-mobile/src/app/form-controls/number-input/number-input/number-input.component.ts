import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.css']
})
export class NumberInputComponent {

  @Input() controlName = '';
  @Input() errorKeys: string[] = [];
  @Input() parentForm: FormGroup;
  @Input() labelValue = '';
  @Input() isPassword = false;

  getFormControl(): FormControl {
    return this.parentForm.get(this.controlName) as FormControl;
  }

}
