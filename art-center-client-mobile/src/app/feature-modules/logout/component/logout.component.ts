import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../../service/local-storage.service';
import { UserContextService } from '../../../service/user-context.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private localStorageService: LocalStorageService,
    private userContextService: UserContextService,
    private route: Router,
  ) { }

  ngOnInit(): void {
  }

  logoutClick(): void {
        this.localStorageService.removeAuthorizationData();
        this.userContextService.getIsUserLogged().next(false);
        this.userContextService.getUserName().next('');

        this.route.navigate(['/login']);
  }

}
