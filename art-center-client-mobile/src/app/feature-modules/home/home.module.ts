import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { WelcomeTextComponent } from './welcome-text/welcome-text.component';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  declarations: [
    HomeComponent,
    WelcomeTextComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    InputTextModule
  ]
})
export class HomeModule { }
