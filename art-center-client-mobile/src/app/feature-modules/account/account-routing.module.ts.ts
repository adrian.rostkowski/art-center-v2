import { RouterModule, Routes } from '@angular/router';
import { AuctionDetailsComponent } from '../auction-details/acution-details/auction-details.component';
import { DescriptionComponent } from '../auction-details/acution-details/tabs/description/description.component';
import { BidsComponent } from '../auction-details/acution-details/bids/bids.component';
import { DetailsComponent } from '../auction-details/acution-details/tabs/details/details.component';
import { NgModule } from '@angular/core';
import { AccountComponent } from './account/account.component';
import { AboutMeComponent } from './account/tabs/about-me/about-me.component';
import { OtherSocialsComponent } from './account/tabs/other-socials/other-socials.component';

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      {
        path: 'tab',
        children: [
          {
            path: 'about-me',
            component: AboutMeComponent,
          },
          {
            path: 'other-socials',
            component: OtherSocialsComponent,
          },
          {
            path: 'details',
            component: DetailsComponent,
          }
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {
}
