import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account/account.component';
import { AccountRoutingModule } from './account-routing.module.ts';
import { ActionButtonModule } from '../../visual-components/action-button/action-button.module';
import { TabMenuModule } from 'primeng/tabmenu';
import { AboutMeComponent } from './account/tabs/about-me/about-me.component';
import { OtherSocialsComponent } from './account/tabs/other-socials/other-socials.component';
import { WavesAnimationModule } from '../../visual-components/waves-animation/waves-animation.module';


@NgModule({
  declarations: [
    AccountComponent,
    AboutMeComponent,
    OtherSocialsComponent
  ],
    imports: [
        CommonModule,
        AccountRoutingModule,
        ActionButtonModule,
        TabMenuModule,
        WavesAnimationModule
    ]
})
export class AccountModule { }
