import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherSocialsComponent } from './other-socials.component';

describe('OtherSocialsComponent', () => {
  let component: OtherSocialsComponent;
  let fixture: ComponentFixture<OtherSocialsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherSocialsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherSocialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
