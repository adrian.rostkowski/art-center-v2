import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  menuItem: MenuItem[];
  activeMenuItem: MenuItem;

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.setUpMenuItems();
    this.setActiveTab();
    this.navigate('tab/about-me');
  }

  private setUpMenuItems(): void {
    this.menuItem = [
      {
        label: 'About me',
        icon: 'pi pi-fw pi-user',
        command: () => this.navigate('tab/about-me')
      },
      {
        label: 'Other socials',
        icon: 'pi pi-fw pi-arrow-up',
        command: () => this.navigate('tab/other-socials')
      },
    ];

    this.setActiveItem();
  }

  setActiveTab(): void {
    const url = window.location.href;

    if (url.includes('tab/about-me')) {
      this.setActiveTabByLabel('About me');
    } else if (url.includes('tab/other-socials')) {
      this.setActiveTabByLabel('Other socials');
    }
  }

  private navigate(url: string): void {
    this.router.navigate([url], { relativeTo: this.activateRoute });
  }

  private setActiveTabByLabel(label: string): void {
    this.activeMenuItem = this.menuItem.filter(n => n.label === label)[0];
  }

  private setActiveItem(): void {
    this.activeMenuItem = this.menuItem[0];
  }

}
