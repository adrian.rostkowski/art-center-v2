import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginApiService } from '../api/login-api.service';
import { LocalStorageService } from '../../../service/local-storage.service';
import { UserContextService } from '../../../service/user-context.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginApiService,
    private localStorageService: LocalStorageService,
    private userContextService: UserContextService,
    private route: Router,
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  loginClick(): void {
    this.formGroup.markAsTouched();
    this.formGroup.markAsDirty();

    const login = this.formGroup.get('login').value;
    const password = this.formGroup.get('password').value;

    this.loginService.login(login, password).subscribe(
      response => {
        // this.localStorageService.setAuthorizationData(response.response.jwt);
        // this.userContextService.getIsUserLogged().next(true);
        // this.userContextService.getUserName().next(response.response.userName);

        this.route.navigate(['/auctions']);
      }
    );
  }

  private initForm(): void {
    this.formGroup = this.fb.group({
      login: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
}
