import { Injectable } from '@angular/core';
import { ApiService } from '../../../api/api.service';
import { Observable } from 'rxjs';
import { GenericResponse } from '../../../api/generic-response';
import { AuthenticationCommandResponse } from './model/authentication-command-response';
import { AuthCommand } from './model/auth-command';

@Injectable({
  providedIn: 'root'
})
export class LoginApiService {

  constructor(
    private api: ApiService
  ) {
  }

  public login(username: string, password: string): Observable<GenericResponse<AuthenticationCommandResponse>> {
    const authRequest: AuthCommand = {username, password};
    return this.api.post<GenericResponse<AuthenticationCommandResponse>>(
      'api/login',
      authRequest,
    );
  }

  public checkAuth(): Observable<GenericResponse<string>> {
    return this.api.get<GenericResponse<string>>('api/user/check-authentication');
  }
}
