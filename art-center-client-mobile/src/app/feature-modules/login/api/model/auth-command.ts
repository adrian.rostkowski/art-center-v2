export interface AuthCommand {
  username: string;
  password: string;
}
