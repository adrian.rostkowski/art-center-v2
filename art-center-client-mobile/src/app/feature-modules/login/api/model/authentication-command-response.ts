export interface AuthenticationCommandResponse {
  userName: string;
  jwt: string;
}
