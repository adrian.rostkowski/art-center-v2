import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuctionsRoutingModule } from './auctions-routing.module';
import { AuctionsComponent } from './component/auctions.component';
import { SingleAuctionBoxComponent } from './component/single-auction-box/single-auction-box.component';
import { AuctionBoxTagComponent } from './component/single-auction-box/auction-box-tag/auction-box-tag.component';
import { DataViewModule } from 'primeng/dataview';
import { AuctionFilterModule } from '../../visual-components/auction-filter/auction-filter.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '../../form-controls/control-errors/control-errors.module';


@NgModule({
  declarations: [AuctionsComponent, SingleAuctionBoxComponent, AuctionBoxTagComponent],
  imports: [
    CommonModule,
    AuctionsRoutingModule,
    DataViewModule,
    AuctionFilterModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ]
})
export class AuctionsModule { }
