import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuctionCardDto } from '../../../../api/backend-model/backend-model';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-single-auction-box',
  templateUrl: './single-auction-box.component.html',
  styleUrls: ['./single-auction-box.component.css']
})
export class SingleAuctionBoxComponent implements OnInit {

  @Input() auctionCardDto: AuctionCardDto;
  @Output() click = new EventEmitter<void>();

  serverUrl = 'http://localhost:8080';

  constructor() { }

  ngOnInit(): void {
  }

}
