import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-auction-box-tag',
  templateUrl: './auction-box-tag.component.html',
  styleUrls: ['./auction-box-tag.component.css']
})
export class AuctionBoxTagComponent {
  @Input() days: number;
  @Input() price: number;
  @Input() currency: string;
}
