import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuctionApiService } from '../../../api/auction/auction-api.service';
import { AuctionCardDto, GetAuctionsListCommand } from '../../../api/backend-model/backend-model';

@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit {

  auctionCardDto: AuctionCardDto[] = [];

  currentPageNumber = 0;
  totalRecords = 0;
  pageSize = 5;

  constructor(
    private router: Router,
    private auctionApiService: AuctionApiService
  ) { }

  ngOnInit(): void {
  }

  searchAuctionCards(event): void {
    this.currentPageNumber = event.first;

    const cmd: GetAuctionsListCommand = {
      auctionKind: 'YCH',
      auctionStep: 'BIDDING',
      daysToEnd: 10,
      findByCurrentUser: false,
      orderBy: ['endDate'],
      orderDirection: 'ASC',
      pageNumber: (this.currentPageNumber / this.pageSize),
      pageSize: this.pageSize,
      priceRangeFrom: null,
      priceRangeTo: null,
      userName: null,
      ychCharacterType: 'ALL',
      ychStatus: 'ALL',
      ychType: 'ALL',
    };

    this.auctionApiService.getAuctionsList(cmd)
      .subscribe(
        (response) => {

          this.auctionCardDto = response.cards;
          this.totalRecords = response.totalRecords;
        }
      );
  }

  navigateToDetails(auctionId: number): void {
    this.router.navigate(['/details-auction', auctionId]);
  }

}
