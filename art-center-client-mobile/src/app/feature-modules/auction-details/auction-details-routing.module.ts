import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuctionDetailsComponent } from './acution-details/auction-details.component';
import { DescriptionComponent } from './acution-details/tabs/description/description.component';
import { BidsComponent } from './acution-details/bids/bids.component';
import { DetailsComponent } from './acution-details/tabs/details/details.component';
import { ConsultationComponent } from './acution-details/tabs/consultation/consultation.component';

const routes: Routes = [
  {
    path: '',
    component: AuctionDetailsComponent,
    children: [
      {
        path: 'tab',
        children: [
          {
            path: 'consultation',
            component: ConsultationComponent,
          },
          {
            path: 'description',
            component: DescriptionComponent,
          },
          {
            path: 'bids',
            component: BidsComponent,
          },
          {
            path: 'details',
            component: DetailsComponent,
          }
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuctionDetailsRoutingModule {
}
