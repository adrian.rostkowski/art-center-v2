import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuctionDetailsComponent } from './acution-details/auction-details.component';
import { InfoCardModule } from '../../visual-components/info-card/info-card.module';
import { DividerModule } from 'primeng/divider';
import { TabMenuModule } from 'primeng/tabmenu';
import { DetailsComponent } from './acution-details/tabs/details/details.component';
import { DescriptionComponent } from './acution-details/tabs/description/description.component';
import { BidsComponent } from './acution-details/bids/bids.component';
import { BidComponent } from './acution-details/bids/bid/bid.component';
import { ActionButtonModule } from '../../visual-components/action-button/action-button.module';
import { AuctionDetailsRoutingModule } from './auction-details-routing.module';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { AddBidDialogComponent } from './acution-details/add-bid-dialog/add-bid-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NumberInputModule } from '../../form-controls/number-input/number-input.module';
import { TranslateModule } from '@ngx-translate/core';
import { ConsultationComponent } from './acution-details/tabs/consultation/consultation.component';
import { TextInputModule } from '../../form-controls/text-input/text-input.module';
import { EditorInputModule } from '../../form-controls/editor-input/editor-input.module';

@NgModule({
  declarations: [
    AuctionDetailsComponent,
    DetailsComponent,
    DescriptionComponent,
    BidsComponent,
    BidComponent,
    AddBidDialogComponent,
    ConsultationComponent
  ],
  imports: [
    CommonModule,
    AuctionDetailsRoutingModule,
    InfoCardModule,
    DividerModule,
    TabMenuModule,
    ActionButtonModule,
    DynamicDialogModule,
    ReactiveFormsModule,
    NumberInputModule,
    TranslateModule,
    TextInputModule,
    EditorInputModule
  ]
})

export class AuctionDetailsModule {
}
