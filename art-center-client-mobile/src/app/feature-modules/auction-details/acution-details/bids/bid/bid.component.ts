import { Component, Input } from '@angular/core';
import { BidDto } from '../../../../../api/backend-model/backend-model';

@Component({
  selector: 'app-bid',
  templateUrl: './bid.component.html',
  styleUrls: ['./bid.component.css']
})
export class BidComponent {

  @Input() bid: BidDto;
  @Input() numberOf: number;
}
