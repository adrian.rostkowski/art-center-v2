import { Component, OnInit } from '@angular/core';
import { BidDto } from '../../../../api/backend-model/backend-model';
import { SelectedAuctionService } from '../tabs/selected-auction-state/selected-auction.service';

@Component({
  selector: 'app-bids',
  templateUrl: './bids.component.html',
  styleUrls: ['./bids.component.css']
})
export class BidsComponent implements OnInit {

  bids: BidDto[] = [];
  minBid: number;

  constructor(private selectedAuctionState: SelectedAuctionService) {
  }

  ngOnInit(): void {
    this.fetchStateData();
  }

  private fetchStateData(): void {
    this.selectedAuctionState.$bidsSubject.subscribe((bids) => {
      this.bids = bids;
    });
    this.minBid = history.state.minBid;
  }
}
