import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DialogResult } from '../../../../visual-components/dialog/dialog-result';

@Component({
  selector: 'app-add-bid-dialog',
  templateUrl: './add-bid-dialog.component.html',
  styleUrls: ['./add-bid-dialog.component.css']
})
export class AddBidDialogComponent implements OnInit {

  private highestBidValue: number;
  formGroup: FormGroup;

  constructor(
    private ref: DynamicDialogRef,
    private formBuilder: FormBuilder,
    private config: DynamicDialogConfig
  ) { }

  ngOnInit(): void {
    this.highestBidValue = this.config.data.maxBidValue;
    this.formGroup = this.formBuilder.group({
      newBid: [null, [Validators.required, this.mustBeHigherThenMaxBid(this.highestBidValue)]],
    });
  }

  closeDialog(result: DialogResult): void {
    this.ref.close(result);
  }

  mustBeHigherThenMaxBid(higherBid: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.value <= higherBid ?
        { newBidIsLowerThenHigher: { highest: this.highestBidValue } } :
        null;
    };
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
      this.formGroup.markAsDirty();
      return;
    }

    const result: DialogResult = {
      success: true,
      body: this.formGroup.get('newBid').value
    };

    this.closeDialog(result);
  }

  onCancel(): void {
    const result: DialogResult = {
      success: false,
      body: null
    };

    this.closeDialog(result);
  }
}
