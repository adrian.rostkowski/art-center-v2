import { Component, OnInit } from '@angular/core';
import { SelectedAuctionService } from '../selected-auction-state/selected-auction.service';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {

  description: string;

  constructor(
    private selectedAuctionService: SelectedAuctionService
  ) { }

  ngOnInit(): void {
    this.description = this.selectedAuctionService.getDescription();
  }
}
