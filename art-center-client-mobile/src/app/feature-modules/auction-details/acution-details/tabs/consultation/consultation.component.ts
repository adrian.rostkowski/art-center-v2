import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.css']
})
export class ConsultationComponent implements OnInit {

  private formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
  }

  initForm(): void {
    this.formGroup = this.fb.group({
      // price: this.createPriceStep(),
      // art: this.createArtStep(),
      // details: this.createDetailsStep(),
    });
  }

}
