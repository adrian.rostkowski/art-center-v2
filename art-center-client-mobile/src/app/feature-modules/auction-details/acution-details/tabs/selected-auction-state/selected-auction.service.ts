import { Injectable } from '@angular/core';
import { AuctionDto, BidDto } from '../../../../../api/backend-model/backend-model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SelectedAuctionService {

  private selectedAuctionState: AuctionDto;
  $bidsSubject = new BehaviorSubject<BidDto[]>([]);

  set setSelectedAuctionState(selectedAuctionState: AuctionDto) {
    this.selectedAuctionState = selectedAuctionState;
  }

  getDescription(): string {
    return this.selectedAuctionState.description;
  }

  // getBids(): BidDto[] {
  //   return this.selectedAuctionState.bids;
  // }

  // setBids(bids: BidDto[]): void {
  //   this.selectedAuctionState.bids = bids;
  // }
}
