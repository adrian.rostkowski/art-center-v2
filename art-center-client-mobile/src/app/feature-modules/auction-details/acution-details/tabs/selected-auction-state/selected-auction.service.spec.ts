import { TestBed } from '@angular/core/testing';

import { SelectedAuctionService } from './selected-auction.service';

describe('SelectedAuctionService', () => {
  let service: SelectedAuctionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SelectedAuctionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
