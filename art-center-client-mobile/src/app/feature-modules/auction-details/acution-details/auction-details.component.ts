import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuctionApiService } from '../../../api/auction/auction-api.service';
import { switchMap } from 'rxjs/operators';
import { AuctionDto } from '../../../api/backend-model/backend-model';
import { SelectedAuctionService } from './tabs/selected-auction-state/selected-auction.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AddBidDialogComponent } from './add-bid-dialog/add-bid-dialog.component';
import { DialogResult } from '../../../visual-components/dialog/dialog-result';
import { environment } from '../../../../environments/environment';
import { BidApiService } from '../../../api/bid/bid-api.service';

@Component({
  selector: 'app-acution-details',
  templateUrl: './auction-details.component.html',
  styleUrls: ['./auction-details.component.css'],
  providers: [DialogService]
})
export class AuctionDetailsComponent implements OnInit {

  auctionId: number;
  auctionDto: AuctionDto;

  auctionDetailsTabs: MenuItem[];
  activeDetailsTab: MenuItem;
  serverUrl: string;

  highestBidValue: number;

  constructor(
    private api: AuctionApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private selectedAuctionState: SelectedAuctionService,
    private dialogService: DialogService,
    private bidApiService: BidApiService,
  ) {
  }

  ngOnInit(): void {
    this.serverUrl = environment.serverUrl;
    debugger;
    this.activatedRoute.paramMap.pipe(
      switchMap((params) => {
        debugger;
          this.auctionId = params.get('auctionId') as any as number;
          return this.api.getById(this.auctionId);
        }
      )
    ).subscribe((auctionDto) => {
      debugger;
      this.auctionDto = auctionDto;
      this.selectedAuctionState.setSelectedAuctionState = auctionDto;
      this.selectedAuctionState.$bidsSubject.next(auctionDto.bids);

      this.setUpMenuItems();
      this.setActiveTab();
      this.setHighestBidValue();

      this.activeFirstTab();
    });
  }

  openAddBidDialog(): void {
    const ref = this.dialogService.open(AddBidDialogComponent, {
      data: {
        maxBidValue: this.highestBidValue
      },
      showHeader: false,
      style: {width: '300px'},
    });

    ref.onClose.subscribe((result: DialogResult) => {
      if (result.success) {
        this.bidApiService.add({ auctionId: this.auctionDto.ychAuctionId, bidValue: result.body }).subscribe(
          (response) => {
            if (response.response.added) {
              this.selectedAuctionState.$bidsSubject.next(response.response.bids);
            }
          }
        );
      }
    });
  }

  setActiveTab(): void {
    const url = window.location.href;

    if (url.includes('auctions')) {
      this.setActiveTabByLabel('Auctions');
    } else if (url.includes('create-auction')) {
      this.setActiveTabByLabel('Create Auction');
    } else if (url.includes('login')) {
      this.setActiveTabByLabel('Sign in');
    } else if (url.includes('logout')) {
      this.setActiveTabByLabel('Sign out');
    }
  }

  isBiddingStep(): boolean {
    return this.auctionDto.step === 'BIDDING';
  }

  private setUpMenuItems(): void {
    this.auctionDetailsTabs = [
      {
        label: 'Consultation',
        icon: 'pi pi-fw pi-comment',
        command: () => this.navigateToConsultation(),
        visible: this.auctionDto.step === 'CONSULTATIONS'
      },
      {
        label: 'Description',
        icon: 'pi pi-fw pi-file',
        command: () => this.navigateToDescription(),
        visible: !!this.auctionDto.description
      },
      {
        label: 'Bids',
        icon: 'pi pi-fw pi-bars',
        command: () => this.navigateToBids(),
        // visible: !!this.auctionDto.bids
      },
      {
        label: 'Details',
        icon: 'pi pi-fw pi-search',
        command: () => this.navigateToDetails()
      },
    ];

    this.setActiveItem();
  }

  private navigateToDescription(): void {
    this.router.navigate(
      ['tab/description'],
      {
        state: {description: this.auctionDto.description},
        relativeTo: this.activateRoute
      }
    );
  }

  private navigateToConsultation(): void {
    this.router.navigate(
      ['tab/consultation'],
      {
        relativeTo: this.activateRoute
      }
    );
  }

  private navigateToBids(): void {
    this.router.navigate(
      ['tab/bids'],
      {
        relativeTo: this.activateRoute,
        state: {bids: this.auctionDto.bids, minBid: this.auctionDto.minBid}
      }
    );
  }

  private navigateToDetails(): void {
    this.router.navigate(
      ['tab/details'],
      {
        relativeTo: this.activateRoute,
        // TODO pass any object with details values
        state: {details: ''}
      }
    );
  }

  private setActiveItem(): void {
    this.activeDetailsTab = this.auctionDetailsTabs[0];
  }

  private setActiveTabByLabel(label: string): void {
    this.activeDetailsTab = this.auctionDetailsTabs.filter(n => n.label === label)[0];
  }

  private setHighestBidValue(): void {
    this.highestBidValue = Math.max(...this.auctionDto.bids.map(n => n.bidValue));
  }

  private activeFirstTab(): void {
    this.auctionDto.step === 'BIDDING' ?
      this.navigateToDescription() :
      this.navigateToConsultation();
  }
}
