import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumComponentComponent } from './premium-component/premium-component.component';



@NgModule({
  declarations: [
    PremiumComponentComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PremiumModuleModule { }
