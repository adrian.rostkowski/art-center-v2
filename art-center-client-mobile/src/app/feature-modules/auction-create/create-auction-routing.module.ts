import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAuctionComponent } from './component/create-auction.component';
import { YCHComponent } from './component/create-auction-forms/ych/ych.component';
import { PriceStepComponent } from './component/create-auction-forms/steps/price-step/price-step.component';
import { ArtStepComponent } from './component/create-auction-forms/steps/art-step/art-step.component';
import { DetailsStepComponent } from './component/create-auction-forms/steps/details-step/details-step.component';

// little explanation about how <router-outlet></router-outlet> works
// right now we are in the children component (look 0.)
// soo every think below is show in first child router-outlet
// which is in create-auction.component.html, the first router-outlet in this child!
// but we have new children in this child (look 1.)
// soo every child will be show in next nested router-outlet, which is in ych.component.html
const routes: Routes = [
  {
    path: '',
    component: CreateAuctionComponent,
    // 0.
    children: [
      {
        path: 'ych', // child route path
        component: YCHComponent, // child route component that the router renders
        // 1.
        children: [
          {
            path: 'price-step', // child route path
            component: PriceStepComponent, // child route component that the router renders
          },
          {
            path: 'art-step', // child route path
            component: ArtStepComponent, // child route component that the router renders
          },
          {
            path: 'details-step', // child route path
            component: DetailsStepComponent, // child route component that the router renders
          },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateAuctionRoutingModule {
}
