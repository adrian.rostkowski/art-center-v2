import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateAuctionRoutingModule } from './create-auction-routing.module';
import { CreateAuctionComponent } from './component/create-auction.component';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { YCHComponent } from './component/create-auction-forms/ych/ych.component';
import { SelectAuctionComponent } from './component/select-auction/select-auction.component';
import { StepsModule } from 'primeng/steps';
import { PriceStepComponent } from './component/create-auction-forms/steps/price-step/price-step.component';
import { ArtStepComponent } from './component/create-auction-forms/steps/art-step/art-step.component';
import { DetailsStepComponent } from './component/create-auction-forms/steps/details-step/details-step.component';
import { NumberInputModule } from '../../form-controls/number-input/number-input.module';
import { DividerModule } from 'primeng/divider';
import { SwitchInputModule } from '../../form-controls/switch-input/switch-input.module';
import { SelectInputModule } from '../../form-controls/select-input/select-input.module';
import { NavigationButtonsComponent } from './component/navigation-buttons/navigation-buttons.component';
import { EditorInputModule } from '../../form-controls/editor-input/editor-input.module';
import { SliderInputModule } from '../../form-controls/slider-input/slider-input.module';
import { FileInputModule } from '../../form-controls/file-input/file-input.module';
import { ActionButtonModule } from '../../visual-components/action-button/action-button.module';
import { CreateAuctionButtonComponent } from './component/create-auction-forms/create-auction-button/create-auction-button.component';


@NgModule({
  declarations: [
    CreateAuctionComponent,
    YCHComponent,
    SelectAuctionComponent,
    PriceStepComponent,
    ArtStepComponent,
    DetailsStepComponent,
    NavigationButtonsComponent,
    CreateAuctionButtonComponent
  ],
    imports: [
        CommonModule,
        CreateAuctionRoutingModule,
        ButtonModule,
        RippleModule,
        StepsModule,
        NumberInputModule,
        DividerModule,
        SwitchInputModule,
        SelectInputModule,
        EditorInputModule,
        SliderInputModule,
        FileInputModule,
        FileInputModule,
        ActionButtonModule,
    ]
})
export class CreateAuctionModule {
}
