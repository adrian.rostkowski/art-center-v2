import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Step, Steps } from './create-auction-forms/steps/steps';
import { StepsCommunicationServiceService } from './create-auction-forms/steps/steps-communication-service.service';

@Component({
  selector: 'app-create-auction',
  templateUrl: './create-auction.component.html',
  styleUrls: ['./create-auction.component.css']
})
export class CreateAuctionComponent implements OnInit {

  steps = new Steps();
  currentStep: Step;
  isAnyAuctionSelected = false;

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private stepsCommunicationServiceService: StepsCommunicationServiceService,

    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.isAnyAuctionSelected =
      this.router.url.includes('ych') ||
      this.router.url.includes('art-trade') ||
      this.router.url.includes('adopt');

    if (this.isAnyAuctionSelected) {
      this.setCurrentStep();
    }
  }

  navigateToYchCreate(): void {
    this.router.navigate(['ych/price-step'], { relativeTo: this.activateRoute });
    this.setIsAnyAuctionSelected(true);
    this.currentStep = this.steps.findByName('PRICE');
  }

  navigateByStepNumber(numberOf: number): void {
    const newStep = this.steps.findByNumber(numberOf);

    /*
     { relativeTo: this.activateRoute } is not really necessary
     this only append parent part of url, for example
     right now we are in 'create-auction/' where
     'this is parent part -> create-auction/ this is child part ->'
     soo this.router.navigate('ych/art-step', { relativeTo: this.activateRoute })
     really means this.router.navigate('ych/art-step', 'create-auction') = 'create-auction/ych/art-step'
     */

    this.router.navigate([newStep.pathToView], { relativeTo: this.activateRoute });

    this.setIsAnyAuctionSelected(true);
    this.currentStep = newStep;
  }

  setIsAnyAuctionSelected(isSelected: boolean): void {
    this.isAnyAuctionSelected = isSelected;
  }

  onGoToNext(): void {
    const numberOfNextStep = this.currentStep.numberOf + 1;

    if (!this.isFormValidBeforeSwitchStep() || !this.steps.isNextStepAvailable(numberOfNextStep)) {
      return;
    }

    this.navigateByStepNumber(numberOfNextStep);
  }

  onGoToPrevious(): void {
    const numberOfNextStep = this.currentStep.numberOf - 1;

    if (!this.isFormValidBeforeSwitchStep() || !this.steps.isPreviousStepAvailable(numberOfNextStep)) {
      return;
    }

    this.navigateByStepNumber(numberOfNextStep);
  }

  private isFormValidBeforeSwitchStep(): boolean {
    switch (this.currentStep.name) {
      case 'PRICE': {
        this.stepsCommunicationServiceService.priceFormGroup.getValue().markAllAsTouched();

        return this.stepsCommunicationServiceService.priceFormGroup.getValue().valid;
      }
      case 'ART': {
        this.stepsCommunicationServiceService.artFormGroup.getValue().markAllAsTouched();

        return this.stepsCommunicationServiceService.artFormGroup.getValue().valid;
      }
      case 'DETAILS': {
        this.stepsCommunicationServiceService.detailsFormGroup.getValue().markAllAsTouched();

        return this.stepsCommunicationServiceService.detailsFormGroup.getValue().valid;
      }
      default: console.error(`Can not find step name equals to ${this.currentStep.name}`);
    }
  }

  private setCurrentStep(): void {
    const url = this.router.url;
    if (url.includes(this.steps.findByName('PRICE').pathToView)) {
      this.currentStep = this.steps.findByName('PRICE');
    } else if (url.includes(this.steps.findByName('ART').pathToView)) {
      this.currentStep = this.steps.findByName('ART');
    } else if (url.includes(this.steps.findByName('DETAILS').pathToView)) {
      this.currentStep = this.steps.findByName('DETAILS');
    } else {
      console.error('Can not find step by current url');
    }
  }

}
