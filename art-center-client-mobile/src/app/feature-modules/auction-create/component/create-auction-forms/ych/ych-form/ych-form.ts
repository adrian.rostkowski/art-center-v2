import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StepsCommunicationServiceService } from '../../steps/steps-communication-service.service';

export class YchForm {

  private formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private stepsCommunicationServiceService: StepsCommunicationServiceService,
  ) { }

  get controls(): ControlsYchForm {
    return {
      minBid: this.formGroup.get('price.minBid') as FormControl,
      isMaxBid: this.formGroup.get('price.isMaxBid') as FormControl,
      maxBid: this.formGroup.get('price.maxBid') as FormControl,
      currency: this.formGroup.get('price.currency') as FormControl,
      characterType: this.formGroup.get('art.characterType') as FormControl,
      auctionType: this.formGroup.get('art.auctionType') as FormControl,
      images: this.formGroup.get('art.images') as FormControl,
      editor: this.formGroup.get('details.editor') as FormControl,
      countOfDays: this.formGroup.get('details.countOfDays') as FormControl,
    };
  }

  // get formSteps(): YchFormSteps {
  //   return {
  //     price: this.formGroup.get('price') as FormGroup,
  //     art: this.formGroup.get('art') as FormGroup,
  //     details: this.formGroup.get('details') as FormGroup
  //   };
  // }

  initForm(): void {
    this.formGroup = this.fb.group({
      price: this.createPriceStep(),
      art: this.createArtStep(),
      details: this.createDetailsStep(),
    });

    this.stepsCommunicationServiceService.priceFormGroup.next(
      this.formGroup.get('price') as FormGroup
    );

    this.stepsCommunicationServiceService.artFormGroup.next(
      this.formGroup.get('art') as FormGroup
    );

    this.stepsCommunicationServiceService.detailsFormGroup.next(
      this.formGroup.get('details') as FormGroup
    );

    this.formGroup.get('price').get('isMaxBid').valueChanges.subscribe(
      result => result ?
        this.formGroup.get('price').get('maxBid').enable({ onlySelf: true, emitEvent: false }) :
        this.formGroup.get('price').get('maxBid').disable({ onlySelf: true, emitEvent: false })
    );
  }

  createPriceStep(): FormGroup {
    return this.fb.group({
      minBid: [null, [Validators.required]],
      isMaxBid: [true, [Validators.required]],
      maxBid: [null, [Validators.required]],
      currency: [null, [Validators.required]],
    });
  }

  createArtStep(): FormGroup {
    return this.fb.group({
      characterType: [null, [Validators.required]],
      auctionType: [null, [Validators.required]],
      images: [null, [Validators.required]],
    });
  }

  createDetailsStep(): FormGroup {
    return this.fb.group({
      editor: [null, []],
      countOfDays: [null, [Validators.required, Validators.min(1)]],
    });
  }

  isValid(): boolean {
    return this.formGroup.valid && this.formGroup.touched;
  }

  getForm(): FormGroup {
    return this.formGroup;
  }
}

export interface ControlsYchForm {
  minBid: FormControl;
  isMaxBid: FormControl;
  maxBid: FormControl;
  currency: FormControl;
  characterType: FormControl;
  auctionType: FormControl;
  images: FormControl;
  editor: FormControl;
  countOfDays: FormControl;
}
