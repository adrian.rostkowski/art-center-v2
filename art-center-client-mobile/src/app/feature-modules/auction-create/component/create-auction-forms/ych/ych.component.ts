import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { YchForm } from './ych-form/ych-form';
import { FormBuilder } from '@angular/forms';
import { StepsCommunicationServiceService } from '../steps/steps-communication-service.service';
import { AuctionApiService } from '../../../../../api/auction/auction-api.service';
import { CreateAuctionCommand } from '../../../../../api/backend-model/backend-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ych',
  templateUrl: './ych.component.html',
  styleUrls: ['./ych.component.css']
})
export class YCHComponent implements OnInit {

  items: MenuItem[];
  ychForm: YchForm;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private stepsCommunicationServiceService: StepsCommunicationServiceService,
    private auctionApiService: AuctionApiService
  ) {
  }

  ngOnInit(): void {
    this.ychForm = new YchForm(this.fb, this.stepsCommunicationServiceService);
    this.ychForm.initForm();

    this.setupStepper();
  }

   create(): void {
    const cmd: CreateAuctionCommand = {
      auctionKind: 'YCH',
      countOfDays: this.ychForm.controls.countOfDays.value,
      currency: this.ychForm.controls.currency.value.code.toUpperCase(),
      description: this.ychForm.controls.editor.value,
      isMaxBid: this.ychForm.controls.isMaxBid.value,
      maxBid: this.ychForm.controls.maxBid.value,
      minBid: this.ychForm.controls.minBid.value,
      ychAuctionCharacterType: this.ychForm.controls.characterType.value.code.toUpperCase(),
      ychAuctionType: this.ychForm.controls.auctionType.value.code.toUpperCase(),
      mainArt: null,
    };

    this.auctionApiService.create(cmd, this.ychForm.controls.images.value).subscribe(
      result => {
        this.router.navigate([`details-auction/${result.response.ychAuctionId}/tab/description`]);
      }
    );
  }

  private setupStepper(): void {
    this.items = [
      { label: 'Price', routerLink: 'price-step' },
      { label: 'Art', routerLink: 'art-step' },
      { label: 'Details', routerLink: 'details-step' },
    ];
  }
}
