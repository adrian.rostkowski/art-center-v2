import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAuctionButtonComponent } from './create-auction-button.component';

describe('CreateAuctionButtonComponent', () => {
  let component: CreateAuctionButtonComponent;
  let fixture: ComponentFixture<CreateAuctionButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAuctionButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAuctionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
