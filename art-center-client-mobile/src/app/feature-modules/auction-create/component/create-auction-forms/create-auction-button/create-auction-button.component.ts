import { Component, EventEmitter, Input, Output } from '@angular/core';
import { YchForm } from '../ych/ych-form/ych-form';

@Component({
  selector: 'app-create-auction-button',
  templateUrl: './create-auction-button.component.html',
  styleUrls: ['./create-auction-button.component.css']
})
export class CreateAuctionButtonComponent {

  @Input() ychForm: YchForm;
  @Output() create = new EventEmitter<null>();

  createClicked(): void {
    this.create.emit();
  }

}
