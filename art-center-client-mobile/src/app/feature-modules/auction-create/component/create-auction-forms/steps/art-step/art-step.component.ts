import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SelectOption } from '../../../../../../form-controls/select-input/select-input/select-input.component';
import { StepsCommunicationServiceService } from '../steps-communication-service.service';

@Component({
  selector: 'app-art-step',
  templateUrl: './art-step.component.html',
  styleUrls: ['./art-step.component.css']
})
export class ArtStepComponent implements OnInit {

  formGroup: FormGroup;

  characterType: SelectOption[];
  auctionType: SelectOption[];

  constructor(
    private stepsCommunicationServiceService: StepsCommunicationServiceService,
  ) { }

  ngOnInit(): void {
    this.characterType = [
      { name: 'Pony', code: 'Pony' },
      { name: 'Furry', code: 'Furry' },
      { name: 'Human', code: 'Human' },
    ];

    this.auctionType = [
      { name: 'Safe', code: 'Safe' },
      { name: 'Explicit', code: 'Explicit' },
      { name: 'NSFW', code: 'NOT_SAFE_FOR_WORK' },
    ];

    this.stepsCommunicationServiceService.artFormGroup.subscribe(
      result => {
        this.formGroup = result;
      }
    );
  }

}
