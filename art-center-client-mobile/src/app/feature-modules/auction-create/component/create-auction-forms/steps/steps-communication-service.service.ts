import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class StepsCommunicationServiceService {

  public priceFormGroup = new BehaviorSubject<FormGroup>(null);
  public artFormGroup = new BehaviorSubject<FormGroup>(null);
  public detailsFormGroup = new BehaviorSubject<FormGroup>(null);

  constructor() { }
}
