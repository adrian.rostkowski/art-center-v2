import { Component, OnInit } from '@angular/core';
import { StepsCommunicationServiceService } from '../steps-communication-service.service';
import { FormGroup } from '@angular/forms';
import { SelectOption } from '../../../../../../form-controls/select-input/select-input/select-input.component';

@Component({
  selector: 'app-price-step',
  templateUrl: './price-step.component.html',
  styleUrls: ['./price-step.component.css']
})
export class PriceStepComponent implements OnInit {

  formGroup: FormGroup;

  currencyOptionsDictionary: SelectOption[];

  constructor(
    private stepsCommunicationServiceService: StepsCommunicationServiceService,
  ) { }

  ngOnInit(): void {
    this.currencyOptionsDictionary = [
      { name: 'USD', code: 'USD' },
      { name: 'DeviantArt', code: 'DEVIANT_ART_POINTS' },
    ];

    this.stepsCommunicationServiceService.priceFormGroup.subscribe(
      result => {
        this.formGroup = result;
      }
    );
  }

}
