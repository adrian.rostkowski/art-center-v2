import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SelectOption } from '../../../../../../form-controls/select-input/select-input/select-input.component';
import { StepsCommunicationServiceService } from '../steps-communication-service.service';

@Component({
  selector: 'app-details-step',
  templateUrl: './details-step.component.html',
  styleUrls: ['./details-step.component.css']
})
export class DetailsStepComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private stepsCommunicationServiceService: StepsCommunicationServiceService,
  ) { }

  ngOnInit(): void {

    this.stepsCommunicationServiceService.detailsFormGroup.subscribe(
      result => {
        this.formGroup = result;
      }
    );
  }

}
