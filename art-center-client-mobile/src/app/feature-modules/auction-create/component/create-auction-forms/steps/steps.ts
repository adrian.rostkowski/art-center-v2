export class Steps {
  readonly steps: Step[] = [];

  constructor() {
    this.steps = [
      { name: 'PRICE', numberOf: 0, pathToView: 'ych/price-step' },
      { name: 'ART', numberOf: 1, pathToView: 'ych/art-step' },
      { name: 'DETAILS', numberOf: 2, pathToView: 'ych/details-step' },
    ];
  }

  public findByName(name: string): Step {
    return this.steps.filter(n => n.name === name)[0];
  }

  public findByNumber(numberOf: number): Step {
    return this.steps.filter(n => n.numberOf === numberOf)[0];
  }

  public isNextStepAvailable(nextStepNumber: number): boolean {
    return this.steps.length > nextStepNumber;
  }

  public isPreviousStepAvailable(nextStepNumber: number): boolean {
    return -1 < nextStepNumber;
  }
}

export interface Step {
  numberOf: number;
  name: string;
  pathToView: string;
}
