import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

export class PriceStepForm {
  private formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  public createPriceStep(): FormGroup {
    this.formGroup = this.fb.group({
      minBid: [null, [Validators.required]],
      isMaxBid: [false, [Validators.required]],
      maxBid: [null, [Validators.required]],
      currency: [null, [Validators.required]],
    });

    return this.formGroup;
  }
}

export interface Controls {
  minBid: FormControl;
  isMaxBid: FormControl;
  maxBid: FormControl;
  currency: FormControl;
}
