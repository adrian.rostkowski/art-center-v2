import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-select-auction',
  templateUrl: './select-auction.component.html',
  styleUrls: ['./select-auction.component.css']
})
export class SelectAuctionComponent {
  @Output() ychSelected = new EventEmitter<null>();

  onYchSelected(): void {
    this.ychSelected.emit();
  }
}
